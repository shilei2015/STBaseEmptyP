
#import "RequestHelper.h"

//上传文件
static NSString * const kApi_upload_image = @"uploadImage";


@interface RequestHelper ()

@end

@implementation RequestHelper

+ (void)load {
    [STBaseNetworking configAppNetworkClass:[self class]];
    [UIScrollView configDefaultEmptyTitle:@"no data title" detail:@"no data detail" imgName:@"img_none"];
    
}

+ (NSString *)baseUrlString {
    return [STBaseNetworking baseUrlString];
}

///网络请求基础路径
+ (NSString *)baseUrlString_default {
    return kBaseUrl;
}

+ (NSString *)baseUrlString_debug {
    return kBaseUrl_debug;
}

+ (NSString *)baseUrlString_release {
    return kBaseUrl_release;
}

+ (NSString *)netWorkTipString:(NSDictionary *)responseDict {
    if ([responseDict isKindOfClass:[NSDictionary class]]) {
        id data = responseDict[@"data"];
        if ([data isKindOfClass:[NSDictionary class]]) {
            return responseDict[@"data"][@"toast"];
        }
    }
    return nil;
}


+ (void)request:(NSString *)apiStr parms:(nullable NSDictionary *)requestParms SuccessBlock:(nullable RequestHelperComplect)networkComplect {
    [self request:apiStr parms:requestParms SuccessBlock:networkComplect loadingHud:NO errorTip:NO];
}

+ (void)cancelRequestWithURL:(nullable NSString *)url {
    [STBaseNetworking cancelRequestWithURL:url];
}




+ (void)request:(NSString *)apiStr parms:(NSDictionary *)requestParms SuccessBlock:(RequestHelperComplect)networkComplect loadingHud:(BOOL)showHud errorTip:(BOOL)tipError {
    
    NSMutableDictionary * parms = [NSMutableDictionary dictionaryWithDictionary:requestParms];
    
    [STBaseNetworking requestWithApiName:apiStr parms:parms requestType:YHHttpMethodPOST SuccessBlock:^(NSDictionary * responseDic, NSError * _Nullable error) {
                
        //1.到这里responseDic一定是NSDictionary
        NSInteger code = [responseDic[@"code"] integerValue];
        if (![self dealCanContinueCode:code]) {
            return;
        }
        if (error.code == NSURLErrorCancelled) {
            return;
        }

        if (networkComplect) {
            id data = responseDic[@"data"];
            if (![data isKindOfClass:[NSDictionary class]]) {data = @{};}
            networkComplect(responseDic, code, data, error);
        }
        
    } loadingHud:showHud errorTip:tipError];
}




+ (void)uploadImages:(NSArray<UIImage *> * _Nullable)items loading:(BOOL)loading success:(nullable void(^)(NSArray <NSString *> *urlStrs))success fail:(nullable void(^)(NSError * error))fail {
    
    
    [STBaseNetworking uploadImages:items preDeal:^(UIImage * _Nonnull image) {
        float scale = 1.3;
        CGSize uploadImgMaxSize = image.size;
        if (scale) {
            CGSize screenSize = [UIScreen mainScreen].bounds.size;
            uploadImgMaxSize = CGSizeMake(screenSize.width*scale, screenSize.height*scale);
        }
        UIImage * uploadImg = [image compressionWithMaxImageSize:uploadImgMaxSize];
        
    } url:kApi_upload_image successBlock:success errorBlock:fail showHud:loading];
    
}






//MARK: - - - - - - - - - - - <STListRefreshNetWork>        列表刷新网络请求
+ (void)refreshNetWithApiName:(NSString *)apiStr parms:(NSDictionary *)requestParms requestType:(RefreshNetWorkType)type successBlock:(RefreshNetComplectBlock)complectBlock failureBlock:(RefreshNetFailureBlock)failureBlock {
    
    YHHttpMethod netType;
    if (type == RefreshNetWorkType_Get) {
        netType = YHHttpMethodGET;
    } else {
        netType = YHHttpMethodPOST;
    }
    
    [self request:apiStr parms:requestParms SuccessBlock:^(NSDictionary * _Nonnull responseDict, NSInteger code, NSDictionary * _Nullable data, NSError * _Nullable error) {
        if (!error && code == 0) {
            complectBlock(data,responseDict);
        } else {
            failureBlock(error);
        }
    } loadingHud:NO errorTip:YES];
    
}


//MARK: - - - - - - - - - - - 处理签名
+ (void)signtureDealWithHeaderDict:(NSMutableDictionary *)header requestParms:(NSMutableDictionary *)requestParms {
    
    NSInteger Nonce = arc4random()%10000000;
    NSString * nonceString = [NSString stringWithFormat:@"%zd",Nonce];
    
    {
        NSMutableDictionary * dict = header;
        
        if ([STDebug appId] && [STDebug appId].length) {
            [dict sl_setObject:[STDebug appId] forKey:@"AppId"];
        } else {
            [dict sl_setObject:kCustomAppId forKey:@"AppId"];
        }
        
        if ([STDebug version] && [STDebug version].length) {
            [dict sl_setObject:[STDebug version] forKey:@"Version"];
        } else {
            [dict sl_setObject:[NSString appVersion] forKey:@"Version"];
        }
        
        
        if ([STDebug enableVerify]) {
            [dict sl_setObject:kSkipVerify forKey:@"SkipVerify"];
        }
        
        
        [dict sl_setObject:@"en" forKey:@"Language"];
        [dict sl_setObject:nonceString forKey:@"Nonce"];
        
        [dict sl_setObject:App.app.Token forKey:@"Token"];
        
        NSMutableDictionary * signtureDict = [NSMutableDictionary dictionaryWithDictionary:requestParms];
        [signtureDict sl_setObject:nonceString forKey:@"Nonce"];
        
        NSString * SIGNATURE = [self signStringWithDict:signtureDict];

        [dict sl_setObject:SIGNATURE forKey:@"Signature"];
    }
    
}



+ (NSString *)signStringWithDict:(NSDictionary *)signtureDict {
    
    NSArray * keys = [[signtureDict allKeys] sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        return [obj1 compare:obj2];
    }];
    
    NSMutableArray * signKVArr = [NSMutableArray new];
    for (int i = 0; i < keys.count; i ++) {
        NSString * key = keys[i];
        NSString * value = [NSString stringWithFormat:@"%@",signtureDict[key]];
        
        NSString * kvStr = [NSString stringWithFormat:@"%@=%@",[self base64String:key],[self base64String:value]];
        [signKVArr addObject:kvStr];
    }
    
    NSString * signString = [signKVArr componentsJoinedByString:@"&"];
    
    NSString * SIGNATURE_PARAM = [NSString stringWithFormat:@"%@&%@",signString,kCustomAppKey];
    
    NSString * SIGNATURE = [SIGNATURE_PARAM MD5String];
    
    NSMutableDictionary * logDic = [NSMutableDictionary dictionary];
    [logDic sl_setObject:SIGNATURE forKey:@"SIGNATURE"];
    [logDic sl_setObject:kSkipVerify forKey:@"SkipVerify"];
    [logDic sl_setObject:SIGNATURE_PARAM forKey:@"SIGNATURE_PARAM"];
        
    return SIGNATURE;
}


+ (NSString *)base64String:(NSString *)string {
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
    NSData *base64Data = [data base64EncodedDataWithOptions:0];
    NSString *baseString = [[NSString alloc]initWithData:base64Data encoding:NSUTF8StringEncoding];
    return baseString;
}





+ (BOOL)dealCanContinueCode:(NSInteger)code {
    if (code == 1) {
        [App exitAppLogin];
        [AppDelegate applicationShowRootWithType:RootType_Login];
    } else if (code == 10103) {
        
    }
    return YES;
}



@end
