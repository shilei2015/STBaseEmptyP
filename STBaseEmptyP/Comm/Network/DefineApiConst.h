
#ifndef DefineApiConst_h
#define DefineApiConst_h


///游客reg
static NSString * const kApi_zhuce = @"register";

///游客登录
static NSString * const kApi_login = @"login";

///黑名单
static NSString * const kApi_black_userlist = @"userBlackList";

///移除黑名单用户
static NSString * const kApi_delete_blackUser = @"userRemoveBlack";

///金币列表
static NSString * const kApi_coinlist = @"coinsList";

///注销
static NSString * const kApi_closeAnAccount = @"userLogOff";

///用户信息
static NSString * const kApi_appuser_info = @"userInfo";

///版本信息
static NSString * const kApi_version = @"getNewVersion";

///资料编辑
static NSString * const kApi_user_edit = @"editUser";

///星座、语言、国家
static NSString * const kApi_center_itemlist = @"country8Launage";

///推送绑定
static NSString * const kApi_Tpns_bind = @"bindTpnsAccount";

///票据验证
static NSString * const kApi_pay_verify = @"appleVerifyOrder";

///预订单
static NSString * const kApi_pay_preOrder = @"preOrder";

///修改头像
static NSString * const kApi_head_edit = @"editHeadImage";




#pragma mark part0
///房间列表
static NSString * const kApi_voiceRoomList = @"voiceRoomList";

///房间类型
static NSString * const kApi_Room_list_type = @"voiceRoomType";

///coolImages
static NSString * const kApi_Room_list_Cool = @"voiceCoolImage";

///创建房间
static NSString * const kApi_Room_Create = @"voiceAddRoom";

///视频列表
static NSString * const kApi_VideoList = @"voiceVideoList";

///评论列表
static NSString * const kApi_Video_Comment_list = @"voiceCommentList";

///添加评论
static NSString * const kApi_Video_Comment = @"voiceVideoComment";

///添加视频
static NSString * const kApi_Video_Create = @"voiceCreateVideo";

///资讯列表
static NSString * const kApi_NewsList = @"voiceRoomArticle";
















#pragma mark part1 暂未调用
///定时任务
static NSString * const kApi_task_minute = @"taskMinuteI";

///给我点赞的列表
static NSString * const kApi_like_userlist = @"likeMeList";

#pragma mark part1_2
///通过ids获取userlist
static NSString * const kApi_user_get = @"userListById";

static NSString * const kApi_userConfig = @"userConfig";


#endif /* DefineApiConst_h */
