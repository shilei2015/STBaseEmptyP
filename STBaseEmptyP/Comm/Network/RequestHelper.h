
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

#define kAgoDev 1
#if kAgoDev

#endif

static NSString * const kBaseUrl_debug = @"http://vclub23.cookiegeeks.com/client/"; ///测试服
static NSString * const kBaseUrl_release = @"http://apiprod.joie.pro/client/"; ///正式服
static NSString * const kCustomAppId = @"1004";
static NSString * const kSkipVerify = @"TEST_20230106";

/// 是否正式环境
#define kCustomRelease 0

#if !kCustomRelease && DEBUG
///测试服
static NSString * const kBaseUrl = kBaseUrl_debug;
static NSString * const kCustomAppKey = @"366FE56D8E422EDD10572AA9431E0F3A";
static NSString * KAgora_AppId = @"a80d13923822432cad427f4ce5d186e5";
#else
///正式服
static NSString * const kBaseUrl = kBaseUrl_release;
static NSString * const kCustomAppKey = @"7977C73E1E9A17A7EAE014D9B339F847";
static NSString * KAgora_AppId = @"8479326d5cd8417eb0c81175ebebe43f";
#endif


@interface RequestHelper : NSObject<SDKNetworkingProtocal,STListRefreshNetWork>


@end

NS_ASSUME_NONNULL_END
