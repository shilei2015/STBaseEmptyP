
#import <Foundation/Foundation.h>
#import "Regist.h"
#import "AppTemp.h"


NS_ASSUME_NONNULL_BEGIN

@interface AppToken : NSObject
///推送上传token
@property (nonatomic , copy)  NSString * _Nullable  xgToken;
///app请求token
@property (nonatomic , copy) NSString * _Nullable Token;
///im登录token
@property (nonatomic , copy) NSString * _Nullable imToken;

@end


@interface App : NSObject
@property (nonatomic , strong) AppToken * _Nullable aToken;
@property (nonatomic , strong) Regist * _Nullable regInfo;
@property (nonatomic , strong) AppUser * _Nullable loginUser;
///管理应用中的临时数据  或跟随应用生命周期  或 流程生命周期（由新建模型管理】）
@property (nonatomic , strong) AppTemp * _Nullable temp;
///推送上传token
@property (nonatomic , copy)  NSString * _Nullable  xgToken;
///app请求token
@property (nonatomic , copy) NSString * _Nullable Token;
///im登录token
@property (nonatomic , copy) NSString * _Nullable imToken;

///获取App对象模型
+ (instancetype)app;

///是否登陆
+ (BOOL)isLogin;

///通过登陆信息进入主页
+ (void)enterWithStartData:(NSDictionary *)data;

///退出登陆
+ (void)exitAppLogin;

///更新用户信息
+ (void)updateLoginUser:(AppUser *)loginUser;

///同步保存App对象模型
+ (void)synchronize;

@end



@interface App (Request)


@end



NS_ASSUME_NONNULL_END
