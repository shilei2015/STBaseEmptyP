
#import "App.h"

@interface App ()

@property (nonatomic , copy) NSString * identifer;

@end

@implementation App
- (NSString *)identifer {
    return [NSString appBundleID];
}

///唯一约束
+ (NSArray *)bg_uniqueKeys {
    return @[@"identifer"];
}

///忽略保存的字段
+(NSArray *)bg_ignoreKeys{
   return @[@"temp",@"regInfo",@"xgToken"];
}


+ (instancetype)app {
    
    static App * sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSArray <App *>* arr = [App bg_findAll:nil];
        App * app = arr.lastObject;
        if (app) {
            sharedInstance = app;
        } else {
            sharedInstance = [[self alloc] init];
        }
    });
    
    return sharedInstance;
}


+ (BOOL)isLogin {
    if (App.app.Token && App.app.Token.length) {
        [AppUser updatePoints:[NSString stringWithFormat:@"%@",AppUser.appUser.Coins]];
        return YES;
    }
    return NO;
}



+ (void)enterWithStartData:(NSDictionary *)data {
    
    if (data && [data isKindOfClass:[NSDictionary class]]) {
        
        [App app].Token = data[@"Token"];
        [App app].imToken = data[@"RtmToken"];
        
        AppUser * loginUser = [AppUser yy_modelWithJSON:data[@"User"]];
        [App updateLoginUser:loginUser];
        
        [AppDelegate checkToBindTpnsToken];
        [AppDelegate applicationShowRootWithType:RootType_Home];
    }
}


+ (void)exitAppLogin {
    [App app].Token = nil;
    [App app].imToken = nil;
    
    [App app].regInfo = nil;
    [App app].loginUser = nil;
    [App bg_clearAsync:nil complete:nil];
    
    [AppDelegate applicationShowRootWithType:RootType_Login];
}


+ (void)updateLoginUser:(AppUser *)loginUser {
    
    [App app].loginUser = [loginUser yy_modelCopy];
    
    [AppUser saveToChat];
    
    [self synchronize];
    
    [AppUser updatePoints:loginUser.Coins];
}

+ (void)synchronize {
    [[App app] resetEventTimeInTime:2 handle:^(id  _Nullable objself, NSInteger handleType, id  _Nullable obj, NSString * _Nullable des) {
        [App bg_clear:nil];
        [objself bg_saveAsync:nil];
    }];
}


- (Regist *)regInfo {
    if (!_regInfo) {
        _regInfo = [Regist new];
    }
    return _regInfo;
}

- (AppTemp *)temp {
    if (!_temp) {
        _temp = [AppTemp new];
    }
    return _temp;
}


@end




@implementation App (Request)




@end



