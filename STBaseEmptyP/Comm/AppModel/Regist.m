
#import "Regist.h"
#import "App.h"

@implementation Regist

+ (instancetype)regist {
    return [App app].regInfo;
}

@end

@implementation AppUser

+ (instancetype)appUser {
    return [App app].loginUser;
}

+ (void)updatePoints:(NSString *)points {
    AppUser.appUser.Coins = points;
    [App synchronize];
}

+ (void)saveToChat {
    
}

@end


///性别相关处理
@implementation AppUser (Helper)

+ (NSString *)genderName:(AppGender)gender {
    switch (gender) {
        case AppGenderBoth:
            return @"Both".st_localized;
            
        case AppGenderMale:
            return @"Men".st_localized;
            
        case AppGenderFemale:
            return @"Women".st_localized;
            
        default:
            break;
    }
    return @"";
}

+ (NSString *)genderImageName:(AppGender)gender {
    switch (gender) {
        case AppGenderBoth:
            return @"icon_both";
            
        case AppGenderMale:
            return @"icon_men";
            
        case AppGenderFemale:
            return @"icon_women";
            
        default:
            break;
    }
    return @"";
}


@end


static NSString * const kApi_appuser_info = @"";
static NSString * const kApi_Tpns_bind = @"";


@implementation AppUser (Api)

+ (void)loadAppUser:(void (^)(AppUser * _Nonnull user, BOOL ok, NSError * _Nullable error))complect loading:(BOOL)loading errorTip:(BOOL)tip {
    
    [RequestHelper request:kApi_appuser_info parms:nil SuccessBlock:^(NSDictionary * _Nonnull responseDict, NSInteger code, NSDictionary * _Nullable data, NSError * _Nullable error) {
        AppUser * getUInfo;
        if (!error && code == 0) {
            getUInfo = [AppUser yy_modelWithJSON:data[@"User"]];
            [App updateLoginUser:getUInfo];
        }
        if (complect) {complect(getUInfo,YES,error);}
    } loadingHud:loading errorTip:tip];
}

+ (void)bindPushToken {
    if ([AppDelegate appdelegate].xgToken && AppUser.appUser.UserId) {
        NSMutableDictionary * requestParms = [NSMutableDictionary new];
        [requestParms sl_setObject:[AppDelegate appdelegate].xgToken forKey:@"TpnsToken"];
        [RequestHelper request:kApi_Tpns_bind parms:requestParms SuccessBlock:nil loadingHud:NO errorTip:NO];
    }
}

@end
