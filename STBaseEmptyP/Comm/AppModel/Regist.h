
#import <Foundation/Foundation.h>

typedef enum {
    AppGenderBoth,
    AppGenderMale,
    AppGenderFemale,
} AppGender;


NS_ASSUME_NONNULL_BEGIN

@interface Regist : NSObject
@property (nonatomic , assign) AppGender Gender;
///时间戳格式如："112103233"
@property (nonatomic , assign) NSInteger birthTimeSpan;

+ (instancetype)regist;

@end





@interface AppUser : NSObject

@property (nonatomic , copy) NSString              * UserId;
@property (nonatomic , copy) NSString              * UdId;
@property (nonatomic , copy) NSString              * Nickname;
@property (nonatomic , assign) AppGender             Gender;

@property (nonatomic , copy) NSString              * HeadImage;
@property (nonatomic , copy) NSString              * Birthday;
@property (nonatomic , copy) NSString              * Introduce;
@property (nonatomic , copy) NSString              * Sign;
@property (nonatomic , copy) NSString              * StarSign;
@property (nonatomic , copy) NSString              * CountryCode;
@property (nonatomic , copy) NSString              * Country;
@property (nonatomic , copy) NSString              * Language;
@property (nonatomic , copy) NSString              * AppId;
@property (nonatomic , copy) NSString              * VersionId;
@property (nonatomic , copy) NSString              * Coins;
@property (nonatomic , strong) NSArray <NSString *> * Albums;


///获取当前App用户
+ (instancetype)appUser;

///更新app用户积分
+ (void)updatePoints:(NSString *)points;

///暂未实现
+ (void)saveToChat;

@end


///性别相关处理
@interface AppUser (Helper)
+ (NSString *)genderName:(AppGender)gender;
+ (NSString *)genderImageName:(AppGender)gender;
@end

@interface AppUser (Api)

///网络请求更新用户信息
+ (void)loadAppUser:(nullable void(^)(AppUser * _Nonnull user, BOOL ok, NSError * _Nullable error))complect loading:(BOOL)loading errorTip:(BOOL)tip;

///推送绑定
+ (void)bindPushToken;

@end




NS_ASSUME_NONNULL_END
