

#import "AppDelegate.h"


@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [self root_application:application didFinishLaunchingWithOptions:launchOptions];
    [self push_application:application didFinishLaunchingWithOptions:launchOptions];
    
//    [STDebug DebugLogStart];
    
    return YES;
}

@end
