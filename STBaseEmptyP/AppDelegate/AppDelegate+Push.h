
#import "AppDelegate.h"

NS_ASSUME_NONNULL_BEGIN

@interface AppDelegate (Push)

@property (nonatomic , copy) NSString * xgToken;

-(BOOL)push_application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;

+ (void)checkToBindTpnsToken;

@end

NS_ASSUME_NONNULL_END
