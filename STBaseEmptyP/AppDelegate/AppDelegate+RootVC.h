
#import "AppDelegate.h"

NS_ASSUME_NONNULL_BEGIN


typedef enum {
    RootType_Login = 0,
    RootType_Home = 1,
    
    RootType_Debug = 10086,
} RootType;

typedef UIViewController* _Nullable (^AppdelegateRootVC)(RootType type);

@interface AppDelegate (Property)

@property (strong, nonatomic) UIWindow * window;
@property (strong, nonatomic) UITabBarController * mainTabVC;

@end


@interface AppDelegate (RootVC)
///显示加载页
- (BOOL)root_application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;

///切换根控制器
+ (void)applicationShowRootWithType:(RootType)rootType;

///获取当前应用AppDelegate对象
+ (AppDelegate *)appdelegate;

@end

NS_ASSUME_NONNULL_END
