
#import "AppDelegate+Push.h"
#import <objc/runtime.h>

#define XGPUSH false

#if XGPUSH
#import <XGPush.h>
#import <XGPushPrivate.h>

static uint32_t const kTPNSId = 0;
static NSString * const kTPNSKey = @"key";
static NSString * const kPush_Domain = @"tpns.sgp.tencent.com";

#endif


#if XGPUSH
@interface AppDelegate ()<XGPushDelegate>
#else
@interface AppDelegate ()
#endif


@end


@implementation AppDelegate (Push)

- (void)setXgToken:(NSString *)xgToken {
    objc_setAssociatedObject(self, @selector(xgToken), xgToken, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (NSString *)xgToken {
    return objc_getAssociatedObject(self, @selector(xgToken));
}


#if XGPUSH
-(BOOL)push_application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    NSLog(@"已集成TPNS");
    [[XGPush defaultManager] configureClusterDomainName:kPush_Domain];
    [[XGPush defaultManager] startXGWithAccessID:kTPNSId accessKey:kTPNSKey delegate:self];
    
#if DEBUG
    //打开 debug 开关
    [[XGPush defaultManager] setEnableDebug:YES];
#endif
    
    
    [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidBecomeActiveNotification object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    }];
    
    return YES;
}


/**
@brief 注册推送服务回调
@param deviceToken APNs 生成的 Device Token
@param xgToken TPNS 生成的 Token，推送消息时需要使用此值。TPNS 维护此值与 APNs 的 Device Token 的映射关系
@param error 错误信息，若 error 为 nil 则注册推送服务成功
@note TPNS SDK1.2.6.0+
*/
- (void)xgPushDidRegisteredDeviceToken:(nullable NSString *)deviceToken xgToken:(nullable NSString *)xgToken error:(nullable NSError *)error {
    //获取 TPNS 生成的 Token
    NSString * token = [[XGPushTokenManager defaultTokenManager] xgTokenString];
    [AppDelegate hasGetTPNSToken:token];
}

/// 注册推送服务失败回调
/// @param error 注册失败错误信息
/// @note TPNS SDK1.2.7.1+
- (void)xgPushDidFailToRegisterDeviceTokenWithError:(nullable NSError *)error {
    
}


/// 统一接收消息的回调
/// @param notification 消息对象(有2种类型NSDictionary和UNNotification具体解析参考示例代码)
/// @note 此回调为前台收到通知消息及所有状态下收到静默消息的回调（消息点击需使用统一点击回调）
/// 区分消息类型说明：xg字段里的msgtype为1则代表通知消息msgtype为2则代表静默消息
- (void)xgPushDidReceiveRemoteNotification:(nonnull id)notification withCompletionHandler:(nullable void (^)(NSUInteger))completionHandler{
 
}
 /// 统一点击回调
/// @param response 如果iOS 10+/macOS 10.14+则为UNNotificationResponse，低于目标版本则为NSDictionary
- (void)xgPushDidReceiveNotificationResponse:(nonnull id)response withCompletionHandler:(nonnull void (^)(void))completionHandler {
 
}


+ (void)jumpToAppSystemSetting {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:@{} completionHandler:nil];
}


+ (void)hasGetTPNSToken:(NSString *)token {
    [AppDelegate appdelegate].xgToken = token;
    [AppDelegate checkToBindTpnsToken];
}

+ (void)checkToBindTpnsToken {
    //TODO: 网络请求通知后台 token
    [AppUser bindPushToken];
}

#else
-(BOOL)push_application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    NSLog(@"未集成TPNS-iOS");
    return YES;
}

+ (void)checkToBindTpnsToken {}
#endif

@end
