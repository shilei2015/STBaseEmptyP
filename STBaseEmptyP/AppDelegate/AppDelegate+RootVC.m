

#import "AppDelegate+RootVC.h"
#import <objc/runtime.h>


static NSString * const kNotiSwitchRootViewController = @"kNotiSwitchRootViewController";


//MARK: - - - - - - - - - - - 属性
@implementation AppDelegate (Property)

- (void)setWindow:(UIWindow *)window {
    objc_setAssociatedObject(self, @selector(window), window, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIWindow *)window {
    return objc_getAssociatedObject(self, @selector(window));
}


- (void)setMainTabVC:(UITabBarController *)mainTabVC {
    objc_setAssociatedObject(self, @selector(mainTabVC), mainTabVC, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UITabBarController *)mainTabVC {
    return objc_getAssociatedObject(self, @selector(mainTabVC));
}

@end



@implementation AppDelegate (RootVC)

//MARK: - - - - - - - - - - - 类方法
///设置当前根控制器
+ (void)applicationShowRootWithType:(RootType)rootType {
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotiSwitchRootViewController object:@(rootType)];
}

///获取当前Appdelegate
+ (AppDelegate *)appdelegate {
    AppDelegate * delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    return delegate;
}




//MARK: - - - - - - - - - - - 对象方法
- (BOOL)root_application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"LaunchScreen" bundle:nil];
    UIViewController * loading = [storyboard instantiateViewControllerWithIdentifier:@"LaunchScreen"];
    [self showRootVC:loading];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(switchRootVC:) name:kNotiSwitchRootViewController object:nil];
        
    [self startAppRunFirst];
    
    return YES;
}


//控制器作为App根控制器显示
- (void)showRootVC:(UIViewController *)rootVC {
    self.window.rootViewController = nil;
    [self.window makeKeyAndVisible];
    self.window.rootViewController = rootVC;
}




//MARK: - - - - - - - - - - - Switch Root
- (void)switchRootVC:(NSNotification *)noti {
    
    RootType type = [noti.object intValue];
    
    UIViewController * rootVC = [self rootVCWithType:type];
    if (!rootVC) {
        rootVC = [UIViewController new];
        rootVC.view.backgroundColor = [UIColor orangeColor];
        NSString * text = [NSString stringWithFormat:@"请配置RootType = %d 时的 RootVC",type];
        UILabel * label = [UILabel new];
        label.text = text;
        label.textAlignment = NSTextAlignmentCenter;
        label.numberOfLines = 0;
        
        [rootVC.view addSubview:label];
        
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(label.superview);
        }];
    }
    [self showRootVC:rootVC];
    

    [self.window makeKeyAndVisible];
}


- (void)startAppRunFirst {
    
    if ([self canShowHomePage]) {
        [AppDelegate applicationShowRootWithType:RootType_Home];
    } else {
        [AppDelegate applicationShowRootWithType:RootType_Login];
    }
}


- (UIViewController *)controllerFromClassName:(NSString *)className {
    UIViewController * startVC = [NSClassFromString(className) new];
    if (!startVC) {
        startVC = [UIViewController new];
        
        NSString * text = [NSString stringWithFormat:@"请配置【className = %@】的控制器",className];
        UILabel * label = [UILabel new];
        label.text = text;
        label.textAlignment = NSTextAlignmentCenter;
        label.numberOfLines = 0;
        
        [startVC.view addSubview:label];
        
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(label.superview);
        }];
        
    }
    return startVC;
}


#pragma mark 项目配置登陆或登陆后页面

- (BOOL)canShowHomePage {
    return YES;
}

- (UIViewController *)rootVCWithType:(RootType)type {
    
    if (type == RootType_Login) {
        [UIViewController currentViewController];
        UIViewController * startVC = [self controllerFromClassName:@"AppVC"];
        STBaseNav * nav = [[STBaseNav alloc] initWithRootViewController:startVC];
        return nav;
    } else if (type == RootType_Home) {
        UIViewController * tabVC = [self controllerFromClassName:@"AppTabVC"];
        STBaseNav * nav = [[STBaseNav alloc] initWithRootViewController:tabVC];
        if ([tabVC isKindOfClass:[UITabBarController class]]) {
            self.mainTabVC = (UITabBarController *)tabVC;
        }
        return nav;
    } else if (type == RootType_Debug) {
        
    }
    
    return nil;
}

@end
