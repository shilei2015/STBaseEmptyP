

#import "AppTabVC.h"
#import "AppFloatTabbar.h"

@interface AppTabVC ()

@property (nonatomic,strong) UITabBarItem * currentItem;

@end

@implementation AppTabVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initFloatTabbar];
    
    {
        [self addItemWithVC:[UIViewController controllerFromClassName:@"POneVC"] nImage:@"t_icon_1_n" sImage:@"t_icon_1_s"];
        [self addItemWithVC:[UIViewController controllerFromClassName:@"POneVC"] nImage:@"t_icon_2_n" sImage:@"t_icon_2_s"];
        [self addItemWithVC:[UIViewController controllerFromClassName:@"POneVC"] nImage:@"t_icon_3_n" sImage:@"t_icon_3_s"];
        [self addItemWithVC:[UIViewController controllerFromClassName:@"POneVC"] nImage:@"t_icon_4_n" sImage:@"t_icon_4_s"];
    }
    
    [self UIConfig];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}




- (void)UIConfig {
    
    UIColor * tabbarBGColor = [UIColor colorWithHexString:@"#1C1B1B"];
    UIColor * shadowColor = [UIColor clearColor];
    if (@available(iOS 15.0, *)) {
        UITabBarAppearance *appearance = [UITabBarAppearance new];
        appearance.backgroundColor = tabbarBGColor;
        appearance.shadowColor = shadowColor;

        self.tabBar.scrollEdgeAppearance = appearance;
        self.tabBar.standardAppearance = appearance;
        [self.tabBar setBackgroundImage:[UIImage new]];
    } else {
        self.tabBar.backgroundImage = [UIImage new];
        self.tabBar.shadowImage = [UIImage new];
        self.tabBar.barTintColor = tabbarBGColor;
    }
    
    self.tabBar.translucent = NO;
}


- (void)initFloatTabbar {
    AppFloatTabbar * tabbar = [[AppFloatTabbar alloc] initWithFrame:CGRectMake(0, UIDevice.height - 70, UIDevice.width, 70)];
    [self setValue:tabbar forKey:@"tabBar"];
}


- (void)addItemWithVC:(UIViewController *)controller nImage:(NSString *)nImage sImage:(NSString *)sImage {
    
    if (!controller) {
        controller = [UIViewController new];
    }
    
    controller.tabBarItem.image = [[UIImage imageNamed:nImage] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    controller.tabBarItem.selectedImage = [[UIImage imageNamed:sImage] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    controller.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
    
    [self addChildViewController:controller];
}




-(void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
    
    if (tabBar.selectedItem == self.currentItem) {
        return;
    }
    self.currentItem  = tabBar.selectedItem;
    
    if (@available(iOS 11.0, *)) {
        UIImpactFeedbackGenerator *feedBackGenertor = [[UIImpactFeedbackGenerator alloc] initWithStyle:UIImpactFeedbackStyleLight];
        [feedBackGenertor impactOccurred];
     }

}



@end
