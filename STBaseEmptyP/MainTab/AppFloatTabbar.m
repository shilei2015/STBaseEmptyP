//
//  AppFloatTabbar.m
//  PublishBase
//
//  Created by 石磊 on 2023/2/6.
//

#import "AppFloatTabbar.h"

static CGFloat const kTabbarH = 49;
static CGFloat const kTabbarItemH = 44;
static CGFloat const kTabbarSpaceH = 15;

@interface AppFloatTabbar ()

@property (nonatomic , strong) UIView * bgView;

@end


@implementation AppFloatTabbar

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self floatBar];
    }
    return self;
}


- (void)floatBar {
    if (!self.bgView) {
        UIView * bgView = [UIView new];
        bgView.frame = CGRectMake(kTabbarSpaceH, 0, UIDevice.width - kTabbarSpaceH*2, kTabbarH);
        bgView.backgroundColor = [UIColor colorWithHexString:@"#26252A"];
        bgView.cornerRadius = kTabbarH/2;
        
        self.bgView = bgView;
    }
    
    [self insertSubview:self.bgView atIndex:1];
    
    self.backgroundImage = nil;
    self.shadowImage = nil;
    
}


- (void)layoutSubviews {
    [super layoutSubviews];
    
    
    NSInteger count = self.items.count ?: 1;
    CGFloat itemW = (UIDevice.width - kTabbarSpaceH*2)/count;
    
    if (CGRectGetHeight(self.frame) < kTabbarItemH) {
        
        
    }
    
    NSInteger tempItemIndex = 0;
    UIView * tempRemoveV;
    for (UIView * itemView in self.subviews) {
        if ([itemView isKindOfClass:NSClassFromString(@"UITabBarButton")]) {
            itemView.frame = CGRectMake(tempItemIndex * itemW + kTabbarSpaceH, (kTabbarH - kTabbarItemH)/2, itemW, kTabbarItemH);
            tempItemIndex += 1;
        } else if ([itemView isKindOfClass:NSClassFromString(@"_UIBarBackground")]) {
            tempRemoveV = itemView;
        }
    }
    
    [tempRemoveV removeFromSuperview];
    
}

@end
