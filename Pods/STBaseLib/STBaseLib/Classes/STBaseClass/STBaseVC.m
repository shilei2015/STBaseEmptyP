//
//  STBaseVC.m
//  STBase
//
//  Created by 石磊 on 2022/11/1.
//

#import "STBaseVC.h"
#import "UIColor+STBase.h"
#import "NSObject+STBase.h"
#import "STDebug.h"
#import "NSBundle+LDSDKBundle.h"

@interface STBaseVC ()

@property(nonatomic, assign, readwrite) BOOL isFirstDidAppear;
@property(nonatomic, assign, readwrite) BOOL isFirstWillAppear;

@end

@implementation STBaseVC



- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (!self.view.backgroundColor) {
        self.view.backgroundColor = [UIColor colorWithHexString:@"#090012"];//黑色
    }
    
    self.navigationController.interactivePopGestureRecognizer.delegate = (id)self;
    self.enableBackGesture = YES;
    
    if (self.enableEmptyHideKeyBoard) {
        [self addHideKeyBoardTap];
    }
}




- (void)addHideKeyBoardTap {
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyboardHide:)];
    //设置成NO表示当前控件响应后会传播到其他控件上，默认为YES。
    tapGestureRecognizer.cancelsTouchesInView = NO;
    //将触摸事件添加到当前view
    [self.view addGestureRecognizer:tapGestureRecognizer];
}

- (void)keyboardHide:(UITapGestureRecognizer*)tap {
    [self.view endEditing:YES];
    LDLog(@"keyboardHide");
}

- (void)backItemClicked {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setEnableBackGesture:(BOOL)enableBackGesture {
    _enableBackGesture = enableBackGesture;
    if (enableBackGesture) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    }
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    if (!self.isFirstWillAppear) {
        self.isFirstWillAppear = YES;
        [self viewWillFirstAppear];
    }
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (self.navigationController.viewControllers.count > 1 && self.enableBackGesture == YES) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    } else {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    
    if (!self.isFirstDidAppear) {
        self.isFirstDidAppear = YES;
        [self viewDidFirstAppear];
    }
}


//MARK: - - - - - - - - - - - 自定义方法
- (void)viewWillFirstAppear {
    
}

- (void)viewDidFirstAppear {
    
}


- (UIViewController *)pushVCWithClassName:(NSString *)className {
    return [self pushVCWithClass:NSClassFromString(className)];
}

- (UIViewController *)pushVCWithClass:(Class)vcClass {
    UIViewController * vc = [[vcClass alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
    return vc;
}


- (void)dealloc {
    [self subDealloc];
    NSString * logT = [NSString stringWithFormat:@"__dealloc__❤️❤️❤️❤️🍏❤️❤️❤️❤️ %@",NSStringFromClass([self class])];
    LDLog(@"%@",logT);
}

- (void)subDealloc {
    
}

@end
