//
//  STXibDesignView.m
//  STBase
//
//  Created by 石磊 on 2022/11/1.
//

#import "STXibDesignView.h"
#import <Masonry/Masonry.h>
#import "STDebug.h"
#import "STPoper.h"
#import "UIViewController+BackButtonHandler.h"

@interface STXibDesignView ()
@property(nonatomic, assign) BOOL isLoad;
@property (nonatomic,strong,readwrite) UIView *contentView;

@end

@implementation STXibDesignView

- (instancetype)initWithCoder:(NSCoder *)coder {
    if (self = [super initWithCoder:coder]) {
        [self nibLoadSelf];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self nibLoadSelf];
    }
    return self;
}

- (void)prepareForInterfaceBuilder {
    [self nibLoadSelf];
}

- (void)nibLoadSelf {
#if TARGET_INTERFACE_BUILDER
    [self templateMethod];
#else
    @try {
        [self templateMethod];
    } @catch (NSException *exception) {
        LDLog(@"%@",exception.description);
    } @finally {

    }
#endif
}


- (void)templateMethod {
    
    if (!self.contentView && self.isLoad == NO) {
        self.isLoad = YES;

        NSString * componentNibName;
        UIView * contentView;
        NSBundle * bundle = [self.class selfBundle];
        componentNibName = [NSStringFromClass(self.class) componentsSeparatedByString:@"."].lastObject;

        UINib * nib = [UINib nibWithNibName:componentNibName bundle:bundle];
        contentView = [nib instantiateWithOwner:self options:nil].firstObject;
        
        self.contentView = contentView;
        if (self.backgroundColor) {
            self.contentView.backgroundColor = self.backgroundColor;
        }
        
        [self dealOtherSubviews:self.subviews];
        
        [self insertSubview:self.contentView atIndex:0];
    }
    
    [self customSetting];
}

- (void)layoutSubviews {
    [super layoutSubviews];

    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.mas_equalTo(0);
    }];
    
    if (CGSizeEqualToSize(self.frame.size, CGSizeZero) == NO) {
        if (self.getFrameHandle) {
            self.getFrameHandle(self);
        }
    }

    [self customLayoutSubs];
}



//MARK: - - - - - - - - - - - 外部重写
+ (instancetype)showWithSize:(CGSize)size {
    STXibDesignView * tpView = (STXibDesignView *)[self xibView];
    
    if (CGSizeEqualToSize(size, CGSizeZero) == YES) {
        UIView * tempBG = UIViewController.currentViewController.view;
        [tempBG addSubview:tpView];
        
        [tpView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.mas_equalTo(tempBG);
            make.left.mas_equalTo(20);
        }];
        
        tpView.getFrameHandle = ^(STXibDesignView * _Nonnull xibView) {
            if ([xibView.superview isKindOfClass:NSClassFromString(@"LSTPopViewBgView")] == NO) {
                xibView.frame = xibView.frame;
                [[STPoper st_popShowView:xibView position:(LDPopPositionCenter) blur:YES bgDismiss:YES] show];
            }
        };
        return tpView;
    }
    
    tpView.frame = CGRectMake(0, 0, size.width, size.height);
    [[STPoper st_popShowView:tpView position:(LDPopPositionCenter) blur:YES bgDismiss:YES] show];
    return tpView;
}



- (void)dealOtherSubviews:(NSArray *)subviews {}

- (void)customSetting {}

- (void)customLayoutSubs {}


- (void)dealloc {
    [self subDealloc];
#ifdef DEBUG
    NSString * logT = [NSString stringWithFormat:@"__dealloc__❤️❤️❤️❤️❤️❤️❤️❤️ %@",NSStringFromClass([self class])];
    LDLog(@"%@",logT);
#endif
}

- (void)subDealloc {}

@end


@implementation STNotFileDesignView

- (NSString *)xibFileName {
#ifdef DEBUG
    NSString * logT = [NSString stringWithFormat:@"请确认 [%@] 是否实现了 [- (NSString *)xibFileName)]",NSStringFromClass([self class])];
    LDLog(@"%@",logT);
#endif
    return nil;
}

- (NSInteger)indexInNib {
    return -1;
}

- (void)templateMethod {
    
    if (!self.subviews.count && !self.contentView && self.isLoad == NO) {
//    if (!self.contentView && self.isLoad == NO) {
        self.isLoad = YES;
        NSInteger nibIndex = [self indexInNib];
        NSString * componentNibName = [self xibFileName];
        if (nibIndex >= 0 && componentNibName) {
            
            NSBundle * bundle = [self.class selfBundle];
            
            NSArray * subNibViews = [bundle loadNibNamed:componentNibName owner:self options:nil];
            UIView * contentView = [subNibViews objectAtIndex:nibIndex];

            self.contentView = contentView;
            self.contentView.backgroundColor = self.backgroundColor;

            [self dealOtherSubviews:self.subviews];
            [self insertSubview:self.contentView atIndex:0];
            
        } else {
            LDLog(@"检查%@ 是否实现了<STNotFileDesignViewDelegate>的方法 可跳转父类查看详情",NSStringFromClass(self.class));
        }
    }
    
    [self customSetting];
}



@end

