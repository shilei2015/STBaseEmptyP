//
//  STXibDesignView.h
//  STBase
//
//  Created by 石磊 on 2022/11/1.
//

#import <UIKit/UIKit.h>
#import "UIView+STXib.h"

@class STXibDesignView;

NS_ASSUME_NONNULL_BEGIN

typedef void(^STXibDesignGetFrameHandle)(STXibDesignView *xibView);

@protocol STNotFileDesignViewDelegate <NSObject>

@required
///返回当前类所在的xib文件名
- (nullable NSString *)xibFileName;

///返回当前类所在的xib文件 中的 序号index  start from 0;
- (NSInteger)indexInNib;


@end




@interface STXibDesignView : UIView


@property (nonatomic,strong,readonly) UIView *contentView;

@property (nonatomic , copy) STXibDesignGetFrameHandle getFrameHandle;


///初始化设置 子类重写
- (void)dealOtherSubviews:(NSArray *)subviews;
- (void)customSetting;
- (void)customLayoutSubs;
- (void)subDealloc;


///测试快速展示
+ (instancetype)showWithSize:(CGSize)size;

@end


@interface STNotFileDesignView : STXibDesignView<STNotFileDesignViewDelegate>


@end



NS_ASSUME_NONNULL_END
