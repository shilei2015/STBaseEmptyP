//
//  STBaseVC.h
//  STBase
//
//  Created by 石磊 on 2022/11/1.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface STBaseVC : UIViewController


///Default is YES  修改需要在 生命周期viewDidAppear前处理
@property(nonatomic, assign) BOOL enableBackGesture;

///Default is NO  修改需要在 生命周期viewDidAppear前处理 点击空白自动隐藏键盘
@property (nonatomic , assign) BOOL enableEmptyHideKeyBoard;


@property(nonatomic, assign, readonly) BOOL isFirstDidAppear;
@property(nonatomic, assign, readonly) BOOL isFirstWillAppear;


///页面将要首次出现
- (void)viewWillFirstAppear;

///页面已经首次出现
- (void)viewDidFirstAppear;


///LDNavBar 点击了返回
- (void)backItemClicked;

///推出一个控制器
- (UIViewController *)pushVCWithClass:(Class)vcClass;
- (UIViewController *)pushVCWithClassName:(NSString *)className;


- (void)subDealloc;

@end

NS_ASSUME_NONNULL_END
