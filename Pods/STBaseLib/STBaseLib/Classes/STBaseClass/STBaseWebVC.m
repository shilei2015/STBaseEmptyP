//
//  STBaseWebVC.m
//  NoName
//
//  Created by 石磊 on 2023/2/27.
//

#import "STBaseWebVC.h"
#import <WebKit/WebKit.h>
#import "UIColor+STBase.h"
#import "MBProgressHUD+Hud.h"

@interface STBaseWebVC ()<WKNavigationDelegate,UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet WKWebView *webView;

@property (weak, nonatomic) IBOutlet UILabel *titleLb;


@property(nonatomic, copy) NSString *urlStr;

@property (nonatomic , copy) NSString * htmlString;


@end

@implementation STBaseWebVC

+ (instancetype)webWithTitle:(nullable NSString *)title url:(NSString *)url {
    STBaseWebVC * web = [self new];
    web.urlStr = url;
    web.title = title;
    
    return web;
}


+ (instancetype)webWithTitle:(nullable NSString *)title htmlString:(NSString *)htmlString {
    STBaseWebVC * web = [self new];
    web.htmlString = htmlString;
    web.title = title;
    
    return web;

}



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    self.bgColorHex = @"#0F0E13";
    self.textColorHex = @"#FFFFFF";
    
    self.titleLb.text = self.title;
    
    UIColor * bgColor = [UIColor colorWithHexString:self.bgColorHex];
    
    
    self.view.backgroundColor = bgColor;
    self.webView.navigationDelegate = self;
    self.webView.contentMode = UIViewContentModeScaleAspectFill;
    self.webView.backgroundColor = bgColor;
    self.webView.hidden = YES;
    self.webView.scrollView.delegate = self;
    self.webView.scrollView.bounces = NO;
    self.webView.scrollView.showsVerticalScrollIndicator = YES;
    self.webView.scrollView.showsHorizontalScrollIndicator = NO;
    
    
    if (self.showLoadingInView) {
        [MBProgressHUD showMessage:nil toView:self.view];
    }
    
    
    if (self.urlStr) {
        NSURL * url = [NSURL URLWithString:self.urlStr];
        NSURLRequest * request = [NSURLRequest requestWithURL:url];
        [self.webView loadRequest:request];
    } else if (self.htmlString) {
        [self loadHtmlString:self.htmlString];
    }
    
    
}

- (void)setTextColorHex:(NSString *)textColorHex {
    _textColorHex = textColorHex;
    [self loadHtmlString:self.htmlString];
}

- (void)setBgColorHex:(NSString *)bgColorHex {
    _bgColorHex = bgColorHex;
    [self loadHtmlString:self.htmlString];
}



- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return nil;
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    __weak typeof(self) bself = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        bself.webView.hidden = NO;
    });
    
    if (self.showLoadingInView) {
        [MBProgressHUD hideHUDForView:self.view];
    }
}


//MARK: - - - - - - - - - - - 本地html页面
- (void)loadHtmlString:(NSString *)htmlString {
    
    NSString *htmls = [NSString stringWithFormat:@"<html> \n"
                               "<head> \n"
                       "<meta content=\"width=device-width,initial-scale=1.0,user-scalable=false,minimum-scale=1.0, maximum-scale=1.0\" name=\"viewport\">"
                               "<style type=\"text/css\"> \n"
                       "* {width:100%%;box-sizing:border-box;}\n"
                       "body {margin:0;padding:10px;box-sizing:border-box;}\n"
                       "body,ul,p,div,h1,h2,h3,h4,h5,h6,li {background-color: %@ !important;color:%@ !important;}"
                               "</style> \n"
                               "</head> \n"
                               "<body>"
                               "%@</body>"
                               "</html>",self.bgColorHex,self.textColorHex,htmlString];

    [self.webView loadHTMLString:htmls baseURL:nil];
}


@end
