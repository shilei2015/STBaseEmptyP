//
//  MBProgressHUD+Hud.h
//  STBase
//
//  Created by 石磊 on 2022/11/1.
//

#import <MBProgressHUD/MBProgressHUD.h>


NS_ASSUME_NONNULL_BEGIN

@interface MBProgressHUD (Hud)

/// 提示
/// - Parameter tip: 提示的内容
+ (void)showTip:(NSString *)tip;

/// 在view上显示提示
+ (void)showTip:(NSString *)tip toView:(nullable UIView *)view;

///成功提示
+ (void)showSuccess:(NSString *)success;

///在view上显示成功提示
+ (void)showSuccess:(NSString *)success toView:(nullable UIView *)view;

///错误提示
+ (void)showError:(NSString *)error;

///在view上显示错误提示
+ (void)showError:(NSString *)error toView:(nullable UIView *)view;

///显示loading...
+ (void)showMessage:(nullable NSString *)message;

///在view上显示loading...
+ (void)showMessage:(nullable NSString *)message toView:(nullable UIView *)view;

///隐藏hud(window)
+ (void)hideHUD;

///隐藏在view上的hud
+ (void)hideHUDForView:(nullable UIView *)view;


@end

NS_ASSUME_NONNULL_END
