

#import "UIView+STLocalized.h"
#import <objc/runtime.h>
#import "STDebug.h"
#import <YYText/YYText.h>
#import <YYModel/YYModel.h>
#import <Masonry/Masonry.h>
#import "UILabel+STBase.h"

@interface STRangeModel ()
@property (nonatomic , assign , readwrite) NSRange matchRange;
@property (nonatomic , copy , readwrite) NSString * matchText;
@property (nonatomic , assign , readwrite) NSRange resultRange;
@property (nonatomic , copy , readwrite) NSString * resultText;
@property (nonatomic , assign , readwrite) NSInteger index;
@end

@implementation STRangeModel
@end



@implementation UIView (STLocalized)

@end

@interface UILabel (STLocalized)

@property (nonatomic , copy) NSString * tempText;

@property (nonatomic , strong) YYLabel * contentYYLB;

@end


IB_DESIGNABLE
@implementation UILabel (STLocalized)

- (void)setTempText:(NSString *)tempText {
    objc_setAssociatedObject(self, @selector(tempText), tempText, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (NSString *)tempText {
    return objc_getAssociatedObject(self, @selector(tempText));
}

- (void)setContentYYLB:(YYLabel *)contentYYLB {
    objc_setAssociatedObject(self, @selector(contentYYLB), contentYYLB, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (YYLabel *)contentYYLB {
    return objc_getAssociatedObject(self, @selector(contentYYLB));
}

- (void)setIsTextLocalize:(BOOL)isTextLocalize {
    objc_setAssociatedObject(self, @selector(isTextLocalize), @(isTextLocalize), OBJC_ASSOCIATION_COPY_NONATOMIC);
    if (isTextLocalize) {
        self.text = self.text.st_localized;
    }
}

- (BOOL)isTextLocalize {
    return [objc_getAssociatedObject(self, @selector(isTextLocalize)) boolValue];
}

- (void)config:(void(^)(STRangeModel * rangeM))rangeConfig action:(void(^)(STRangeModel * rangeM))tapAction {
    
    {
        if (self.text && self.text.length) {
            self.tempText = self.text;
            self.text = nil;
            self.userInteractionEnabled = YES;
        }
        
        
        if (self.tempText && self.tempText.length) {
            NSString * regExp = @"\\{.*?\\}";
            NSString * text = self.tempText.st_localized;
            NSError * error;
            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regExp options:0 error:&error];
            if (!error) {
                
                __block NSMutableArray <STRangeModel *>* rangeArr = [NSMutableArray new];
                __block NSMutableString * tempFullString = [NSMutableString stringWithString:text];
                
                [[regex matchesInString:text options:0 range:NSMakeRange(0, text.length)] enumerateObjectsUsingBlock:^(NSTextCheckingResult * _Nonnull checkingResult, NSUInteger idx, BOOL * _Nonnull stop) {
                    
                    NSString * rangeText = [text substringWithRange:checkingResult.range];
                    
                    NSString *regex3 = @"[\{|\}]";
                    NSRegularExpression *regularE3 = [[NSRegularExpression alloc] initWithPattern:regex3 options:NSRegularExpressionCaseInsensitive error:nil];
                    NSString *modifiedStr = [regularE3 stringByReplacingMatchesInString:rangeText options:NSMatchingReportProgress range:NSMakeRange(0, rangeText.length) withTemplate:@""].st_localized;
                    
                    STRangeModel * rangeM = [STRangeModel new];
                    rangeM.matchText = rangeText;
                    rangeM.matchRange = checkingResult.range;
                    {
                        NSRange tempRange = [tempFullString rangeOfString:rangeText];
                        [tempFullString replaceCharactersInRange:tempRange withString:modifiedStr];
                        
                        NSRange resultRange = [tempFullString rangeOfString:modifiedStr];
                        
                        rangeM.resultText = modifiedStr;
                        rangeM.resultRange = resultRange;
                    }
                    rangeM.index = rangeArr.count;
                    [rangeArr addObject:rangeM];
                }];
                
                {

                    if (!self.contentYYLB) {
                        YYLabel * btLabel = [YYLabel new];
                        btLabel.numberOfLines = self.numberOfLines;
                        btLabel.preferredMaxLayoutWidth = self.bounds.size.width;//设置最大宽度
                        [self addSubview:btLabel];
                        
                        [btLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                            make.top.left.bottom.right.mas_equalTo(0);
                        }];
                        
                        self.contentYYLB = btLabel;
                    }
                    
                    NSString * fString = tempFullString;
                    
                    NSMutableAttributedString *attrM = [[NSMutableAttributedString alloc] initWithString:fString];
                    attrM.yy_color = self.textColor;
                    attrM.yy_font = self.font;
                    attrM.yy_lineSpacing = self.lineSpace;
                    attrM.yy_alignment = self.textAlignment;
                    
                    for (STRangeModel * rangeM in rangeArr) {
                        
                        if (rangeConfig) {rangeConfig(rangeM);}
                        
                        UIColor * rangeColor = rangeM.rangeColor ?:self.textColor;
                        UIColor * rangeFont = rangeM.rangeFont ?:self.font;
                        
                        @try {
                            NSRange range = rangeM.resultRange;
                            if (rangeM.underLineStyle != NSUnderlineStyleNone) {
                                [attrM yy_setUnderlineStyle:rangeM.underLineStyle range:range];
                            }
                            
                            if (rangeM.strikethroughStyle != NSUnderlineStyleNone) {
                                [attrM yy_setStrikethroughStyle:rangeM.strikethroughStyle range:range];
                            }
                            
                            [attrM yy_setColor:rangeColor range:range];
                            [attrM yy_setFont:rangeFont range:range];
                            
                            if (tapAction) {
                                [attrM yy_setTextHighlightRange:range color:rangeColor backgroundColor:nil tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
                                    
                                    LDLog(@"clicked %@",[rangeM yy_modelToJSONObject]);
                                    tapAction(rangeM);
                                }];
                            }
                            
                        } @catch (NSException *exception) {
                            
                        } @finally {
                            
                        }
                        
                    }
                    
                    
                    self.contentYYLB.attributedText = attrM;
                    
                }
                
            } else {
                LDLog(@"NSRegularExpression error = %@",error);
            }

        }
        
    }
    
    /*
    static int a = 0;
    static int b = 0;
     
     a = 0;//0|1|3
     b = 0;//0|1|2|3
     

     if (b > 4) {
         b = 0;
         a += 1;
         if (a > 6) {
             a = 0;
         }
     }
     */
    
    /*
     NSRegularExpression：头文件的枚举
     typedef NS_OPTIONS(NSUInteger, NSRegularExpressionOptions) {
          NSRegularExpressionCaseInsensitive          = 1 << 0,   // 不区分大小写的
          NSRegularExpressionAllowCommentsAndWhitespace  = 1 << 1,   // 忽略空格和# (注释符)
          NSRegularExpressionIgnoreMetacharacters        = 1 << 2,   // 整体化
          NSRegularExpressionDotMatchesLineSeparators    = 1 << 3,   // 匹配任何字符，包括行分隔符
          NSRegularExpressionAnchorsMatchLines          = 1 << 4,   // 允许^和$在匹配的开始和结束行
          NSRegularExpressionUseUnixLineSeparators      = 1 << 5,   // (查找范围为整个的话无效)
          NSRegularExpressionUseUnicodeWordBoundaries    = 1 << 6    // (查找范围为整个的话无效)
          };
     这些枚举是针对表达式的，例如options选择为：NSRegularExpressionCaseInsensitive，那么表达式Pattern可以@"Yu"匹配字符串中的yu。
     */
    
    
    /*
     typedef NS_OPTIONS(NSUInteger, NSMatchingOptions)  {
       NSMatchingReportProgress         = 1 << 0, //找到最长的匹配字符串后调用block回调
       NSMatchingReportCompletion       = 1 << 1, //找到任何一个匹配串后都回调一次block
       NSMatchingAnchored               = 1 << 2, //从匹配范围的开始处进行匹配
       NSMatchingWithTransparentBounds  = 1 << 3, //允许匹配的范围超出设置的范围
       NSMatchingWithoutAnchoringBounds = 1 << 4  //禁止^和$自动匹配行还是和结束
         };
     */
    
    
    
}




@end


@implementation UIButton (STLocalized)


- (void)setIsTextLocalize:(BOOL)isTextLocalize {
    objc_setAssociatedObject(self, @selector(isTextLocalize), @(isTextLocalize), OBJC_ASSOCIATION_COPY_NONATOMIC);
    
    if (isTextLocalize) {

        NSString * normalString = [self titleForState:UIControlStateNormal];
        NSString * selectString = [self titleForState:UIControlStateSelected];
        
        [self setTitle:normalString.st_localized forState:UIControlStateNormal];
        [self setTitle:selectString.st_localized forState:UIControlStateSelected];

    }
}

- (BOOL)isTextLocalize {
    return [objc_getAssociatedObject(self, @selector(isTextLocalize)) boolValue];
}

@end










@implementation NSString (STLocalize)

- (NSString *)st_localized {
    NSString * backString = NSLocalizedString(self, nil);
#if DEBUG
//    {
//        NSString * localKey = @"local_key_value";
//        NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
//        NSDictionary * dict = [defaults objectForKey:localKey];
//        [defaults removeObjectForKey:localKey];
//
//        NSMutableDictionary * tempDict = [NSMutableDictionary dictionaryWithDictionary:dict];
//
//        if ([backString containsString:@" _OK"]) {
//            [tempDict removeObjectForKey:self];
//        } else {
//
//            NSString * result = backString;
//            if (backString.length) {
//                if ([backString containsString:@" _loc"]) {
//                    result = [NSString stringWithFormat:@"%@",backString];
//                } else {
//                    result = [NSString stringWithFormat:@"%@ _loc",backString];
//                }
//            }
//            [tempDict setObject:result forKey:self];
//            backString = result;
//        }
//        [defaults setObject:tempDict forKey:localKey];
//    }
#endif
    
    return backString;
}

+ (void)showLocalizedString {
    
    NSString * localKey = @"local_key_value";
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary * dict = [defaults objectForKey:localKey];
    
    __block NSMutableString * tempString = [NSMutableString new];
    
    NSArray * sourtKeys = [[dict allKeys] sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        return [obj1 compare:obj1 options:NSLiteralSearch];
    }];
    
    for (NSString * key in sourtKeys) {
        NSString * value = dict[key];
        
        NSString * str = [NSString stringWithFormat:@"\"%@\" = \"%@\";\n",key,value];
        [tempString appendString:str];
    }
    
    LDLog(@"🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨🟨 \n%@",tempString);
}



- (NSString *)st_documentLocalizedReplaceTagsValues:(NSArray <NSString *>*)values {
    
    NSString * tempString = [[NSString alloc] initWithString:self.st_localized];
    
    NSInteger count = values.count;
    
    for (int i = 0; i < count; i++) {
        NSString * tag = [NSString stringWithFormat:@"{%@}",values[i]];
        NSString * replaceValue = [NSString stringWithFormat:@"%@",values[i].st_localized];
        tempString = [tempString stringByReplacingOccurrencesOfString:tag withString:replaceValue];
    }
    
    return tempString;
}


- (NSString *)st_documentLocalizedReplaceTags:(NSArray <NSString *>*)tags withValues:(NSArray <NSString *>*)values {
    
    NSString * tempString = [[NSString alloc] initWithString:self.st_localized];
    
    NSInteger count = 0;
    if (values.count < tags.count) {
        count = values.count;
    } else {
        count = tags.count;
    }
    
    for (int i = 0; i < count; i++) {
        NSString * tag = [NSString stringWithFormat:@"%@",tags[i]];
        NSString * replaceValue = [NSString stringWithFormat:@"%@",values[i]];
        tempString = [tempString stringByReplacingOccurrencesOfString:tag withString:replaceValue];
    }
    
    return tempString;
}









#pragma mark DEBUG TEST
+ (void)testttttttt {
    
    NSMutableString * str = [[NSMutableString alloc] initWithString:@"Are you sure you want to block {nickname} and {nickname2}?".st_localized];
    
    NSString *regExp = @"{.*?}";
    
    {
        NSString *temp = [NSString stringWithFormat:@"%@",str];
        NSError * error;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regExp options:NSRegularExpressionCaseInsensitive error:&error];
        
        if (!error) {
            [[regex matchesInString:temp options:0 range:NSMakeRange(0, [temp length])] enumerateObjectsUsingBlock:^(NSTextCheckingResult * _Nonnull checkingResult, NSUInteger idx, BOOL * _Nonnull stop) {
                NSString * needReplaceString = [temp substringWithRange:checkingResult.range];
                LDLog(@"needReplaceString = %@",needReplaceString);
            }];
        } else {
            LDLog(@"NSRegularExpression error = %@",error);
        }
        
    }
    
    {
        NSError *error;
        NSRegularExpression *regular = [NSRegularExpression regularExpressionWithPattern:@"[a-z]{3}"
                                        options:0 error:&error];
        NSString *string = @"abcv1asdf";
        [regular enumerateMatchesInString:string options:NSMatchingReportCompletion
        range:NSMakeRange(0, string.length)
        usingBlock:^(NSTextCheckingResult * _Nullable result, NSMatchingFlags flags, BOOL * _Nonnull stop) {
            LDLog(@"range = %@ str = %@ flag = %lu", NSStringFromRange(result.range),
                     [string substringWithRange:result.range], (unsigned long)flags);
        }];
    }
}


@end
