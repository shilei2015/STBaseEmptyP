//
//  NSObject+STBase.m
//  STBase
//
//  Created by 石磊 on 2022/11/1.
//

#import "NSObject+STBase.h"
#import "NSString+STBase.h"
#import <objc/runtime.h>
#import <YYModel/YYModel.h>
#import "STDebug.h"

static NSString *const STResetEventHandleIdentifer = @"STResetEventHandle";
static NSString * const DefaultHandleIdentifer = @"default_ST_normalHandle";


@interface NSObject (STBase)

@property (nonatomic,strong) NSMutableDictionary *handleMap;

@end

@implementation NSObject (STBase)

- (void)setTempModel:(id)tempModel {
    objc_setAssociatedObject(self, @selector(tempModel), tempModel, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (id)tempModel {
    return objc_getAssociatedObject(self, @selector(tempModel));
}


- (void)setParentController:(STParentVCHandle)parentController {
    objc_setAssociatedObject(self, @selector(parentController), parentController, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (STParentVCHandle)parentController {
    return objc_getAssociatedObject(self, @selector(parentController));
}


- (void)setHandleMap:(NSMutableDictionary *)handleMap {
    objc_setAssociatedObject(self, @selector(handleMap), handleMap, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (NSMutableDictionary *)handleMap {
    return objc_getAssociatedObject(self, @selector(handleMap));
}


- (void)addHandle:(HandleBlock)handle withIdentifer:(NSString *)identifier {
    if (!self.handleMap) {
        self.handleMap = [NSMutableDictionary dictionary];
    }
    if (handle) {
        [self.handleMap setObject:handle forKey:identifier];
    }
}
- (HandleBlock)handleWithIdentifier:(NSString *)identifier {
    
    HandleBlock temp = self.handleMap[identifier];
    if (!temp) {
        LDLog(@"\n⚠️⚠️⚠️⚠️请检查[%@ handleWithIdentifier:%@]是否有调用",NSStringFromClass(self.class),identifier);
    }
    return temp;
}


//MARK: - - - - - - - - - - - 暴露出来的方法
- (void)configHandleBlock:(nullable HandleBlock)handleBlock {
    [self addHandle:handleBlock withIdentifer:DefaultHandleIdentifer];
}

- (HandleBlock)handleBlock {
    return [self handleWithIdentifier:DefaultHandleIdentifer];
}


- (void)setDefaultHandleBlock:(HandleBlock)defaultHandleBlock {
    [self configHandleBlock:defaultHandleBlock];
}

- (HandleBlock)defaultHandleBlock {
    return [self handleBlock];
}

//(id _Nullable objself,NSInteger handleType,id _Nullable obj,NSString * _Nullable des)
- (void)handleBackType:(NSInteger)type obj:(nullable id)obj des:(nullable NSString *)des {
    
    if (self.defaultHandleBlock) {
        self.defaultHandleBlock(self, type, obj, des);
    }
    
}

- (void)cancelAllEnvent {
    [[self class] cancelPreviousPerformRequestsWithTarget:self];
}

- (void)resetEventTimeInTime:(NSTimeInterval)inTime handle:(HandleBlock)handle {
    [self addHandle:handle withIdentifer:STResetEventHandleIdentifer];
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [self performSelector:@selector(resetHandleMethod) withObject:nil afterDelay:inTime];
}

- (void)resetHandleMethod {
    HandleBlock handle = [self handleWithIdentifier:STResetEventHandleIdentifer];
    if (handle) {
        handle(self,1,nil,nil);
    }
}


- (void)st_everyTime:(NSTimeInterval)time doSome:(void(^)(void))doSome {
    [self resetEventTimeInTime:time handle:^(id  _Nullable objself, NSInteger handleType, id  _Nullable obj, NSString * _Nullable des) {
        [objself st_everyTime:time doSome:doSome];
    }];
}




+ (void)exchangeSelector:(SEL)originSel enchangeSel:(SEL)exchangeSel {
    SEL orignsel  = originSel;
    SEL exchgesel = exchangeSel;

    Method originalM  = class_getInstanceMethod([self class], orignsel);
    Method exchangeM  = class_getInstanceMethod([self class], exchgesel);

    BOOL didAddMethod = class_addMethod([self class], orignsel, method_getImplementation(exchangeM), method_getTypeEncoding(exchangeM));
    if (didAddMethod) {
        class_replaceMethod([self class], exchgesel, method_getImplementation(originalM), method_getTypeEncoding(originalM));
    } else {
        method_exchangeImplementations(originalM, exchangeM);
    }
}


@end



@implementation NSObject (STArchive)


- (void)encodeWithCoder:(NSCoder*)aCoder {
    [self yy_modelEncodeWithCoder:aCoder];
}

- (id)initWithCoder:(NSCoder*)aDecoder {
    return [self yy_modelInitWithCoder:aDecoder];
}

// 归档
- (BOOL)saveToPath:(NSString *)path {
    LDLog(@"归档  path -- %@",path);
    NSString * file = [path documentsAppend];
    BOOL success = [NSKeyedArchiver archiveRootObject:self toFile:file];
    return success;
}


// 解档
+ (NSObject *)objectAtPath:(NSString *)path {
    LDLog(@"读档  path -- %@",path);
    NSString * file = [path documentsAppend];
    id decodedBag = [NSKeyedUnarchiver unarchiveObjectWithFile:file];
    return decodedBag;
}

+ (BOOL)deleteItemAtPath:(NSString *)path {
    LDLog(@"删除  path -- %@",path);
    NSString * file = [path documentsAppend];
    NSError * error;
    BOOL success = [[NSFileManager defaultManager] removeItemAtPath:file error:&error];
    if (error) {
        LDLog(@"删除失败 == %@",error.description);
    }
    return success;
}



@end


@implementation NSObject (Keyboard)

+ (void)configKeyboardWithBottomCons:(NSLayoutConstraint *)bottomCons layoutView:(UIView *)layoutView {
    [self configKeyboardWithBottomCons:bottomCons layoutView:layoutView showMargin:0];
}

+ (void)configKeyboardWithBottomCons:(NSLayoutConstraint *)bottomCons layoutView:(UIView *)layoutView showMargin:(CGFloat)showMargin {
    
    CGFloat hideMargin = bottomCons.constant;
    
    [[NSNotificationCenter defaultCenter] addObserverForName:UIKeyboardWillShowNotification object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        CGFloat curkeyBoardHeight = [[[note userInfo] objectForKey:@"UIKeyboardBoundsUserInfoKey"] CGRectValue].size.height;
        CGFloat keyBoardHeight = curkeyBoardHeight + showMargin;
        NSTimeInterval duration = [[[note userInfo] objectForKey:@"UIKeyboardAnimationDurationUserInfoKey"] floatValue];
        
        
        [UIView animateWithDuration:duration animations:^{
            bottomCons.constant = keyBoardHeight;
            [layoutView layoutIfNeeded];
        }];
        
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:UIKeyboardWillHideNotification object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        NSTimeInterval duration = [[[note userInfo] objectForKey:@"UIKeyboardAnimationDurationUserInfoKey"] floatValue];
        
        [UIView animateWithDuration:duration animations:^{
            bottomCons.constant = hideMargin;
            [layoutView layoutIfNeeded];
        }];
        
    }];
}





@end






@implementation EndMark

+ (instancetype)end {
    static id instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[[self class] alloc] init];
    });
    return instance;
}
@end



@implementation NSObject (Utils)
+ (id)performSelector:(SEL)aSelector withObjects:(id)object, ... {
    //获取方法签名
    NSMethodSignature *signature = [[self class] instanceMethodSignatureForSelector:aSelector];
    if (!signature) {
        NSAssert(NO,@"未找到%@ 方法",NSStringFromSelector(aSelector));
    }
    //使用NSInvocation包装方法
    NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:signature];
    //设置方法调用者
    invocation.target = self;
    // 设置方法的SEL
    invocation.selector = aSelector;
    //获取除去self,_cmd以外的参数个数
    NSInteger paramsCount = signature.numberOfArguments - 2;
    // 设置参数
    va_list params;
    va_start(params, object);
    int i = 0;
    //[EndMark end]为自定义的结束符号,仅此而已,从而使的为该方法可以接收 nil 做为参数
    for (id tmpObject = object; (id)tmpObject != [EndMark end]; tmpObject=va_arg(params, id)) {
        //防止越界
        if (i >= paramsCount) break;
        // 去掉 self, cmd所以从 2 开始
        [invocation setArgument:&tmpObject atIndex:i +2];
        i++;
    }
    va_end(params);
    //调用方法
    [invocation invoke];
    //获取返回值
    id returnValue = nil;
    if (signature.methodReturnType) {
        [invocation getReturnValue:&returnValue];
    }
    return returnValue;
    
}


@end
