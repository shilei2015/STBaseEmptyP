//
//  NSMutableDictionary+STBase.h
//  STTempPods
//
//  Created by 石磊 on 2022/3/21.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSMutableDictionary (STBase)

/** 如果anobject不存在就将其替换为 "" 加到dict里面 */
- (void)sl_setObject:(nullable id)anObject forKey:(NSString *)aKey;

@end


@interface NSDictionary (STBase)

- (NSString *)st_jsonStringEncode;


+ (NSDictionary *)st_dictFromJson:(NSString *)jsonString;

@end

NS_ASSUME_NONNULL_END
