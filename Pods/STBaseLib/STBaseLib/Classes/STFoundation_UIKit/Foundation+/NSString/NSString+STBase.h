//
//  NSString+STBase.h
//  STTempPods
//
//  Created by 石磊 on 2022/4/11.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (STBase)

+ (NSString *)nonNull:(NSString *)string;
+ (NSString *)nonNull:(NSString *)string placeHolder:(NSString *)placeHolder;


- (NSString *)urlEncodedString;
- (NSString *)urlDecodeString;
- (NSString *)MD5String;
- (nullable NSString *)base64String;


+ (NSString *)string:(NSString *)preStr append:(NSString *)appendStr by:(NSString *)byStr;
+ (NSString *)joinedByString:(NSString *)join componentStrings:(NSString *)string,... NS_REQUIRES_NIL_TERMINATION;


///计算获取行高
- (CGFloat)lineHeightWithFont:(UIFont *)font width:(CGFloat)width;

@end

@interface NSString (Path)
- (NSString *)documentsAppend;
@end


@interface NSString (AppInfo)
+ (NSString *)appName;
+ (NSString *)appVersion;
+ (NSString *)appBuildVersion;
+ (NSString *)appBundleID;
+ (NSString *)appIcon;
///经过处理的数据只有 “-”前的代码  zh
+ (NSString *)sysLang;
///获取到的系统语言 原样 ：zh_Hans_CN
+ (NSString *)originSysLang;

@end


@interface NSString (AppStore)

+ (NSString *)appStoreReciptString;

@end

@interface NSString (Json)

- (NSData *)toData;
- (nullable NSDictionary *)jsonObject;

@end

@interface NSString (OSSImageUrl)

///根据图片质量获取图片
- (NSString *)qualityImage:(float)quality;

///根据大小获取缩放裁剪后的图片
- (NSString *)sizeImage:(CGSize)size;

- (NSString *)sizeImage:(CGSize)size scale:(float)scale;

///200*200*
- (NSString *)smallImage;

/// ScreenWidth * ScreenWidth
- (NSString *)middleImage;


@end


NS_ASSUME_NONNULL_END


/*
@interface NSString (Template)

@end
 */
