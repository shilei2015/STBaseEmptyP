//
//  NSString+STBase.m
//  STTempPods
//
//  Created by 石磊 on 2022/4/11.
//

#import "NSString+STBase.h"
#import <CommonCrypto/CommonCrypto.h>
#import "NSArray+STBase.h"
#import "UIDevice+STBase.h"

@implementation NSString (STBase)

+ (NSString *)nonNull:(NSString *)string {
    if (!string || [string isKindOfClass:[NSNull class]]) {
        return @"";
    }
    return string;
}

+ (NSString *)nonNull:(NSString *)string placeHolder:(NSString *)placeHolder {
    if (!string || [string isKindOfClass:[NSNull class]]) {
        return placeHolder;
    }
    return string;
}


+ (NSString *)string:(NSString *)preStr append:(NSString *)appendStr by:(NSString *)byStr {
    
    NSString * str = @"";
    if ([NSString nonNull:preStr].length) {
        str = preStr;
    }
    if ([NSString nonNull:appendStr].length) {
        if (str.length) {
            str = [str stringByAppendingFormat:@"%@%@",byStr,appendStr];
        } else {
            str = appendStr;
        }
    }
    
    return str;
}

//// urlencode
//- (NSString *)urlEncodedString {
//    NSString *encodedString = (NSString *)
//    CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
//                                                              (CFStringRef)self,
//                                                              (CFStringRef)@"!$&'()*+,-./:;=?@_~%#[]",
//                                                              NULL,
//                                                              kCFStringEncodingUTF8));
//    return encodedString;
//}
//
//// urldecode
//- (NSString *)urlDecodeString {
//    NSString *decodedString  = (__bridge_transfer NSString *)CFURLCreateStringByReplacingPercentEscapesUsingEncoding(NULL,
//                                                                                                                     (__bridge CFStringRef)self,
//                                                                                                                     CFSTR(""),
//                                                                                                                     CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding));
//    return decodedString;
//}


// urlencode
- (NSString *)urlEncodedString {
    NSCharacterSet *encodeSet = [NSCharacterSet characterSetWithCharactersInString:@"!*'();:@&=+$,/?%#[]"];
    NSString *encode = [self stringByAddingPercentEncodingWithAllowedCharacters:encodeSet];//编码
    return encode;
}

// urldecode
- (NSString *)urlDecodeString {
    NSString *decode = [self stringByRemovingPercentEncoding];//解码
    return decode;
}



- (NSString *)MD5String {
    
    const char *concat_str = [self UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(concat_str, (unsigned int)strlen(concat_str), result);
    NSMutableString *hash = [NSMutableString string];
    for( int i = 0; i < 16; i++){
        [hash appendFormat:@"%02X", result[i]];
    }
    return hash;
}

- (NSString *)base64String {
    if (self) {
        NSData *data = [self dataUsingEncoding:NSUTF8StringEncoding];
        NSData *base64Data = [data base64EncodedDataWithOptions:0];
        NSString *baseString = [[NSString alloc]initWithData:base64Data encoding:NSUTF8StringEncoding];
        return baseString;
    }
    return nil;
}



- (CGFloat)lineHeightWithFont:(UIFont *)font width:(CGFloat)width {
    CGSize size = [self boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil].size;

    return size.height;
}


@end


@implementation NSString (Path)
-(NSString *)documentsAppend {
    NSString *documents = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    NSString *str = [NSString stringWithFormat:@"%@/caches",documents];

    // 判断临时存储文件夹是否存在，不存在就创建
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if(![fileManager fileExistsAtPath:str]) {
        [fileManager createDirectoryAtPath:str withIntermediateDirectories:YES attributes:nil error:nil];
    }
    return [str stringByAppendingPathComponent:self];
}


@end


@implementation NSString (AppInfo)


+ (NSString *)appName {
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"];
}

+ (NSString *)appVersion {
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
}

+ (NSString *)appBuildVersion {
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
}

+ (NSString *)appBundleID {
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"];
}

+ (NSString *)appIcon {
    NSDictionary * infoPlist = [[NSBundle mainBundle] infoDictionary];
    NSString *icon = [[infoPlist valueForKeyPath:@"CFBundleIcons.CFBundlePrimaryIcon.CFBundleIconFiles"] lastObject];
    return icon;
}


+ (NSString *)sysLang {
    //判断当前系统语言
    NSString *language = [NSString originSysLang];;
    
    NSArray  *array = [language componentsSeparatedByString:@"-"];
    
    NSString *currentSysLanguage = array.firstObject;
    
    return currentSysLanguage;
}

+ (NSString *)originSysLang {
    NSString *language = [NSLocale preferredLanguages].firstObject;    
    return language;
}


+ (NSString *)joinedByString:(NSString *)join componentStrings:(NSString *)string,... {
    
    
    va_list params;//定义一个指向个数可变的参数列表指针
    
    va_start(params, string);//va_start 得到第一个可变参数地址
    NSString *arg;
    if (string) {
        //将第一个参数添加到array
        
        NSMutableArray * tempArr = [NSMutableArray new];
        id prev = string;
        
        [tempArr sl_addSkipNolengString:prev];
        //va_arg 指向下一个参数地址
        while ((arg = va_arg(params, NSString *))) {
            [tempArr sl_addSkipNolengString:arg];
        }
        //置空
        va_end(params);
        
        NSString * text = [tempArr componentsJoinedByString:join];
        return text;
    }
    return @"";
}






@end



@implementation NSString (AppStore)

+ (NSString *)appStoreReciptString {
    NSURL * receiptUrl = [[NSBundle mainBundle] appStoreReceiptURL];
    NSData* data = [NSData dataWithContentsOfURL:receiptUrl];
    NSString * reciptStr = [data base64EncodedStringWithOptions:0];
    
    return reciptStr;
}



@end


@implementation NSString (Json)

- (NSData *)toData {
    NSData * data = [self dataUsingEncoding:NSUTF8StringEncoding];
    return data;
}

- (nullable NSDictionary *)jsonObject {
    NSData * jsonData = [self toData];
    NSDictionary * jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:NULL];
    return jsonObject;
}

@end


@implementation NSString (OSSImageUrl)

- (NSString *)qualityImage:(float)quality {
    return [self stringByAppendingFormat:@"?x-oss-process=image/quality,q_%d",(int)(quality*100)];
}

- (NSString *)sizeImage:(CGSize)size {
    return [self stringByAppendingFormat:@"?x-oss-process=image/resize,m_fill,h_%d,w_%d",(int)size.height,(int)size.width]; ;
}

- (NSString *)sizeImage:(CGSize)size scale:(float)scale {
    return [self stringByAppendingFormat:@"?x-oss-process=image/resize,m_fill,h_%d,w_%d",(int)(size.height*scale),(int)(size.width*scale)];
}

- (NSString *)smallImage {
    return [self stringByAppendingFormat:@"?x-oss-process=image/resize,m_fill,h_%d,w_%d",100,100]; ;
}

- (NSString *)middleImage {
    return [self stringByAppendingFormat:@"?x-oss-process=image/resize,m_fill,h_%d,w_%d",(int)UIDevice.width,(int)UIDevice.width]; ;
}


//- (NSString *)middleImage {
//    return [self stringByAppendingFormat:@"?x-oss-process=image/resize,m_fill,h_%d,w_%d",(int)UIDevice.width,(int)UIDevice.width]; ;
//}


@end


/*

 @interface NSString (Template)

 @end

 @implementation NSString (Template)

 @end
 


 */



