//
//  NSMutableAttributedString+STBase.m
//  LDSDK
//
//  Created by 石磊 on 2023/2/3.
//

#import "NSMutableAttributedString+STBase.h"

@implementation NSMutableAttributedString (STBase)

+ (instancetype)attrStringWith:(NSString *)text color:(UIColor *)color font:(UIFont *)font kern:(CGFloat)kern lineSpacing:(CGFloat)lineSpacing alignment:(NSTextAlignment)alignment {
    
    if (!text) {
        return [[NSMutableAttributedString alloc] initWithString:@""];
    }
    NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:text];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    if (lineSpacing) {
        paragraphStyle.lineSpacing = lineSpacing;
    }
    paragraphStyle.alignment = alignment;
    paragraphStyle.lineBreakMode = NSLineBreakByCharWrapping;
    
    NSRange allTextRange = NSMakeRange(0, text.length);
    
    [attr addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:allTextRange];
    
    [attr addAttribute:NSForegroundColorAttributeName value:color range:allTextRange];
    [attr addAttribute:NSFontAttributeName value:font range:allTextRange];
    [attr addAttribute:NSKernAttributeName value:@(kern) range:allTextRange];
    [attr addAttribute:NSFontAttributeName value:font range:allTextRange];
    
    
    return attr;
}

@end
