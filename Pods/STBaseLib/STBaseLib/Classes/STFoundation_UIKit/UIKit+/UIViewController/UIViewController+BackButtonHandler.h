
#import <UIKit/UIKit.h>

@protocol BackButtonHandlerProtocol <NSObject>

@optional
/// 准守协议 - 点击系统返回 在该方法中处理事件
-(BOOL)navigationShouldPopOnBackButton;

@end


@interface UINavigationController (STComm)

///推出一个控制器 并将前面的skip个控制器移除
- (void)pushViewController:(UIViewController *)vc animated:(BOOL)animated skip:(NSInteger)skip;

///Pop返回 并跳过前面的 popCount个控制器
- (void)popViewCOntrollerWithCount:(NSInteger)popCount animated:(BOOL)animated;

@end


@interface UIViewController (STComm)


///获取当前控制器
+ (UIViewController*)currentViewController;

///获取KeyWindow
+ (UIWindow *)keyWindow;

///通过ClassName创建一个控制器
+ (UIViewController *)controllerFromClassName:(NSString *)className;

@end



