
#import "UIViewController+BackButtonHandler.h"
#import <objc/runtime.h>
#import "NSObject+STBase.h"



@implementation UINavigationController (ShouldPopOnBackButton)

+ (void)load {
    Method originalMethod = class_getInstanceMethod([self class], @selector(navigationBar:shouldPopItem:));
    Method overloadingMethod = class_getInstanceMethod([self class], @selector(overloaded_navigationBar:shouldPopItem:));
    method_setImplementation(originalMethod, method_getImplementation(overloadingMethod));
}


- (BOOL)overloaded_navigationBar:(UINavigationBar *)navigationBar shouldPopItem:(UINavigationItem *)item {

    if([self.viewControllers count] < [navigationBar.items count]) {
        return YES;
    }

    BOOL shouldPop = YES;
    UIViewController* vc = [self topViewController];
    SEL selector = @selector(navigationShouldPopOnBackButton);
    if([vc respondsToSelector:selector]) {
        shouldPop = [[vc performSelector:selector] boolValue];
    }

    if(shouldPop) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self popViewControllerAnimated:YES];
        });
    } else {
        // Workaround for iOS7.1. Thanks to @boliva - http://stackoverflow.com/posts/comments/34452906
        for(UIView *subview in [navigationBar subviews]) {
            if(0. < subview.alpha && subview.alpha < 1.) {
                [UIView animateWithDuration:.25 animations:^{
                    subview.alpha = 1.;
                }];
            }
        }
    }

    return NO;
}


@end


@implementation UINavigationController (STBase)

- (void)pushViewController:(UIViewController *)vc animated:(BOOL)animated skip:(NSInteger)skip {
    
    if (!vc) return;
    
    NSMutableArray <UIViewController *>* tempArr = [NSMutableArray arrayWithArray:self.viewControllers];
    
    if (skip >= tempArr.count) {
        [tempArr removeAllObjects];
        [tempArr addObject:self.viewControllers.firstObject];
    } else {
        [tempArr removeObjectsInRange:NSMakeRange(tempArr.count - skip, skip)];
    }
    vc.hidesBottomBarWhenPushed = YES;
    [tempArr addObject:vc];
    [self setViewControllers:tempArr animated:animated];
}

- (void)popViewCOntrollerWithCount:(NSInteger)popCount animated:(BOOL)animated {
    
    NSMutableArray <UIViewController *>* tempArr = [NSMutableArray arrayWithArray:self.viewControllers];
    
    if (popCount >= tempArr.count) {
        [self popToRootViewControllerAnimated:animated];
        return;
    } else {
        [tempArr removeObjectsInRange:NSMakeRange(tempArr.count - 1-popCount, popCount+1)];
    }
    
    [self setViewControllers:tempArr animated:animated];
    
}


@end




@implementation UIViewController (STComm)


+ (UIViewController *)viewControllerOfView:(UIView *)view {
    // 遍历响应者链。返回第一个找到视图控制器
    UIResponder *responder = view;
    while ((responder = [responder nextResponder])){
        if ([responder isKindOfClass: [UIViewController class]]) {
            return (UIViewController *)responder;
        }
    }
    // 如果没有找到则返回nil
    return nil;
}

//获取当前控制器
+ (UIViewController*)currentViewController {
    UIViewController * curVC = [self currentViewControllerWithRootViewController:[self keyWindow].rootViewController];
    return curVC;
}

//获取KeyWindow
+ (UIWindow *)keyWindow {
    return [UIApplication sharedApplication].windows.firstObject;
}

+ (UIViewController*)currentViewControllerWithRootViewController:(UIViewController*)rootViewController {
    if (rootViewController.presentedViewController) {
        UIViewController* presentedViewController = rootViewController.presentedViewController;
        return [self currentViewControllerWithRootViewController:presentedViewController];
    } else if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController* navigationController = (UINavigationController*)rootViewController;
        return [self currentViewControllerWithRootViewController:navigationController.visibleViewController];
    } else if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController *tabBarController = (UITabBarController *)rootViewController;
        return [self currentViewControllerWithRootViewController:tabBarController.selectedViewController];
    } else {
        return rootViewController;
    }
}


+ (UIViewController *)controllerFromClassName:(NSString *)className {
    UIViewController * startVC = [NSClassFromString(className) new];
    if (!startVC) {
        startVC = [UIViewController new];
        
        NSString * text = [NSString stringWithFormat:@"请配置【className = %@】的控制器",className];
        UILabel * label = [UILabel new];
        label.text = text;
        label.textAlignment = NSTextAlignmentCenter;
        label.numberOfLines = 0;
        label.frame = [UIScreen mainScreen].bounds;
        
        [startVC.view addSubview:label];
        
    }
    return startVC;
}


@end


