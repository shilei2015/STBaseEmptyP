//
//  UIImage+STSize.h
//  Asocial
//
//  Created by 石磊 on 2022/11/2.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImage (STSize)

///根据图片尺寸压缩图片
- (UIImage *)compressionWithMaxImageSize:(CGSize)maxImageSize;

///根据图片文件大小压缩图片 (可能会有点偏差 但不会太大)
- (UIImage *)compressImageQualityToByte:(NSInteger)maxLength;

@end

NS_ASSUME_NONNULL_END
