//
//  UIColor+STBase.h
//  STTempPods
//
//  Created by 石磊 on 2022/3/22.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIColor (STBase)

+ (UIColor *)randomColor;
+ (UIColor *)colorWithHexString:(NSString *)hexString;
+ (NSString *)colorHexFromUIColor:(UIColor*)color;

- (UIColor *)alphaColor:(float)alpha;
+ (UIColor *)mostColor:(UIImage *)image;
- (UIImage *)colorImageWithSize:(CGSize)size;
@end

NS_ASSUME_NONNULL_END
