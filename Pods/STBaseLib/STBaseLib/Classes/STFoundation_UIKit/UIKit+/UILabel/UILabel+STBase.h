

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UILabel (STBase)

@property (nonatomic , assign) IBInspectable NSInteger lineSpace;


- (void)middleLineStyle;

- (void)underLineStyle;

@end

NS_ASSUME_NONNULL_END
