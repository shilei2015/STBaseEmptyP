

#import "UILabel+STBase.h"
#import "NSString+STBase.h"
#import <objc/runtime.h>

@implementation UILabel (STBase)


+ (void)load {
    Method systemMethod = class_getInstanceMethod([self class], @selector(setText:));
    Method customMethod = class_getInstanceMethod([self class], @selector(st_setText:));
    method_exchangeImplementations(systemMethod, customMethod);
}


- (void)st_setText:(NSString *)text {
    [self st_setText:[NSString nonNull:text]];
    if (self.lineSpace > 0) {
        [self configLineSpace:self.lineSpace];
    }
}

- (void)middleLineStyle {
    //中划线
    NSDictionary *attribtDic = @{NSStrikethroughStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle]};
    NSMutableAttributedString *attribtStr = [[NSMutableAttributedString alloc]initWithString:self.text attributes:attribtDic];
    
   //赋值
   self.attributedText = attribtStr;
    
}

- (void)underLineStyle {
    // 下划线
    NSDictionary *attribtDic = @{NSUnderlineStyleAttributeName:[NSNumber numberWithInteger:NSUnderlineStyleSingle]};
    NSMutableAttributedString *attribtStr = [[NSMutableAttributedString alloc]initWithString:self.text attributes:attribtDic];
     
    //赋值
    self.attributedText = attribtStr;
}



- (void)setLineSpace:(NSInteger)lineSpace {
    [self configLineSpace:lineSpace];
    objc_setAssociatedObject(self, @selector(lineSpace), @(lineSpace), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSInteger)lineSpace {
    return [objc_getAssociatedObject(self, @selector(lineSpace)) integerValue];
}

- (void)configLineSpace:(NSInteger)lineSpace {
    
    if (self.text && self.text.length) {
        NSString * text = self.text;
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:text];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
        [paragraphStyle setLineSpacing:lineSpace];
        [paragraphStyle setAlignment:self.textAlignment];
        
        
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, text.length)];
        
        self.attributedText = attributedString;
        
        [self sizeToFit];
    } else {
        self.attributedText = nil;
    }
}




@end
