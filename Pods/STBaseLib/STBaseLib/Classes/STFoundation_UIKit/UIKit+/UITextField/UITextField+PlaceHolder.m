//
//  UITextField+PlaceHolder.m
//  SDKTestProject
//
//  Created by 石磊 on 2023/1/6.
//

#import "UITextField+PlaceHolder.h"
#import <objc/runtime.h>
#import "NSObject+STBase.h"



@implementation UITextField (PlaceHolder)

+ (void)load {
    SEL orignsel  = @selector(setPlaceholder:);
    SEL exchgesel = @selector(st_setPlaceholder:);
    [NSObject exchangeSelector:orignsel enchangeSel:exchgesel];
}

- (void)setPlaceHolderColor:(UIColor *)placeHolderColor {
    objc_setAssociatedObject(self, @selector(placeHolderColor), placeHolderColor, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    if (self.placeholder && self.placeHolderColor) {
        self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.placeholder attributes:@{NSForegroundColorAttributeName: placeHolderColor}];
    }
}

- (UIColor *)placeHolderColor {
    return objc_getAssociatedObject(self, @selector(placeHolderColor));
}

- (void)st_setPlaceholder:(NSString *)placeholder {
    [self st_setPlaceholder:placeholder];
    if (self.placeholder && self.placeHolderColor) {
        self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.placeholder attributes:@{NSForegroundColorAttributeName: self.placeHolderColor}];
    }
}



@end









@implementation UITextField (STKeyboardDeal)

- (void)configKeyboardWithBottomCons:(NSLayoutConstraint *)bottomCons {
    
    
    UITapGestureRecognizer * resignFirstResponderGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(resignFirstResponder)];
    
    resignFirstResponderGesture.cancelsTouchesInView = NO;
    [resignFirstResponderGesture setDelegate:self];
    resignFirstResponderGesture.enabled = YES;
    
    
    __weak typeof(self) belf = self;
    
    [[NSNotificationCenter defaultCenter] addObserverForName:UIKeyboardWillShowNotification object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        
        [belf.window addGestureRecognizer:resignFirstResponderGesture];
        
        
        CGFloat curkeyBoardHeight = [[[note userInfo] objectForKey:@"UIKeyboardBoundsUserInfoKey"] CGRectValue].size.height;
        CGFloat keyBoardHeight = curkeyBoardHeight;
        NSTimeInterval duration = [[[note userInfo] objectForKey:@"UIKeyboardAnimationDurationUserInfoKey"] floatValue];
        
        [UIView animateWithDuration:duration animations:^{
            bottomCons.constant = keyBoardHeight;
            [bottomCons.firstItem layoutIfNeeded];
        }];
        
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:UIKeyboardWillHideNotification object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        
        [belf.window removeGestureRecognizer:resignFirstResponderGesture];
        
        
        NSTimeInterval duration = [[[note userInfo] objectForKey:@"UIKeyboardAnimationDurationUserInfoKey"] floatValue];
        
        [UIView animateWithDuration:duration animations:^{
            bottomCons.constant = 0;
            [bottomCons.firstItem layoutIfNeeded];
        }];
        
    }];
    
    
}



-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    //  Should not recognize gesture if the clicked view is either UIControl or UINavigationBar(<Back button etc...)    (Bug ID: #145)
    return NO;

//    return YES;
}


- (void)tapRecognized:(UITapGestureRecognizer*)gesture  // (Enhancement ID: #14)
{
    if (gesture.state == UIGestureRecognizerStateEnded)
    {
        //Resigning currently responder textField.
        [self resignFirstResponder];
    }
}








@end
