//
//  UITextField+PlaceHolder.h
//  SDKTestProject
//
//  Created by 石磊 on 2023/1/6.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITextField (PlaceHolder)

@property (nonatomic,strong) IBInspectable UIColor *placeHolderColor;

@end



@interface UITextField (STKeyboardDeal)



@end

NS_ASSUME_NONNULL_END
