//
//  UIView+STBase.m
//  STTempPods
//
//  Created by 石磊 on 2022/3/21.
//

#import "UIView+STBase.h"
#import "NSObject+STBase.h"
#import <objc/runtime.h>

static NSString * const TapHandleIdentifer = @"default_ST_tapHandle";
static NSString * const LongTapHandleIdentifer = @"default_ST_longTapHandle";

@implementation UIView (STBase)

- (BOOL)choosed {
    return [objc_getAssociatedObject(self, @selector(choosed)) boolValue];
}

- (void)setChoosed:(BOOL)choosed {
    objc_setAssociatedObject(self, @selector(choosed), @(choosed), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}


- (void)tapViewHandle:(HandleBlock)tapHandle {
    if (!tapHandle) return;
    [self addHandle:tapHandle withIdentifer:TapHandleIdentifer];
    self.userInteractionEnabled = YES;
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(xxxx_tapSelf)];
    [self addGestureRecognizer:tap];
}

- (void)longTapHandle:(HandleBlock)longTapHandle {
    if (!longTapHandle) return;
    [self addHandle:longTapHandle withIdentifer:LongTapHandleIdentifer];
    UILongPressGestureRecognizer * tap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(xxxx_LongTapedSelf:)];
    
    [self addGestureRecognizer:tap];
}

- (void)xxxx_tapSelf {
    HandleBlock tapHandle = [self handleWithIdentifier:TapHandleIdentifer];
    if (tapHandle) {
        tapHandle(self, 0, nil, @"Tap self");
    }
}

- (void)xxxx_LongTapedSelf:(UILongPressGestureRecognizer *)longTap {
    
    switch (longTap.state) {
           case UIGestureRecognizerStateBegan: {
               ///识别成功是这个case.可以在这里写识别成功的代码
               HandleBlock longTapHandle = [self handleWithIdentifier:LongTapHandleIdentifer];
               if (longTapHandle) {
                   longTapHandle(self, 0, nil, @"Long Tap self");
               }
           }
               break;
           case UIGestureRecognizerStateChanged: {
               ///
           }
               break;
           case UIGestureRecognizerStateEnded: {
               ///手势结束(比如手离开了屏幕)
           }
           default:
               break;
       }
    
}





@end


@implementation UIStackView (STBase)

- (void)tapItem:(void(^)(NSInteger index))tapItem {
    
    [self.arrangedSubviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull item, NSUInteger idx, BOOL * _Nonnull stop) {
        [item tapViewHandle:^(id  _Nullable objself, NSInteger handleType, id  _Nullable obj, NSString * _Nullable des) {
            if (tapItem) {
                tapItem(idx);
            }
        }];
    }];
    
}

- (void)tapItem:(void(^)(NSInteger index,UIView * item))tapItem otherItem:(void(^)(NSInteger index,UIView * item))otherItem {
    
    __weak typeof(self) bself = self;
    [self.arrangedSubviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull item, NSUInteger idx, BOOL * _Nonnull stop) {
        
        [item tapViewHandle:^(id  _Nullable objself, NSInteger handleType, id  _Nullable obj, NSString * _Nullable des) {
            if (tapItem) {
                tapItem(idx,objself);
            }
            
            if (otherItem) {
                NSArray * arr = bself.arrangedSubviews;
                for (UIView * otherIt in arr) {
                    if (![otherIt isEqual:objself]) {
                        otherItem([arr indexOfObject:otherIt],otherIt);
                    }
                }
            }
            
        }];
    }];
    
}

@end
