//
//  UIView+STXib.m
//  STTempPods
//
//  Created by 石磊 on 2022/3/22.
//

#import "UIView+STXib.h"
#import <Masonry/Masonry.h>
#import "NSBundle+LDSDKBundle.h"
#import <objc/runtime.h>

@implementation UIResponder (STBundle)

+ (NSBundle *)selfBundle {
    NSBundle * bundle = [NSBundle bundleForClass:[self class]];
    
    BOOL aa = [self conformsToProtocol:@protocol(SDKBundleProtocal)];
    if (aa) {
        Class<SDKBundleProtocal> class = [self class];
        bundle = [NSBundle st_SDKBundle:[class bundleName]];
    } else {
        if (!bundle) {
            bundle = [NSBundle mainBundle];
        } else {}
    }
    
    return bundle;
}

- (NSBundle *)selfBundle {
    return [self.class selfBundle];
}


@end



@implementation UIView (STXib)



+ (instancetype)xibView {
    NSString * className = [self className];
    BOOL isXibDesign = [self.class isSubclassOfClass:NSClassFromString(@"STXibDesignView")];
    UIView * view;
    if (isXibDesign) {
        view = [self new];
    } else {
        @try {
            view = [[self selfBundle] loadNibNamed:className owner:nil options:nil].firstObject;
        } @catch (NSException *exception) {
            NSLog(@"%@",exception);
            view = [self new];
        } @finally {
            
        }
        
    }

    return view;
}


+ (instancetype)xibViewWithRadius:(float)radius {
    UIView * view = [self xibView];
    if (radius) {
        view.layer.cornerRadius = radius;
        view.layer.masksToBounds = YES;
    }
    return view;
}


+ (UINib *)getNib {
    UINib * nib = [UINib nibWithNibName:[self className] bundle:nil];
    return nib;
}




+ (NSString *)className {
    NSString * className = [NSStringFromClass([self class]) componentsSeparatedByString:@"."].lastObject;
    return className;
}

- (NSString *)className {
    NSString * className = [NSStringFromClass([self class]) componentsSeparatedByString:@"."].lastObject;
    return className;
}


@end



