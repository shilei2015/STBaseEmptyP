//
//  UIView+STBase.h
//  STTempPods
//
//  Created by 石磊 on 2022/3/21.
//

#import <UIKit/UIKit.h>
#import "STBaseDefine.h"

NS_ASSUME_NONNULL_BEGIN

@interface UIView (STBase)

@property (nonatomic , assign) BOOL choosed;

///Tap事件触发 handle
- (void)tapViewHandle:(nullable HandleBlock)tapHandle;

///LongTap事件
- (void)longTapHandle:(HandleBlock)longTapHandle;

///手动触发点击事件
- (void)xxxx_tapSelf;


@end


@interface UIStackView (STBase)

///点击选项
- (void)tapItem:(void(^)(NSInteger index))tapItem;

///点击选项
- (void)tapItem:(void(^)(NSInteger index,UIView * item))tapItem otherItem:(void(^)(NSInteger index,UIView * item))otherItem;

@end


NS_ASSUME_NONNULL_END
