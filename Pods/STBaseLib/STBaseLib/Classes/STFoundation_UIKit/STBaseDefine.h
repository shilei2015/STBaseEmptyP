//
//  STBaseDefine.h
//  Pods
//
//  Created by 石磊 on 2022/11/1.
//#import "STBaseDefine.h"

#ifndef STBaseDefine_h
#define STBaseDefine_h

#import <UIKit/UIKit.h>

///获取控制器
typedef UIViewController* _Nullable (^STParentVCHandle)(void);




///没有参数的回调
typedef void(^VoidHandleBlock)(void);

///
typedef void(^OutConfigHandle)(id _Nonnull obj);

///
typedef void(^OptionTypeHandle)(NSInteger type);


///(int type,id _Nullable obj)
typedef void(^OptionHandleBlock)(NSInteger type,id _Nullable obj);


///(id _Nullable objself,int handleType,id _Nullable obj,NSString * _Nullable des)
typedef void(^HandleBlock)(id _Nullable objself,NSInteger handleType,id _Nullable obj,NSString * _Nullable des);


#endif /* STBaseDefine_h */
