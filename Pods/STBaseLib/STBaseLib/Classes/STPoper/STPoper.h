

#import <Foundation/Foundation.h>
#import <LSTPopView/LSTPopView.h>

typedef NS_ENUM(NSInteger, LDPopPosition) {
    LDPopPositionTop = 0,           //贴顶
    LDPopPositionCenter = 1,        //居中
    LDPopPositionBottom = 2,        //贴底
};


NS_ASSUME_NONNULL_BEGIN

@interface STPoper : NSObject

+ (instancetype)st_popShowView:(UIView *)showView position:(LDPopPosition)position blur:(BOOL)blur bgDismiss:(BOOL)bgDismiss;

+ (instancetype)st_popShowView:(UIView *)showView parentView:(nullable UIView *)pView position:(LDPopPosition)position blur:(BOOL)blur bgDismiss:(BOOL)bgDismiss;

+ (instancetype)st_popShowView:(UIView *)showView position:(LDPopPosition)position blur:(BOOL)blur  bgDismiss:(BOOL)bgDismiss config:(nullable void(^)(LSTPopView *popView))configHandle;


+ (instancetype)st_popShowView:(UIView *)showView parentView:(nullable UIView *)pview position:(LDPopPosition)position blur:(BOOL)blur bgDismiss:(BOOL)bgDismiss config:(nullable void(^)(LSTPopView *popView))configHandle;


- (void)show;
- (void)dismiss;
+ (void)dismissView:(UIView *)view;


@end

NS_ASSUME_NONNULL_END
