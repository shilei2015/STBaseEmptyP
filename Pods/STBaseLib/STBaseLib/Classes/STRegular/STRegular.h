//
//  STRegular.h
//  STBaseLib
//
//  Created by EDY on 2023/3/22.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface STRegular : NSObject

/// 匹配字符串中相应的格式
/// - Parameters:
///   - regFormat: 需要匹配的格式如："<img.*></img>"
///   - fullString: 带匹配的完整内容字符串
+ (NSArray *)st_regular_match:(NSString *)regFormat from:(NSString *)fullString;

@end

NS_ASSUME_NONNULL_END
