//
//  STRefreshConst.h
//  Pods
//
//  Created by 石磊 on 2022/3/22.
//

#ifndef STRefreshConst_h
#define STRefreshConst_h
#import <UIKit/UIKit.h>

@class SectionModel,ST_Refresher;

/*----------------------分割线------------------------*/
typedef enum {
    RefreshNetWorkType_None,
    RefreshNetWorkType_Get,
    RefreshNetWorkType_Post,
} RefreshNetWorkType;


/*----------------------分割线------------------------*/
typedef NSString *_Nullable(^List_Api)(BOOL addMore);

typedef NSDictionary *_Nullable(^List_Parms)(NSMutableDictionary * _Nullable baseParms,BOOL addMore);

typedef NSArray <SectionModel *>*_Nullable(^List_Objs)(id _Nullable data,NSDictionary * _Nullable responseDic);

typedef void(^ComplectBlock)(BOOL addMore,BOOL success,ST_Refresher * _Nullable bRef,NSArray * _Nullable tempSourceItems);

typedef void(^RefreshLoadingHandle)(BOOL loading,BOOL complect);




/*----------------------分割线------------------------*/
//定义返回请求数据的block类型
typedef void (^RefreshNetFailureBlock) (NSError * _Nullable failure);
///请求完成的回调
typedef void (^RefreshNetComplectBlock) (id _Nullable data,id _Nullable originData);



/*----------------------分割线------------------------*/

typedef NS_ENUM(NSInteger, STListActionType) {
    STListActionTypeNone = 0,
    STListActionTypeItemClicked = 1,
    STListActionTypeItemCellAction = 2,
};

typedef void(^STListViewActionHandle)(id _Nullable bview, NSIndexPath * _Nonnull  indexPath,id _Nullable obj,NSInteger type,STListActionType actionType,NSString * _Nullable des);


typedef Class _Nullable (^ItemMsgHandle)(id _Nullable itemModel);


typedef void(^STListReloadDataHandle)(UIScrollView * _Nonnull listView);


#endif /* STRefreshConst_h */
