//
//  ST_Refresh.h
//  STBasic_Objc
//
//  Created by 244235126@qq.com on 2021/8/2.
//

#import <Foundation/Foundation.h>
#import "STRefreshConst.h"
#import "SectionModel.h"

///获取假数据--主要通能【显示列表多种状态：有数据、无数据、数据加载完 的状态 和 列表cell样式】
static NSString * _Nonnull const ListDebugApi = @"kAppListDebugUITestApiName";

/*================================================[分割线]================================================*/
NS_ASSUME_NONNULL_BEGIN
@interface ST_Refresher : NSObject

@property(nonatomic, assign, readonly) NSInteger currentPage;
@property(nonatomic, assign) NSInteger page_size;

///无限加载
@property(nonatomic, assign) BOOL infiniteLoading;

///是否下拉加载更多
@property(nonatomic, assign) BOOL reverse;

@property (nonatomic, copy) RefreshLoadingHandle loadingHandle;




+ (void)configAppNetworkClass:(Class)mClass;


/// 配置后端处理分页的字段
/// - Parameters:
///   - pageKey: 获取数据的页数key   default is "page"
///   - pageSizeKey: 每页的数量key   default is "page_size"
+ (void)configPageKeyForPage:(nullable NSString *)pageKey pagesize:(nullable NSString *)pageSizeKey;


+ (instancetype)refresherWithApi:(List_Api)api params:(List_Parms)params objs:(List_Objs)objs requestType:(RefreshNetWorkType)requestType complect:(nullable ComplectBlock)complectBlock pageable:(BOOL)pageable;

- (void)configLoadingHandle:(RefreshLoadingHandle)handle;

- (void)listRefreshData;
- (void)listAddMoreData;

@end

NS_ASSUME_NONNULL_END


