//
//  SectionModel.h
//  STTempPods
//
//  Created by 石磊 on 2022/3/22.
//

#import <Foundation/Foundation.h>
#import "STRefreshProtocal.h"

NS_ASSUME_NONNULL_BEGIN


@interface SectionModel : NSObject
@property (nonatomic, strong, nullable) id <STItem> headModel;
@property (nonatomic, strong, nullable) id <STItem> footModel;
@property (nonatomic, strong, nullable) NSMutableArray <id<STItem>>* items;
@property(nonatomic, assign) BOOL isSingleSection;

+ (instancetype)modelWithItems:(NSArray <id<STItem>>*)items;
+ (instancetype)singleSectionWithItems:(nullable NSArray *)items;
+ (NSArray <SectionModel *>*)singleSectionArrWithItems:(nullable NSArray *)items;

@end

NS_ASSUME_NONNULL_END
