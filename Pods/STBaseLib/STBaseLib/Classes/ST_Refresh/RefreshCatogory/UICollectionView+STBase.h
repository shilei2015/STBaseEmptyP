//
//  UICollectionView+STBase.h
//  STTempPods
//
//  Created by 石磊 on 2022/3/23.
//

#import <UIKit/UIKit.h>

typedef void(^STFrameChangeHandle)(BOOL originChange,BOOL sizeChange,UICollectionView * _Nonnull bCollectionView);

typedef void(^STIndexChangeHandle)(NSInteger currentIndex, UIScrollView * _Nonnull bCollectionView);

NS_ASSUME_NONNULL_BEGIN

@interface UICollectionView (STBase)<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate>

///CollectionView 尺寸发生变化时触发
@property(nonatomic, copy) STFrameChangeHandle frameHandle;

///collectionView设置了pageable时 滑动切换 会 触发
@property(nonatomic, copy) STIndexChangeHandle currentIndexHandle;

///每列的个数 可以为小数 如水平布局的情况下 显示2.5个Cell
@property(nonatomic, assign) IBInspectable float lineCount;

///高宽比 优先级 > itemSize
@property(nonatomic, assign) IBInspectable CGFloat hPerW;

///等比例额外的宽高
@property(nonatomic, assign) IBInspectable CGSize additionalSize;

///统一类型cell  item 的 size
@property(nonatomic, assign) IBInspectable CGSize itemSize;

///点击的item跟随滚动到屏幕中间
@property(nonatomic, assign) BOOL followScroll;


///手动调节CollectionView当前滚动到IndexPath
- (void)scrollAtIndexPath:(NSIndexPath *)indexPath scrollTo:(BOOL)scroll;


- (void)ST_ListDefaultSetting;

@end

NS_ASSUME_NONNULL_END
