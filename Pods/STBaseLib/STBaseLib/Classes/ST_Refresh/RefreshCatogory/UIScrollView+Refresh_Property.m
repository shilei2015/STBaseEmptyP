//
//  UIScrollView+Refresh_Property.m
//  STBasic_Objc
//
//  Created by 244235126@qq.com on 2021/8/5.
//

#import "UIScrollView+Refresh_Property.h"

#import "UIScrollView+SLRefresh.h"

#import <objc/runtime.h>
#import "TMRefresh.h"

//新建一个类
@interface FeedBackModel : NSObject
//在这个新建类里声明 weak delegate
@property (nonatomic, weak) id<STScrollViewDelegate> feedBackDelegate;

@end

@implementation FeedBackModel

@end


@interface UIScrollView (Refresh_Property)

@property (nonatomic,strong) FeedBackModel *stDelegateFeedBackModel;

@end



@implementation UIScrollView (Refresh_Property)


#pragma mark ScrollViewDelegate

- (void)setOutDelegate:(id<STScrollViewDelegate>)outDelegate {
    self.stDelegateFeedBackModel.feedBackDelegate = outDelegate;
}



- (void)setStDelegateFeedBackModel:(FeedBackModel *)stDelegateFeedBackModel {
    objc_setAssociatedObject(self, _cmd, stDelegateFeedBackModel, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}


- (FeedBackModel *)stDelegateFeedBackModel {
    FeedBackModel * fm = objc_getAssociatedObject(self, @selector(stDelegateFeedBackModel));
    if (fm) {
        return fm;
    }

    fm = [FeedBackModel new];
    objc_setAssociatedObject(self, _cmd, fm, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    return fm;
}


//MARK: - - - - - - - - - - - UIScrollViewDelegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if ([self.stDelegateFeedBackModel.feedBackDelegate respondsToSelector:@selector(scrollViewWillBeginDragging:)]) {
        [self.stDelegateFeedBackModel.feedBackDelegate scrollViewWillBeginDragging:scrollView];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if ([self.stDelegateFeedBackModel.feedBackDelegate respondsToSelector:@selector(scrollViewDidScroll:)]) {
        [self.stDelegateFeedBackModel.feedBackDelegate scrollViewDidScroll:scrollView];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if ([self.stDelegateFeedBackModel.feedBackDelegate respondsToSelector:@selector(scrollViewDidEndDragging:willDecelerate:)]) {
        [self.stDelegateFeedBackModel.feedBackDelegate scrollViewDidEndDragging:scrollView willDecelerate:decelerate];
    }
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView {
    if ([self.stDelegateFeedBackModel.feedBackDelegate respondsToSelector:@selector(scrollViewWillBeginDecelerating:)]) {
        [self.stDelegateFeedBackModel.feedBackDelegate scrollViewWillBeginDecelerating:scrollView];
    }
}

#pragma mark ScrollViewDelegate


//MARK: - 🚪🚪🚪🚪 分类属性
//MARK: -------------------- 🧵 enableHead
/*================================================[enableheader]================================================*/
- (BOOL)enableHead {
    NSNumber * headerNumb = objc_getAssociatedObject(self, @selector(enableHead));
    return [headerNumb boolValue];
}

- (void)setEnableHead:(BOOL)enableHead {
    objc_setAssociatedObject(self, @selector(enableHead), @(enableHead), OBJC_ASSOCIATION_ASSIGN);
}

- (BOOL)infiniteMore {
    NSNumber * infiniteMore = objc_getAssociatedObject(self, @selector(infiniteMore));
    return [infiniteMore boolValue];
}

- (void)setInfiniteMore:(BOOL)infiniteMore {
    objc_setAssociatedObject(self, @selector(infiniteMore), @(infiniteMore), OBJC_ASSOCIATION_ASSIGN);
}


//MARK: - ------------------- 🧵 enableFoot
static char kAssociatedObjectKey_enableFooter;
- (BOOL)enableFoot {
    NSNumber * footerNumb = objc_getAssociatedObject(self, &kAssociatedObjectKey_enableFooter);
    return [footerNumb boolValue];
}

- (void)setEnableFoot:(BOOL)enableFoot {
    if (enableFoot != self.enableFoot) {
        objc_setAssociatedObject(self, &kAssociatedObjectKey_enableFooter, @(enableFoot), OBJC_ASSOCIATION_ASSIGN);
    }
}



static char kAssociatedObjectKey_enableEmpty;
- (BOOL)enableEmpty {
    NSNumber * footerNumb = objc_getAssociatedObject(self, &kAssociatedObjectKey_enableEmpty);
    return [footerNumb boolValue];
}

- (void)setEnableEmpty:(BOOL)enableEmpty {
    if (enableEmpty != self.enableEmpty) {
        objc_setAssociatedObject(self, &kAssociatedObjectKey_enableEmpty, @(enableEmpty), OBJC_ASSOCIATION_ASSIGN);
    }
}


- (void)setAutoManage:(BOOL)autoManage {
    objc_setAssociatedObject(self, @selector(autoManage), @(autoManage), OBJC_ASSOCIATION_ASSIGN);
}

- (BOOL)autoManage {
    id autoM = objc_getAssociatedObject(self, @selector(autoManage));
    return autoM ? [autoM boolValue] : YES;
}



//MARK: - ------------------- 🧵 st_refresh
- (ST_Refresher *)st_refresh {
    return objc_getAssociatedObject(self, @selector(st_refresh));
}

- (void)setSt_refresh:(ST_Refresher *)st_refresh {
    if (st_refresh != self.st_refresh) {
        objc_setAssociatedObject(self, @selector(st_refresh), st_refresh, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
}

//MARK: - ------------------- 🧵 itemList
static char kAssociatedObjectKey_itemList;
- (NSMutableArray<SectionModel *> *)itemList {
    NSMutableArray *_handlers = objc_getAssociatedObject(self, &kAssociatedObjectKey_itemList);
    if (!_handlers) {
        _handlers = [NSMutableArray array];
        objc_setAssociatedObject(self, &kAssociatedObjectKey_itemList, _handlers, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    return _handlers;
}

- (void)setItemList:(NSMutableArray<SectionModel *> *)itemList {
    if (itemList != self.itemList) {
        objc_setAssociatedObject(self, &kAssociatedObjectKey_itemList, itemList, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
}


- (void)setSingleListItems:(NSMutableArray<id<STItem>> *)singleListItems {
    if (self.itemList.firstObject.isSingleSection) {
        self.itemList.firstObject.items = singleListItems;
    }
}


- (nullable NSMutableArray<id> *)singleListItems {
    if (!self.itemList.firstObject.items) {
        NSArray * sections = [SectionModel singleSectionArrWithItems:@[]];
        self.itemList = [NSMutableArray arrayWithArray:sections];
    }
    return self.itemList.firstObject.items;
}



static char kAssociatedObjectKey_listViewType;
- (void)setListViewType:(NSString *)listViewType {
    objc_setAssociatedObject(self, &kAssociatedObjectKey_listViewType, listViewType, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (NSString *)listViewType {
    return objc_getAssociatedObject(self, &kAssociatedObjectKey_listViewType);
}


- (void)setIdentifer:(NSString *)identifer {
    objc_setAssociatedObject(self, @selector(identifer), identifer, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (NSString *)identifer {
    return objc_getAssociatedObject(self, @selector(identifer));
}




static char kAssociatedObjectKey_ListHandle;
- (void)setListActionHandle:(STListViewActionHandle)listActionHandle {
    objc_setAssociatedObject(self, &kAssociatedObjectKey_ListHandle, listActionHandle, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (STListViewActionHandle)listActionHandle {
    return objc_getAssociatedObject(self, &kAssociatedObjectKey_ListHandle);
}


static char kAssociatedObjectKey_itemClassHandle;
- (void)setItemClassHandle:(ItemMsgHandle)itemClassHandle {
    objc_setAssociatedObject(self, &kAssociatedObjectKey_itemClassHandle, itemClassHandle, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (ItemMsgHandle)itemClassHandle {
    return objc_getAssociatedObject(self, &kAssociatedObjectKey_itemClassHandle);
}


static char kAssociatedObjectKey_ItemClass;
- (void)setItemCellClass:(Class)itemClass {
    objc_setAssociatedObject(self, &kAssociatedObjectKey_ItemClass, itemClass, OBJC_ASSOCIATION_ASSIGN);
}

- (Class)itemCellClass {
    return objc_getAssociatedObject(self, &kAssociatedObjectKey_ItemClass);
}


- (void)setItemModelClass:(Class)itemModelClass {
    objc_setAssociatedObject(self, @selector(itemModelClass), itemModelClass, OBJC_ASSOCIATION_ASSIGN);
}

- (Class)itemModelClass {
    return objc_getAssociatedObject(self, @selector(itemModelClass));
}



- (void)setPreReloadHandle:(STListReloadDataHandle)preReloadHandle {
    objc_setAssociatedObject(self, @selector(preReloadHandle), preReloadHandle, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (STListReloadDataHandle)preReloadHandle {
    return objc_getAssociatedObject(self, @selector(preReloadHandle));
}


- (void)setReloadHandle:(STListReloadDataHandle)reloadHandle {
    objc_setAssociatedObject(self, @selector(reloadHandle), reloadHandle, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (STListReloadDataHandle)reloadHandle {
    return objc_getAssociatedObject(self, @selector(reloadHandle));
}

- (void)setCustomEmptyView:(UIView *)customEmptyView {
    objc_setAssociatedObject(self, @selector(customEmptyView), customEmptyView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIView *)customEmptyView {
    return objc_getAssociatedObject(self, @selector(customEmptyView));
}


- (void)setEmptyTitle:(NSString *)emptyTitle {
    objc_setAssociatedObject(self, @selector(emptyTitle), emptyTitle, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSString *)emptyTitle {
    return objc_getAssociatedObject(self, @selector(emptyTitle));
}

- (void)setEmptyDetail:(NSString *)emptyDetail {
    objc_setAssociatedObject(self, @selector(emptyDetail), emptyDetail, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSString *)emptyDetail {
    return objc_getAssociatedObject(self, @selector(emptyDetail));
}






@end

















@implementation UICollectionViewCell (List)

- (void)setListViewType:(NSString *)listViewType {
    objc_setAssociatedObject(self, @selector(listViewType), listViewType, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSString *)listViewType {
    return objc_getAssociatedObject(self, @selector(listViewType));
}

@end

@implementation UITableViewCell (List)

- (void)setListViewType:(NSString *)listViewType {
    objc_setAssociatedObject(self, @selector(listViewType), listViewType, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSString *)listViewType {
    return objc_getAssociatedObject(self, @selector(listViewType));
}

@end
