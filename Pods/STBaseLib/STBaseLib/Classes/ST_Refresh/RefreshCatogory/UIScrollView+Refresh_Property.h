//
//  UIScrollView+Refresh_Property.h
//  STBasic_Objc
//
//  Created by 244235126@qq.com on 2021/8/5.
//

#import <UIKit/UIKit.h>
#import "ST_Refresher.h"

NS_ASSUME_NONNULL_BEGIN


@protocol STScrollViewDelegate <NSObject>

@optional
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView;
- (void)scrollViewDidScroll:(UIScrollView *)scrollView;
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;
- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView;

@end





@interface UIScrollView (Refresh_Property)

/// 列表中的   数据源  [SectionModel]
@property(nonatomic, strong) NSMutableArray <SectionModel *>*itemList;

@property (nonatomic, strong, nullable) NSMutableArray <id>* singleListItems;

///列表显示的Item cell class （优先级最高）高于<STItem>
@property (assign, nonatomic) Class itemCellClass;

@property (assign, nonatomic) Class itemModelClass;

/// 是否可以有头部 - 下拉刷新控件  default is NO
@property(nonatomic, assign) BOOL enableHead;

/// 是否可以有尾部 - 加载更多 控件 default is NO
@property(nonatomic, assign) BOOL enableFoot;

/// 是否可以有空数据页面 default is NO
@property(nonatomic, assign) BOOL enableEmpty;

/// 是否自动管理delegate和datasourse     default is YES
@property(nonatomic, assign) BOOL autoManage;

@property(nonatomic, assign) BOOL infiniteMore;

/// 列表 数据加载的配置
@property(nonatomic, strong) ST_Refresher *st_refresh;


///空数据页面title
@property(nonatomic, copy)  NSString * _Nullable  emptyTitle;

///空数据页面detail
@property(nonatomic, copy)  NSString * _Nullable  emptyDetail;


///listview 内部事件回调
@property(nonatomic, copy) STListViewActionHandle listActionHandle;


///列表刷新回调
@property(nonatomic, copy) STListReloadDataHandle preReloadHandle;

///列表刷新回调
@property(nonatomic, copy) STListReloadDataHandle reloadHandle;


@property (nonatomic , strong) UIView * customEmptyView;



///用于 区分同类型数据 不同显示方式
@property(nonatomic, copy)  NSString * _Nullable  listViewType;

@property(nonatomic, copy)  NSString * _Nullable  identifer;

///listview 内部事件回调 获取itemcell class 单独配置item时使用
///优先级  itemCellClass > protocol(STItem)  >  itemClassHandle
@property(nonatomic, copy) ItemMsgHandle itemClassHandle;




- (void)setOutDelegate:(id<STScrollViewDelegate>)outDelegate;


@end














@interface UICollectionViewCell (List)
@property(nonatomic, copy)  NSString * _Nullable  listViewType;

@end

@interface UITableViewCell (List)
@property(nonatomic, copy)  NSString * _Nullable  listViewType;


@end


NS_ASSUME_NONNULL_END
