//
//  UICollectionView+STBase.m
//  STTempPods
//
//  Created by 石磊 on 2022/3/23.
//

#import "UICollectionView+STBase.h"

#import "UIScrollView+SLRefresh.h"
#import "UIScrollView+Refresh_Property.h"
#import "UIView+STXib.h"
#import "NSObject+STBase.h"

#import <LYEmptyView/LYEmptyViewHeader.h>
#import <objc/runtime.h>

#import "STDebug.h"
#import "STDebug.h"

@interface UICollectionView (STBase)

@property(nonatomic, assign) CGPoint temp_origin;
@property(nonatomic, assign) CGSize temp_size;

@end


@implementation UICollectionView (STBase)

+ (void)load {
    Method systemMethod = class_getInstanceMethod([self class], @selector(layoutSubviews));
    Method customMethod = class_getInstanceMethod([self class], @selector(st_layoutSubviews));
    method_exchangeImplementations(systemMethod, customMethod);
}

- (void)st_layoutSubviews {

    BOOL isOriginChange = !CGPointEqualToPoint(self.frame.origin, self.temp_origin);
    BOOL isSizeChange = !CGSizeEqualToSize(self.frame.size, self.temp_size);

    if (isSizeChange) {
        [self customLayoutSetting];
    }

    if (self.frameHandle) {
        self.frameHandle(isOriginChange, isSizeChange,self);
    }
    self.temp_origin = self.frame.origin;
    self.temp_size = self.frame.size;

    [self st_layoutSubviews];
}

- (void)ST_ListDefaultSetting {

    self.delegate = self;
    self.dataSource = self;

    [self registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:NSStringFromClass([UICollectionViewCell class])];
    [self registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass([UICollectionReusableView class])];
    [self registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:NSStringFromClass([UICollectionReusableView class])];

    [self emptySingle];
    
    LDLog(@"%@",NSStringFromSelector(_cmd));
    LDLogFunction
}


//
//MARK: - - - - - - - - - - - 可以重写的
- (void)customLayoutSetting {
    
    //self.lineCount
    //self.hPerW
    if (!self.lineCount) {
        LDLog(@"不计算CollectionViewCell size");
        return;
    }
    
    UICollectionViewFlowLayout * layout = (UICollectionViewFlowLayout *)self.collectionViewLayout;
    
    float itemW = 100;
    float itemH = 100;
    
    if (self.itemSize.width) {
        itemW = self.itemSize.width;
        itemH = self.itemSize.height;
    } else {
        float space_h = layout.minimumInteritemSpacing;
        UIEdgeInsets inset = layout.sectionInset;
        
        float count = self.lineCount;
        itemW = ((self.bounds.size.width - (inset.left + inset.right) - (space_h * ceil(count-1)) - 1) / (count * 1.0f)) ?:100;
        itemH = 100;
        
        if (self.hPerW) {
            float hPerW = self.hPerW ?: 1;
            itemH = ((itemW - self.additionalSize.width) * hPerW) + self.additionalSize.height;
        } else if (self.itemSize.height) {
            itemH = self.itemSize.height;
        }
        if (!itemH) {
            itemH = 100;
        }
    }
    
    CGSize size = CGSizeMake(itemW, itemH);
    layout.itemSize = size;
    self.itemSize = size;
    
    LDLog(@"计算 item size = CGSize(%lf,%lf)",layout.itemSize.width,layout.itemSize.height);
}


//MARK: - - - - - - - - - - - <UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    NSInteger sectionItemCount = self.itemList.count;
    return sectionItemCount;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    SectionModel * sectionM = self.itemList[section];
    NSInteger sectionItemCount = sectionM.items.count;
    return sectionItemCount;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    id <STItem> itemModel = [self itemModelWithIndexPath:indexPath];
    UICollectionViewCell * cell = [self getItemCellWithCellModel:itemModel indexPath:indexPath];
    {
        __weak typeof(self) bself = self;
        HandleBlock handleBlock = ^(id  _Nullable bview, NSInteger handleType, id  _Nullable obj, NSString * _Nullable des) {
            if (bself.listActionHandle) {
                bself.listActionHandle(bview, indexPath, obj, handleType, STListActionTypeItemCellAction,des);
            }
        };
        [cell configHandleBlock:handleBlock];
        cell.parentController = self.parentController;
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];

    UICollectionViewFlowLayout * layout = (UICollectionViewFlowLayout *)self.collectionViewLayout;
    
    LDLog(@"点击 item %@",layout.debugDescription);
    
    id <STItem> itemModel = [self itemModelWithIndexPath:indexPath];
    if (self.listActionHandle) {
        self.listActionHandle(collectionView, indexPath, itemModel, 0, STListActionTypeItemClicked,@"clicked item cell");
    }

    ///跟随滚动  当前点击的item滚动到屏幕居中位置（除无法居中）
    if (self.followScroll) {
        [collectionView scrollAtIndexPath:indexPath scrollTo:YES];
    }
}

- (void)scrollAtIndexPath:(NSIndexPath *)indexPath scrollTo:(BOOL)scroll {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self scrollToItemAtIndexPath:indexPath atScrollPosition:(UICollectionViewScrollPositionCenteredHorizontally) animated:scroll];
    });
}


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    SectionModel * sectionModel = [self sectionModelWithSection:indexPath.section];
    return [self getReusableViewWithSectionModel:sectionModel indexPath:indexPath forKind:kind];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return self.itemSize;
}

//MARK: - - - - - - - - - - - 内部处理方法
- (Class)itemCellClassWithItemModel:(id<STItem>)itemModel {
    Class itemClass;
    if (self.itemCellClass) {
        itemClass = self.itemCellClass;
    } else if ([itemModel conformsToProtocol:@protocol(STItem)]) {
        itemClass = [itemModel itemCellClassWithItemType:STItemType_CollectionView_Cell listViewType:self.listViewType];
    } else if (self.itemClassHandle) {
        itemClass = self.itemClassHandle(itemModel);
    }
    return itemClass;
}

- (UICollectionViewCell *)getItemCellWithCellModel:(id<STItem>)itemModel indexPath:(NSIndexPath *)indexPath {

    Class itemClass = [self itemCellClassWithItemModel:itemModel];
    NSString * itemClassName = [NSStringFromClass(itemClass) componentsSeparatedByString:@"."].lastObject;
    NSString * identifer = [[itemClassName stringByAppendingString:@"_collectionViewCell"] stringByAppendingFormat:@"%p",self];
    UICollectionViewCell * cell;
    
    if (itemModel && !cell && itemClassName) {
        SEL selector = @selector(isRegisterNib);
        
        BOOL isRegistNib = YES;
        if ([itemClass respondsToSelector:selector]) {
            id registNib = [itemClass performSelector:selector];
            isRegistNib = [registNib boolValue];
            LDLog(@"%@ isRegistNib %@",itemClassName,[registNib boolValue] ? @"YES" : @"NO");
        }
        
        if (isRegistNib) {
            UINib * nib = [UINib nibWithNibName:itemClassName bundle:[itemClass selfBundle]];
            [self registerNib:nib forCellWithReuseIdentifier:identifer];
        } else {
            [self registerClass:NSClassFromString(itemClassName) forCellWithReuseIdentifier:identifer];
        }
        
        cell = [self dequeueReusableCellWithReuseIdentifier:identifer forIndexPath:indexPath];
    }
    
    
    if (!cell) {
        cell = [self dequeueReusableCellWithReuseIdentifier:NSStringFromClass([UICollectionViewCell class]) forIndexPath:indexPath];
        cell.backgroundColor = [UIColor lightGrayColor];
    }
    
    if (cell && [cell conformsToProtocol:@protocol(STCell)]) {
        cell.listViewType = self.listViewType;
        ((id <STCell>)cell).itemCell_TempModel = itemModel;
    }

    return cell;
}

- (UICollectionReusableView *)getReusableViewWithSectionModel:(SectionModel *)sectionModel indexPath:(NSIndexPath *)indexPath forKind:(NSString *)kind {

    UICollectionReusableView *reusableView;
    BOOL isHeader = [kind isEqualToString:UICollectionElementKindSectionHeader];
    id <STItem> reusableViewModel = isHeader ? sectionModel.headModel : sectionModel.footModel;

    Class headViewClass = [reusableViewModel itemCellClassWithItemType:(isHeader ? STItemType_CollectionView_Header : STItemType_CollectionView_Footer) listViewType:self.listViewType];
    
    if (!headViewClass || ![headViewClass isKindOfClass:[UICollectionReusableView class]]) {
        reusableView = [self dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass([UICollectionReusableView class]) forIndexPath:indexPath];
    } else {
        NSString * itemClassName = [NSStringFromClass(headViewClass) componentsSeparatedByString:@"."].lastObject;
        
        UINib * nib = [UINib nibWithNibName:NSStringFromClass(headViewClass) bundle:nil];

        if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {

            reusableView = [self dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:itemClassName forIndexPath:indexPath];
            if (reusableViewModel && !reusableView) {
                [self registerNib:nib forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:itemClassName];
                reusableView = [self dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:itemClassName forIndexPath:indexPath];
            }

        } else if ([kind isEqualToString:UICollectionElementKindSectionFooter]) {

            reusableView = [self dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:itemClassName forIndexPath:indexPath];
            if (reusableViewModel && !reusableView) {
                [self registerNib:nib forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:itemClassName];
                reusableView = [self dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:itemClassName forIndexPath:indexPath];
            }

        }
    }

    if (reusableView && [reusableView conformsToProtocol:@protocol(STCell)]) {
        ((id <STCell>)reusableView).itemCell_TempModel = reusableViewModel;
    }
    return reusableView;
}


- (SectionModel *)sectionModelWithSection:(NSInteger)section {
    SectionModel * sectionModel = self.itemList[section];
    return sectionModel;
}

- (id<STItem>)itemModelWithIndexPath:(NSIndexPath *)indexPath {
    SectionModel * sectionModel = self.itemList[indexPath.section];
    id itemModel = sectionModel.items[indexPath.row];
    return itemModel;
}

//MARK: ⬆️⬆️



//MARK: - - - - - - - - - - - 自定义属性
- (void)setFrameHandle:(STFrameChangeHandle)frameHandle {
    objc_setAssociatedObject(self, @selector(frameHandle), frameHandle, OBJC_ASSOCIATION_RETAIN);
}

- (STFrameChangeHandle)frameHandle {
    return objc_getAssociatedObject(self, @selector(frameHandle));
}


- (void)setTemp_size:(CGSize)temp_size {
    objc_setAssociatedObject(self, @selector(temp_size), [NSValue valueWithCGSize:temp_size], OBJC_ASSOCIATION_RETAIN);
}

- (CGSize)temp_size {
    return [objc_getAssociatedObject(self, @selector(temp_size)) CGSizeValue];
}


- (void)setTemp_origin:(CGPoint)temp_origin {
    objc_setAssociatedObject(self, @selector(temp_origin), [NSValue valueWithCGPoint:temp_origin], OBJC_ASSOCIATION_RETAIN);
}

- (CGPoint)temp_origin {
    return [objc_getAssociatedObject(self, @selector(temp_origin)) CGPointValue];
}


- (void)setLineCount:(float)lineCount {
    objc_setAssociatedObject(self, @selector(lineCount), @(lineCount), OBJC_ASSOCIATION_RETAIN);
}

- (float)lineCount {
    return [objc_getAssociatedObject(self, @selector(lineCount)) floatValue];
}


- (void)setHPerW:(CGFloat)hPerW {
    objc_setAssociatedObject(self, @selector(hPerW), @(hPerW), OBJC_ASSOCIATION_RETAIN);
}

- (CGFloat)hPerW {
    return [objc_getAssociatedObject(self, @selector(hPerW)) floatValue];
}


- (void)setAdditionalSize:(CGSize)additionalSize {
    objc_setAssociatedObject(self, @selector(additionalSize), [NSValue valueWithCGSize:additionalSize], OBJC_ASSOCIATION_RETAIN);
}

- (CGSize)additionalSize {
    return [objc_getAssociatedObject(self, @selector(additionalSize)) CGSizeValue];
}


- (void)setItemSize:(CGSize)itemSize {
    objc_setAssociatedObject(self, @selector(itemSize), [NSValue valueWithCGSize:itemSize], OBJC_ASSOCIATION_RETAIN);
}

- (CGSize)itemSize {
    return [objc_getAssociatedObject(self, @selector(itemSize)) CGSizeValue];
}

- (void)setFollowScroll:(BOOL)followScroll {
    objc_setAssociatedObject(self, @selector(followScroll), @(followScroll), OBJC_ASSOCIATION_RETAIN);
}

- (BOOL)followScroll {
    return [objc_getAssociatedObject(self, @selector(followScroll)) boolValue];
}


- (void)setCurrentIndexHandle:(STIndexChangeHandle)currentIndexHandle {
    objc_setAssociatedObject(self, @selector(currentIndexHandle), currentIndexHandle, OBJC_ASSOCIATION_RETAIN);
}

- (STIndexChangeHandle)currentIndexHandle {
    return objc_getAssociatedObject(self, @selector(currentIndexHandle));
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if (self.currentIndexHandle) {
        NSInteger index = round(scrollView.contentOffset.x/CGRectGetWidth(scrollView.frame));
        LDLog(@"index = %zd",index);
        self.currentIndexHandle(index, scrollView);
    }
}


@end
