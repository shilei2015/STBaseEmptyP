//
//  UITableView+STBase.m
//  STTempPods
//
//  Created by 石磊 on 2022/3/22.
//

#import "UITableView+STBase.h"
#import "STRefreshProtocal.h"
#import "UIScrollView+SLRefresh.h"
#import "UIScrollView+Refresh_Property.h"
#import "UIView+STBase.h"
#import <objc/runtime.h>
#import "STCatoHeader.h"
#import "STDebug.h"
#import "STDebug.h"




@interface UITableView (STBase)<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate>

///中转的协议对象  - 中转tableview的协议
//@property (nonatomic,strong) FeedBackModel *stDelegateFeedBackModel;

@end



@implementation UITableView (STBase)



- (void)ST_ListDefaultSetting {
    self.estimatedSectionHeaderHeight = 0;
    self.estimatedSectionFooterHeight = 0;
    self.estimatedRowHeight = UITableViewAutomaticDimension;// 此值为一个大概值，类似于占位值
    self.rowHeight = UITableViewAutomaticDimension;

    self.delegate = self;
    self.dataSource = self;
}

//MARK: - 🚪🚪🚪🚪 UITableViewDataSource

//MARK: Header-FooterView
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    SectionModel * sectionModel = [self sectionModelWithSection:section];
    id <STItem> headerModel = sectionModel.headModel;
    UIView * headView = [self getHeadViewWithHeadModel:headerModel];
    headView.frame = CGRectMake(0, 0, self.bounds.size.width, 50);
    headView.backgroundColor = [UIColor randomColor];
    return headView;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    SectionModel * sectionModel = [self sectionModelWithSection:section];
    id <STItem> footerModel = sectionModel.footModel;
    UIView * footView = [self getFootViewWithFootModel:footerModel];
    return footView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    SectionModel * sectionModel = [self sectionModelWithSection:section];
    if ([sectionModel.headModel conformsToProtocol:@protocol(STItem)]) {
        return [sectionModel.headModel itemHeightWithItemType:STItemType_TableView_Header listViewType:self.listViewType];
    }
    return self.sectionHeaderHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    SectionModel * sectionModel = [self sectionModelWithSection:section];
    if ([sectionModel.footModel conformsToProtocol:@protocol(STItem)]) {
        return [sectionModel.footModel itemHeightWithItemType:STItemType_TableView_Footer listViewType:self.listViewType];
    }
    return self.sectionFooterHeight;
}


//MARK: - - - - - - - - - - - TableView datasourse
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.itemList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (self.itemList.count) {
        SectionModel * sModel = self.itemList[section];
        if (sModel) {
            return sModel.items.count;
        }
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

    CGFloat itemH = UITableViewAutomaticDimension;
    id <STItem> itemModel = [self itemModelWithIndexPath:indexPath];
    if ([itemModel conformsToProtocol:@protocol(STItem)]) {
        if ([itemModel respondsToSelector:@selector(itemHeightWithItemType:listViewType:)]) {
            CGFloat h = [itemModel itemHeightWithItemType:STItemType_TableView_Cell listViewType:self.listViewType];
            if (h > 0) {
                itemH = h;
            }
        }
    } else if (self.rowHeight > 0) {
        return self.rowHeight;
    }
    
    
    return itemH;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    id <STItem> itemModel = [self itemModelWithIndexPath:indexPath];
    UITableViewCell * cell = [self getItemCellWithCellModel:itemModel];
    {
        __weak typeof(self) bself = self;
        HandleBlock handle = ^(id  _Nullable bview, NSInteger handleType, id  _Nullable obj, NSString * _Nullable des) {
            if (bself.listActionHandle) {
                bself.listActionHandle(bview, indexPath, obj, handleType, STListActionTypeItemCellAction,des);
            }
        };
        [cell configHandleBlock:handle];
        cell.parentController = self.parentController;
    }
    
    return cell;
}

//MARK: - 🚪🚪🚪🚪 UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    id <STItem> itemModel = [self itemModelWithIndexPath:indexPath];
    
    if (self.listActionHandle) {
        self.listActionHandle(tableView, indexPath, itemModel, 0, STListActionTypeItemClicked,@"tableview clicked item cell");
    }
}


- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    id <STCell>tempCell = cell;
    if (cell && [cell conformsToProtocol:@protocol(STCell)]) {
        
//        instancesRespondToSelector
        if ([tempCell respondsToSelector:@selector(didEndDisplayForRowAtIndexPath:)]) {
            [tempCell didEndDisplayForRowAtIndexPath:indexPath];
        }
    }
}

//- (UISwipeActionsConfiguration *)tableView:(UITableView *)tableView trailingSwipeActionsConfigurationForRowAtIndexPath:(NSIndexPath *)indexPath {
//    if ([self.stDelegateFeedBackModel.feedBackDelegate respondsToSelector:@selector(tableView:trailingSwipeActionsConfigurationForRowAtIndexPath:)]) {
//        return [self.stDelegateFeedBackModel.feedBackDelegate tableView:tableView trailingSwipeActionsConfigurationForRowAtIndexPath:indexPath];
//    }
//    return nil;
//}


//MARK: - - - - - - - - - - - UIScrollViewDelegate
//- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
//    if ([self.stDelegateFeedBackModel.feedBackDelegate respondsToSelector:@selector(scrollViewWillBeginDragging:)]) {
//        [self.stDelegateFeedBackModel.feedBackDelegate scrollViewWillBeginDragging:scrollView];
//    }
//}
//
//- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//    if ([self.stDelegateFeedBackModel.feedBackDelegate respondsToSelector:@selector(scrollViewDidScroll:)]) {
//        [self.stDelegateFeedBackModel.feedBackDelegate scrollViewDidScroll:scrollView];
//    }
//}
//
//- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
//    [self.stDelegateFeedBackModel.feedBackDelegate scrollViewDidEndDragging:scrollView willDecelerate:decelerate];
//}
//- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView {
//    [self.stDelegateFeedBackModel.feedBackDelegate scrollViewWillBeginDecelerating:scrollView];
//}



//MARK: - - - - - - - - - - - 内部处理方法
- (Class)itemCellClassWithItemModel:(id<STItem>)itemModel {
    Class itemClass;
    if (self.itemCellClass) {
        itemClass = self.itemCellClass;
    } else if ([itemModel conformsToProtocol:@protocol(STItem)]) {
        itemClass = [itemModel itemCellClassWithItemType:STItemType_TableView_Cell listViewType:self.listViewType];
    } else if (self.itemClassHandle) {
        itemClass = self.itemClassHandle(itemModel);
    }
    return itemClass;
}

- (UITableViewCell *)getItemCellWithCellModel:(id<STItem>)itemModel {

    Class itemClass = [self itemCellClassWithItemModel:itemModel];
    
    NSString * itemClassName = [NSStringFromClass(itemClass) componentsSeparatedByString:@"."].lastObject;
    NSString * identifer = [[itemClassName stringByAppendingString:@"_tableViewCell"] stringByAppendingFormat:@"%p",self];
    UITableViewCell * cell = [self dequeueReusableCellWithIdentifier:identifer];
    
    if ([itemClassName isEqualToString:NSStringFromClass([UITableViewCell class])]) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifer];
        cell.textLabel.text = [NSString stringWithFormat:@"Undefined type item"];
    } else if (itemModel && !cell && itemClassName) {
        
        SEL selector = @selector(isRegisterNib);
        
        BOOL isRegistNib = YES;
        if ([itemClass respondsToSelector:selector]) {
            id registNib = [itemClass performSelector:selector];
            isRegistNib = [registNib boolValue];
            LDLog(@"%@ isRegistNib %@",itemClassName,[registNib boolValue] ? @"YES" : @"NO");
        }
        
        if (isRegistNib) {
            UINib * nib = [UINib nibWithNibName:itemClassName bundle:[itemClass selfBundle]];
            [self registerNib:nib forCellReuseIdentifier:identifer];
        } else {
            [self registerClass:NSClassFromString(itemClassName) forCellReuseIdentifier:identifer];
        }
        cell = [self dequeueReusableCellWithIdentifier:identifer];
    }

    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifer];
        cell.textLabel.text = [NSString stringWithFormat:@"Undefined type item"];
    }
    
    if (cell && [cell conformsToProtocol:@protocol(STCell)]) {
        cell.listViewType = self.listViewType;
        ((id <STCell>)cell).itemCell_TempModel = itemModel;
    }
    return cell;
}

- (UIView *)getHeadViewWithHeadModel:(id<STItem>)headModel {
    Class headViewClass = [headModel itemCellClassWithItemType:STItemType_TableView_Header listViewType:self.listViewType];

    UIView * headView = [self dequeueReusableHeaderFooterViewWithIdentifier:@"header"];
    if (headModel && !headView) {
        NSString * headViewClassName = [NSStringFromClass(headViewClass) componentsSeparatedByString:@"."].lastObject;
        UINib * nib = [UINib nibWithNibName:headViewClassName bundle:nil];
        [self registerNib:nib forHeaderFooterViewReuseIdentifier:@"header"];
        headView = [self dequeueReusableHeaderFooterViewWithIdentifier:@"header"];
    }
    
    if ([headView conformsToProtocol:@protocol(STCell)]) {
        id <STCell> tt = (id <STCell>)headView;
        tt.itemCell_TempModel = headModel;
    }
        
    return headView;
}

- (UIView *)getFootViewWithFootModel:(id<STItem>)footModel {
    Class footViewClass = [footModel itemCellClassWithItemType:STItemType_TableView_Footer listViewType:self.listViewType];
    UIView * footView = [self dequeueReusableHeaderFooterViewWithIdentifier:@"foot"];
    if (footModel && !footView) {
        NSString * footViewClassName = [NSStringFromClass(footViewClass) componentsSeparatedByString:@"."].lastObject;
        UINib * nib = [UINib nibWithNibName:footViewClassName bundle:nil];
        [self registerNib:nib forHeaderFooterViewReuseIdentifier:@"foot"];
        footView = [self dequeueReusableHeaderFooterViewWithIdentifier:@"foot"];
    }
    if ([footView conformsToProtocol:@protocol(STCell)]) {
        id <STCell> tt = (id <STCell>)footView;
        tt.itemCell_TempModel = footModel;
    }
    return footView;
}


/*----------------------分割线------------------------*/
- (SectionModel *)sectionModelWithSection:(NSInteger)section {
    if (self.itemList && self.itemList.count) {
        SectionModel * sectionModel = self.itemList[section];
        return sectionModel;
    }
    return nil;
}

- (id<STItem>)itemModelWithIndexPath:(NSIndexPath *)indexPath {

    if (self.itemList && self.itemList.count) {
        SectionModel * sectionModel = self.itemList[indexPath.section];
        id itemModel = sectionModel.items[indexPath.row];
        return itemModel;
    }
    return nil;
}




@end





@implementation UITableViewCell (STBase)

- (UITableView *)tableView {
    id view = [self superview];
    while (view && [view isKindOfClass:[UITableView class]] == NO) {
        view = [view superview];
    }

    UITableView *tableView = (UITableView *)view;

    return tableView;
}


@end
