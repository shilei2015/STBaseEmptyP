//
//  UIScrollView+SLRefresh.m
//  STBasic_Objc
//
//  Created by 244235126@qq.com on 2021/8/4.
//

#import "UIScrollView+SLRefresh.h"

#import "UIScrollView+Refresh_Property.h"
#import "NSObject+STBase.h"
#import "TMRefresh.h"
#import <LYEmptyView/LYEmptyViewHeader.h>
#import <YYModel/YYModel.h>


static NSString * kEmptyTitle = @"";
static NSString * kEmptyDetail = @"";
static NSString * kEmptyImg = @"";
static NSString * kEmptyViewClassName = nil;

@implementation UIScrollView (SLRefresh)


+ (void)configDefaultEmptyTitle:(NSString *)title detail:(NSString *)detail imgName:(NSString *)imgName {
    kEmptyTitle = title;
    kEmptyDetail = detail;
    kEmptyImg = imgName;
}


- (void)emptySingle {
    if (!self.singleListItems) {
        self.itemList = [NSMutableArray arrayWithArray:[SectionModel singleSectionArrWithItems:@[]]];
    }
}

- (void)defaultEmptySet {
    
    {// EmptyView
        if (!self.ly_emptyView) {
            
            if (self.customEmptyView) {
                if ([self.customEmptyView isKindOfClass:[LYEmptyView class]]) {
                    self.ly_emptyView = (LYEmptyView *)self.customEmptyView;
                } else {
                    self.ly_emptyView = [LYEmptyView emptyViewWithCustomView:self.customEmptyView];
                }
                return;
            }
            
            NSString * title = self.emptyTitle ? self.emptyTitle : kEmptyTitle;
            NSString * detail = self.emptyDetail ? self.emptyDetail : kEmptyDetail;;
            self.ly_emptyView = [LYEmptyView emptyViewWithImage:[UIImage imageNamed:kEmptyImg] titleStr:title detailStr:detail];
            
        }
    }
}


//MARK: - 🚪🚪🚪🚪 方法
- (void)startHeadRefreshWithAnimation:(BOOL)animated {
    if (animated) {
        if (!self.enableHead) {
            self.enableHead = YES;
        }
        [self.mj_header beginRefreshing];
    } else {
        [self.st_refresh listRefreshData];
    }
}


- (void)startFootRefreshWithAnimation:(BOOL)animated {
    if (animated) {
        [self.mj_footer beginRefreshing];
    } else {
        [self.st_refresh listAddMoreData];
    }
}


- (void)setRefresherWithApi:(List_Api)api params:(List_Parms)params objs:(List_Objs)objs requestType:(RefreshNetWorkType)requestType pageable:(BOOL)pageable refresherConfig:(nullable void(^)(ST_Refresher *rtf))config {
    [self setRefresherWithApi:api params:params objs:objs requestType:requestType pageable:pageable];
    if (config) {
        config(self.st_refresh);
    }
}


- (void)setRefresherWithApi:(List_Api)api params:(List_Parms)params objs:(List_Objs)objs requestType:(RefreshNetWorkType)requestType pageable:(BOOL)pageable {
    __weak typeof(self) bself = self;
    self.st_refresh = [ST_Refresher refresherWithApi:api params:params objs:objs requestType:requestType complect:^(BOOL addMore,BOOL suc,ST_Refresher * bRef,NSArray *tempSourceItems) {

        
        if (suc) {
            BOOL reverse = bself.st_refresh.reverse;
            BOOL isSectionModel = [tempSourceItems.firstObject isKindOfClass:[SectionModel class]];
            
            if (!isSectionModel) {
                if (bself.itemModelClass && ![NSStringFromClass(bself.itemModelClass) isEqualToString:@"NSObject"]) {
                    //itemModelClass    已配置
                    NSArray * modelArr = [NSArray yy_modelArrayWithClass:bself.itemModelClass json:tempSourceItems];
                    SectionModel * singleSection = [SectionModel singleSectionWithItems:modelArr];
                    [bself singleAddMore:addMore sectinModel:singleSection reverse:reverse pageAble:pageable];
                } else {
                    //itemModelClass    未配置
                    SectionModel * singleSection = [SectionModel singleSectionWithItems:tempSourceItems];
                    [bself singleAddMore:addMore sectinModel:singleSection reverse:reverse pageAble:pageable];
                }
                
            } else {
                NSArray <SectionModel *>* newList = tempSourceItems;
                
                BOOL isSingle = newList.firstObject.isSingleSection;
                if (isSingle) {
                    [bself singleAddMore:addMore sectinModel:newList.firstObject reverse:reverse pageAble:pageable];
                } else {
                    [bself mutiSectionAddMore:addMore sectinModels:newList reverse:reverse pageAble:pageable];
                }
            }
            

        } else {
            if (addMore) {
                [bself.mj_footer endRefreshing];
            } else {
                [bself.mj_header endRefreshing];
            }
        }
        if (bself.enableEmpty) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [bself defaultEmptySet];
            });
        }
        
        if (bself.st_refresh.loadingHandle) {bself.st_refresh.loadingHandle(NO, YES);}

    } pageable:pageable];
    
    if (self.autoManage) {
        
        if (self.enableHead) {
            TMRefreshHeader * header = [TMRefreshHeader headerWithRefreshingTarget:self.st_refresh refreshingAction:@selector(listRefreshData)];
            self.mj_header = header;
        } else {
            self.mj_header = nil;
        }
        
        if ([self isKindOfClass:[UITableView class]] || [self isKindOfClass:[UICollectionView class]]) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wundeclared-selector"
            SEL selector = @selector(ST_ListDefaultSetting);
#pragma clang diagnostic pop
            if ([self respondsToSelector:selector]) {
                
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                [self performSelector:selector];
                NSLog(@"走了调用");
#pragma clang diagnostic pop
            }
        }
        
    }
}



- (void)singleAddMore:(BOOL)addMore sectinModel:(SectionModel *)sectionModel reverse:(BOOL)reverse pageAble:(BOOL)pageable {

    if (!addMore) {//下拉
        if (reverse) {
            NSArray * itemModels = sectionModel.items;
            NSIndexSet *indexSet = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, [itemModels count])];
            [self.singleListItems insertObjects:itemModels atIndexes:indexSet];
        } else {
            [self.singleListItems removeAllObjects];
        }
    }

    if (!self.singleListItems) {
        [self.itemList addObject:sectionModel];
    } else if (!reverse) {
        [self.singleListItems addObjectsFromArray:sectionModel.items];
    }
    

    //处理上拉加载更多
    //1.刷新的情况
    //2.加载更多的情况
    if (self.enableFoot && pageable && self.singleListItems.count > 0) {
        [self initFooter];
    }

    if (!addMore) {
        [self.mj_header endRefreshing];
    }

    [self reloadList];

    if (reverse && [self isKindOfClass:[UITableView class]] && sectionModel.items.count > 0) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:sectionModel.items.count-1 inSection:0];
        dispatch_async(dispatch_get_main_queue(), ^{
            [(UITableView *)self scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:NO];
        });
    }
    
    NSLog(@"刷新列表");

}


- (void)mutiSectionAddMore:(BOOL)addMore sectinModels:(NSArray <SectionModel *>*)newList reverse:(BOOL)reverse pageAble:(BOOL)pageable  {

    /// 多section情况
    if (!addMore) {//下拉
        [self.itemList removeAllObjects];
    }
    [self.itemList addObjectsFromArray:newList];
    if ([self respondsToSelector:@selector(reloadData)]) {
        [self performSelector:@selector(reloadData)];
    }

    if (self.enableFoot && pageable && self.itemList.count) {
        [self initFooter];
    }

    /*----------------------多section的情况------------------------*/

    if (addMore) {
        [self.mj_footer endRefreshing];
    } else {
        [self.mj_header endRefreshing];
    }
}


- (void)initFooter {
    if (self.enableFoot) {
        if (!self.mj_footer) {
            TMRefreshFooter * footer = [TMRefreshFooter footerWithRefreshingTarget:self.st_refresh refreshingAction:@selector(listAddMoreData)];
            self.mj_footer = footer;
            self.mj_header.hidden = NO;
        }
    }
}



- (void)reloadList {
    BOOL isSingle = self.itemList.firstObject.isSingleSection;
    if (isSingle) {
        /*----------------------单section的情况------------------------*/
        self.mj_header.hidden = NO;
        self.mj_footer.hidden = NO;
        if (self.singleListItems.count >= self.st_refresh.page_size * self.st_refresh.currentPage) {//大于当前请求数量
            [self.mj_footer endRefreshing];
        } else if (self.singleListItems.count > 0) {//小于当前请求数量
            [self.mj_footer endRefreshingWithNoMoreData];
            if (self.st_refresh.reverse) {
                self.mj_header.hidden = YES;
            }
        } else {//没有数据
            if (self.infiniteMore == NO) {
                self.mj_footer.hidden = YES;
                if (self.st_refresh.reverse) {
                    self.mj_header.hidden = YES;
                }
            }
        }
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if (self.preReloadHandle) {self.preReloadHandle(self);}
        
        if ([self respondsToSelector:@selector(reloadData)]) {
            [self performSelector:@selector(reloadData)];
        }
        if (self.reloadHandle) {self.reloadHandle(self);}
        
    });
    
}






- (void)examplePositionTag {
    
    
    /* 使用
     
     //TableView 网络请求数据
     
     - (void)configTableViewList {
     
     __weak typeof(self) belf = self;
     
     UIScrollView * listView = self.collectionView;
     listView.enableHead = YES;
     listView.enableFoot = YES;
     listView.enableEmpty = YES;
     listView.itemCellClass = NSClassFromString(@"xxxCell");
     listView.itemModelClass = NSClassFromString(@"xxxItemModel");
     
     List_Api api = ^NSString * _Nullable(BOOL addMore) {
         return ListDebugApi;
     };
     List_Parms parms = ^NSDictionary * _Nullable(NSMutableDictionary * _Nullable baseParms, BOOL addMore) {
         return nil;
     };
     List_Objs objs = ^NSArray<SectionModel *> * _Nullable(NSDictionary * _Nullable data,NSDictionary * _Nullable responseDic) {
         NSArray * tempArr = data[@"List"];
         return tempArr;
     };
     [listView setRefresherWithApi:api params:parms objs:objs requestType:RefreshNetWorkType_Post pageable:YES];
     [listView startHeadRefreshWithAnimation:YES];
     
     listView.listActionHandle = ^(id  _Nullable bview, NSIndexPath * _Nonnull indexPath, id  _Nullable obj, NSInteger type, STListActionType actionType, NSString * _Nullable des) {
         //点击列表单元格
     };
     
 }

     //CollectionView 网络请求数据
     
     - (void)configCollectionViewList {
     
     self.collectionView.lineCount = 2;
     self.collectionView.hPerW = 240.0/180.0;
     
     UIScrollView * listView = self.collectionView;
     listView.enableHead = YES;
     listView.enableFoot = YES;
     listView.enableEmpty = YES;
     listView.itemCellClass = NSClassFromString(@"xxxCell");
     listView.itemModelClass = NSClassFromString(@"xxxItemModel");
     
     List_Api api = ^NSString * _Nullable(BOOL addMore) {
     return ListDebugApi;
     };
     List_Parms parms = ^NSDictionary * _Nullable(NSMutableDictionary * _Nullable baseParms, BOOL addMore) {
     return nil;
     };
     List_Objs objs = ^NSArray<SectionModel *> * _Nullable(NSDictionary * _Nullable data,NSDictionary * _Nullable responseDic) {
     NSArray * tempArr = data[@"List"];
     return tempArr;
     };
     [listView setRefresherWithApi:api params:parms objs:objs requestType:RefreshNetWorkType_Post pageable:YES];
     [listView startHeadRefreshWithAnimation:YES];
     
     listView.listActionHandle = ^(id  _Nullable bview, NSIndexPath * _Nonnull indexPath, id  _Nullable obj, NSInteger type, STListActionType actionType, NSString * _Nullable des) {
     //点击列表单元格
     };
     
     }
     
     
     ///本地处理数据
     - (void)configTable2 {
         NSArray * arr = @[[NSObject new],[NSObject new],[NSObject new],[NSObject new]];
         UIScrollView * listView = self.tableView2;
     
         listView.itemCellClass = NSClassFromString(@"ItemCell");
         [listView ST_ListDefaultSetting];
         [listView.singleListItems addObjectsFromArray:arr];
         [listView reloadList];
     }
     
     */
    
    
}

@end




