//
//  UITableView+STBase.h
//  STTempPods
//
//  Created by 石磊 on 2022/3/22.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN




@interface UITableView (STBase)

- (void)ST_ListDefaultSetting;

@end



@interface UITableViewCell (STBase)

- (UITableView *)tableView;

@end


NS_ASSUME_NONNULL_END
