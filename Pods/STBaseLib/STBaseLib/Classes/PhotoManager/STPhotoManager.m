
#import "STPhotoManager.h"
#import "UIViewController+BackButtonHandler.h"
#import "STAppHelper.h"
#import "UIView+STLocalized.h"

#import "TZImagePickerController.h"
#import <YBImageBrowser/YBImageBrowser.h>
#import <YYImage/YYAnimatedImageView.h>
#import <objc/runtime.h>
#import "STDebug.h"
#import "STBaseNetworking.h"

@implementation YYAnimatedImageView (Extension)
+ (void)load {
    // hook：钩子函数
    Method method1 = class_getInstanceMethod(self, @selector(displayLayer:));
    Method method2 = class_getInstanceMethod(self, @selector(dx_displayLayer:));
    method_exchangeImplementations(method1, method2);
}

-(void)dx_displayLayer:(CALayer *)layer {
    
    if ([UIImageView instancesRespondToSelector:@selector(displayLayer:)]) {
        [super displayLayer:layer];
    }
    
}

@end


/*----------------------分割线------------------------*/
/*----------------------分割线------------------------*/

@implementation SLImagePickerConfig
@end


@implementation PhotoPickerItemModel

+ (instancetype)modelWithUrl:(NSURL *)url duration:(NSTimeInterval)duration {
    PhotoPickerItemModel * model = [PhotoPickerItemModel new];
    model.duration = duration;
    model.v_url = url;
    
    return model;
}

@end

/*----------------------分割线------------------------*/
/*----------------------分割线------------------------*/
@interface STPhotoManager ()<UINavigationControllerDelegate,UIImagePickerControllerDelegate>

@property(nonatomic, copy) ItemChoosedBlock handle;

@end


@implementation STPhotoManager

+ (STPhotoManager *)manager {
    static STPhotoManager * sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

//相册选择图片
+ (void)chooseImageWithMaxCount:(NSInteger)count presenter:(UIViewController *)presenter didFinishedChoose:(ItemChoosedBlock)choosedImages {
    [self chooseImageOrVideoWithMaxCount:count presenter:presenter didFinishedChoose:choosedImages setting:^(SLImagePickerConfig * _Nonnull configItem) {
        configItem.allowPickingVideo = NO;
    }];
}



//相册选择图片 - 带路径
+ (void)chooseImageOrVideoWithMaxCount:(NSInteger)count presenter:(UIViewController *)presenter didFinishedChoose:(ItemChoosedBlock)choosedImages {
    [self chooseImageOrVideoWithMaxCount:count presenter:presenter didFinishedChoose:choosedImages setting:nil];
}

+ (void)chooseImageOrVideoWithMaxCount:(NSInteger)count presenter:(UIViewController *)presenter didFinishedChoose:(ItemChoosedBlock)choosedImages setting:(nullable STPickerConfigHandler)setting {
    
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:count delegate:nil];
    
    imagePickerVc.allowPickingVideo = YES;
    imagePickerVc.allowTakeVideo = YES;
    imagePickerVc.showSelectBtn = NO;
    imagePickerVc.allowPickingGif = NO;
    imagePickerVc.showSelectedIndex = NO;
    imagePickerVc.modalPresentationStyle = UIModalPresentationFullScreen;
    imagePickerVc.statusBarStyle = UIStatusBarStyleDefault;
//    imagePickerVc.canBurnAfterReading = YES;
    
    if (setting) {
        SLImagePickerConfig * config = [SLImagePickerConfig new];
        setting(config);
        imagePickerVc.allowPickingVideo = config.allowPickingVideo;
        imagePickerVc.allowTakeVideo = config.allowPickingVideo;
        imagePickerVc.allowTakePicture = config.allowTakePicture;
    }
    
    
    
    [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
        
        NSMutableArray * arr = [NSMutableArray new];
        for (PHAsset * asset in assets) {
            PHAssetResource * resource = [[PHAssetResource assetResourcesForAsset:asset] firstObject];
            NSURL *tempPrivateFileURL = [resource valueForKey:@"privateFileURL"];
            LDLog(@"💖💖💖💖 %@  💖💖💖💖 ",tempPrivateFileURL);
            
            NSInteger index = [assets indexOfObject:asset];
            
            PhotoPickerItemModel * model = [PhotoPickerItemModel new];
            model.path = tempPrivateFileURL.absoluteString;
            model.image = photos[index];
            model.itemType = ItemModelTypeImage;
            
            [arr addObject:model];
        }
        if (choosedImages) {choosedImages(arr,photos);}
        
    }];
    
    
    [imagePickerVc setDidFinishPickingVideoHandle:^(UIImage *coverImage, PHAsset *asset) {
        PHAssetResource * resource = [[PHAssetResource assetResourcesForAsset:asset] firstObject];
        NSURL *tempPrivateFileURL = [resource valueForKey:@"privateFileURL"];
        LDLog(@"💖💖💖💖 %@  💖💖💖💖 ",tempPrivateFileURL);
        
        PhotoPickerItemModel * model = [PhotoPickerItemModel new];
        model.path = tempPrivateFileURL.absoluteString;
        model.image = coverImage;
        model.itemType = ItemModelTypeVideo;
        
        if (choosedImages) {choosedImages(@[model],@[coverImage]);}
        
    }];
    
    
    
    if (presenter) {
        [presenter presentViewController:imagePickerVc animated:YES completion:nil];
    } else {
        [[UIViewController currentViewController] presentViewController:imagePickerVc animated:YES completion:nil];
    }
    
}




+ (void)chooseVideoWithPresenter:(UIViewController *)presenter didFinishedChoose:(VideoChoosedBlock)videoChoosedHanle {
    
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:1 delegate:nil];
    
    imagePickerVc.allowPickingVideo = YES;
    imagePickerVc.allowTakeVideo = YES;
    
    imagePickerVc.allowPickingImage = NO;
    imagePickerVc.allowTakePicture = NO;
    
    imagePickerVc.showSelectBtn = NO;
    imagePickerVc.allowPickingGif = NO;
    imagePickerVc.showSelectedIndex = NO;
    imagePickerVc.statusBarStyle = UIStatusBarStyleDefault;
        
    imagePickerVc.videoMaximumDuration = 60;
    
    [imagePickerVc setDidFinishPickingVideoHandle:^(UIImage *coverImage, PHAsset *asset) {
        
        PHVideoRequestOptions *options = [[PHVideoRequestOptions alloc] init];
        options.version = PHVideoRequestOptionsVersionCurrent;
        options.deliveryMode = PHVideoRequestOptionsDeliveryModeAutomatic;
        PHImageManager *manager = [PHImageManager defaultManager];
        
        // 这三个方法都能拿到文件的路径
        // 1.Playback only. The result handler is called on an arbitrary queue.
        [manager requestPlayerItemForVideo:asset options:options resultHandler:^(AVPlayerItem * _Nullable playerItem, NSDictionary * _Nullable info) {
            AVURLAsset *urlAsset = (AVURLAsset *)playerItem.asset;
            NSURL *url = urlAsset.URL;
            PhotoPickerItemModel * model = [PhotoPickerItemModel modelWithUrl:url duration:CMTimeGetSeconds(urlAsset.duration)];
            if (videoChoosedHanle) {
                videoChoosedHanle(coverImage,model);
            }
        }];
    }];
    
    
    imagePickerVc.modalPresentationStyle = UIModalPresentationFullScreen;
    if (presenter) {
        [presenter presentViewController:imagePickerVc animated:YES completion:nil];
    } else {
        [[UIViewController currentViewController] presentViewController:imagePickerVc animated:YES completion:nil];
    }
}




+ (void)updateImages:(NSArray <id>*)images complect:(UpdateImagesBlock)complect hud:(BOOL)show {
//    if (show) {
//        [MBProgressHUD showMessage:@""];
//    }
//    [OSSImageUploader asyncUploadImages:images complete:^(NSArray<NSString *> *names, UploadImageState state) {
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [MBProgressHUD hideHUD];
//            if (complect) {
//                complect(names,state == UploadImageSuccess);
//            }
//        });
//    }];
    
    [[STBaseNetworking appNetworkClass] uploadImages:images loading:show success:^(NSArray<NSString *> * _Nonnull urlStrs) {
        if (complect) {
            complect(urlStrs,YES);
        }
    } fail:^(NSError * _Nonnull error) {
        if (complect) {
            complect(nil,NO);
        }
    }];
     
    
}


+ (void)chooseAndUpdateImageWithHandle:(UpdateImagesBlock)handle imageHandle:(nonnull ImageChoosedBlock)imageHandle {
    [STPhotoManager chooseImageWithMaxCount:1 presenter:[UIViewController currentViewController] didFinishedChoose:^(NSArray<PhotoPickerItemModel *> * _Nullable items, NSArray<UIImage *> * _Nullable images) {
        if (imageHandle) {
            imageHandle(images);
        }
        [STPhotoManager updateImages:images complect:handle hud:NO];
    }];
}

+ (void)chooseImageHandle:(ImageChoosedBlock)imageHandle updateHandle:(UpdateImagesBlock)handle maxCount:(NSInteger)count showUpdateHud:(BOOL)showHud {
    [STPhotoManager chooseImageWithMaxCount:count presenter:[UIViewController currentViewController] didFinishedChoose:^(NSArray<PhotoPickerItemModel *> * _Nullable items, NSArray<UIImage *> * _Nullable images) {
        if (imageHandle) {
            imageHandle(images);
        }
        [STPhotoManager updateImages:images complect:handle hud:showHud];
    }];
}



+ (void)takePhoto:(ItemChoosedBlock)handle {
    
    STPhotoManager * manage = [STPhotoManager manager];
    manage.handle = handle;
    
    UIImagePickerController * imagePickerController = [[ UIImagePickerController alloc ] init ];
    imagePickerController.delegate = manage ;
    imagePickerController.allowsEditing = NO;
    imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [[UIViewController currentViewController] presentViewController:imagePickerController animated:YES completion:nil];
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    UIImage * photo = [info objectForKey:UIImagePickerControllerOriginalImage];
    NSDictionary * meta = [info objectForKey:UIImagePickerControllerMediaMetadata];
    if (photo) {
        [[TZImageManager manager] savePhotoWithImage:photo meta:meta location:nil completion:^(PHAsset *asset, NSError *error){
            if (!error && asset) {
                PHAssetResource * resource = [[PHAssetResource assetResourcesForAsset:asset] firstObject];
                NSURL *tempPrivateFileURL = [resource valueForKey:@"privateFileURL"];
                
                PhotoPickerItemModel * model = [PhotoPickerItemModel new];
                model.path = tempPrivateFileURL.absoluteString;
                model.image = photo;
                model.itemType = ItemModelTypeImage;
                
                if (self.handle) {
                    self.handle(@[model],@[photo]);
                }
            }
        }];
    }
    
    
    
}


+ (void)singleChooseImageWithSheetImageChoosedHandle:(ItemChoosedBlock)imageHandle {
    
    [STAppHelper sheetWithTitle:nil content:nil cancelText:@"Cancel".st_localized sureText:@[@"Gallery".st_localized,@"Camera".st_localized] presenter:nil action:^(id  _Nullable objself, NSInteger handleType, id  _Nullable obj, NSString * _Nullable des) {
        if (handleType == 1) {
            [STPhotoManager chooseImageWithMaxCount:1 presenter:nil didFinishedChoose:imageHandle];
        } else if (handleType == 2) {
            [STPhotoManager takePhoto:imageHandle];
        }
    }];
}


@end



@implementation STPhotoManager (ImageBrowser)

+ (void)showBrowserWithImages:(NSArray *)images showIndex:(NSInteger)index {
    
    NSMutableArray * dataArr = [NSMutableArray new];
    for (id imageObj in images) {
        
        YBIBImageData * imageData = [YBIBImageData new];
        
        if ([imageObj isKindOfClass:[UIImage class]]) {
            imageData.image = ^UIImage * _Nullable{
                return imageObj;
            };
            LDLog(@"相册 - data = %@",imageObj);
        } else if ([imageObj isKindOfClass:[NSString class]] && [((NSString *)imageObj) hasPrefix:@"/var"]) {
            imageData.imagePath = imageObj;
            LDLog(@"相册 - path = %@",imageObj);
        } else {
            imageData.imageURL = [NSURL URLWithString:imageObj];
            LDLog(@"相册 - urlstring = %@",imageObj);
        }
        [dataArr addObject:imageData];
    }
    
    YBImageBrowser *browser = [YBImageBrowser new];
    browser.dataSourceArray = dataArr;
    browser.currentPage = index;
    browser.defaultToolViewHandler.topView.operationButton.hidden = YES;
    [browser show];
}


+ (void)showBrowserWithVideo:(NSString *)videoUrl {
        
//    YBIBVideoData *data2 = [YBIBVideoData new];
//    data2.videoURL = [NSURL URLWithString:videoUrl];
//
//    YBImageBrowser *browser = [YBImageBrowser new];
//    browser.dataSourceArray = @[data2];
//    browser.currentPage = 0;
//    browser.defaultToolViewHandler.topView.operationButton.hidden = YES;
//    [browser show];
}

@end

