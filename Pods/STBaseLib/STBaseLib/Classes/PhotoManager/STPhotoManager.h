
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class PhotoPickerItemModel,SLImagePickerConfig;

typedef enum {
    ItemModelTypeImage,
    ItemModelTypeVideo,
} ItemModelType;


typedef void(^VideoChoosedBlock)(UIImage * _Nullable coverImage,PhotoPickerItemModel * _Nullable vModel);

typedef void(^ImageChoosedBlock)(NSArray <UIImage *>* _Nullable images);

//typedef void(^ItemChoosedBlock)(NSArray <PhotoPickerItemModel *>* _Nullable items);

typedef void(^ItemChoosedBlock)(NSArray <PhotoPickerItemModel *>* _Nullable items,NSArray <UIImage *>* _Nullable images);

typedef void(^UpdateImagesBlock)(NSArray <NSString *>* _Nullable imageUrls,BOOL success);


typedef void(^STPickerConfigHandler)(SLImagePickerConfig * _Nonnull configItem);

NS_ASSUME_NONNULL_BEGIN


@interface PhotoPickerItemModel : NSObject

@property(nonatomic, copy) NSString *path;

@property(nonatomic, assign) ItemModelType itemType;

@property (nonatomic,strong) UIImage *image;

@property (nonatomic,strong) NSURL *v_url;

@property(nonatomic, assign) NSTimeInterval duration;

@end










@interface SLImagePickerConfig : NSObject

@property(nonatomic, assign) BOOL allowPickingVideo;

@property(nonatomic, assign) BOOL allowTakePicture;

@end


@interface STPhotoManager : NSObject

/// 相机拍照
+ (void)takePhoto:(ItemChoosedBlock)handle;

/// 单选图片
+ (void)singleChooseImageWithSheetImageChoosedHandle:(ItemChoosedBlock)imageHandle;

/// 选择图片
+ (void)chooseImageWithMaxCount:(NSInteger)count presenter:(nullable UIViewController *)presenter didFinishedChoose:(ItemChoosedBlock)choosedImages;

/// 选择图片或视频
+ (void)chooseImageOrVideoWithMaxCount:(NSInteger)count presenter:(nullable UIViewController *)presenter didFinishedChoose:(ItemChoosedBlock)choosedImages;

/// 选择图片
+ (void)chooseImageOrVideoWithMaxCount:(NSInteger)count presenter:(UIViewController *)presenter didFinishedChoose:(ItemChoosedBlock)choosedImages setting:(nullable STPickerConfigHandler)setting;


///选择视频
+ (void)chooseVideoWithPresenter:(UIViewController *)presenter didFinishedChoose:(VideoChoosedBlock)videoChoosedHanle;











/// 批量上传图片
+ (void)updateImages:(NSArray <UIImage *>*)images complect:(UpdateImagesBlock)complect hud:(BOOL)show;
//DEPRECATED_MSG_ATTRIBUTE("不要调用该方法 上传图片没有实现...");


///选择单张照片并上传
+ (void)chooseAndUpdateImageWithHandle:(UpdateImagesBlock)handle imageHandle:(ImageChoosedBlock)imageHandle;
//DEPRECATED_MSG_ATTRIBUTE("不要调用该方法 上传图片没有实现...");

///选择多张照片并上传
+ (void)chooseImageHandle:(ImageChoosedBlock)imageHandle updateHandle:(UpdateImagesBlock)handle maxCount:(NSInteger)count showUpdateHud:(BOOL)showHud;
//DEPRECATED_MSG_ATTRIBUTE("不要调用该方法 上传图片没有实现...");


@end


@interface STPhotoManager (ImageBrowser)

/// 显示图片浏览器
/// - Parameters:
///   - images: [path | UIImage | urlstring]
///   - index: 当前图片的index from 0
+ (void)showBrowserWithImages:(NSArray *)images showIndex:(NSInteger)index;


+ (void)showBrowserWithVideo:(NSString *)videoUrl;

@end

NS_ASSUME_NONNULL_END







