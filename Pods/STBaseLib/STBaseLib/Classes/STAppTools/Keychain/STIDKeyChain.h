

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface STIDKeyChain : NSObject

///检查是否存在
+(BOOL)checkExistForkey:(NSString *)key;

///随机一个序列号
+ (NSString *)randomIDFOrkey:(NSString *)key;


///保存到key
+ (void)save:(NSString *)key data:(id)data;

///删除
+ (void)delete:(NSString *)key;


///获取key对应的值
+ (id)load:(NSString *)key;



/**
 本方法是得到 UUID 后存入系统中的 keychain 的方法
 程序删除后重装,仍可以得到相同的唯一标示
 但是当系统升级或者刷机后,系统中的钥匙串会被清空,此时本方法失效 - 获取新的UUID
 */
+ (NSString *)getDeviceIDInKeychain;

///删除保存的设备UUID
+ (void)TestdeleteServer;


@end

NS_ASSUME_NONNULL_END
