//
//  STAppHelper.m
//  STBase
//
//  Created by 石磊 on 2022/11/1.
//
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "STBaseDefine.h"


NS_ASSUME_NONNULL_BEGIN

@interface STAppHelper : NSObject


@end


@interface STAppHelper (Alert)

+ (void)confirmWithTitle:(nullable NSString *)title content:(nullable NSString *)content presenter:(nullable UIViewController *)presenter action:(nullable HandleBlock)handle;

+ (void)confirmWithTitle:(NSString *)title content:(NSString *)content cancelText:(nullable NSString *)cancelText sureText:(nullable NSString *)sureText presenter:(nullable UIViewController *)presenter action:(nullable HandleBlock)handle;



+ (void)sheetWithTitle:(nullable NSString *)title content:(nullable NSString *)content cancelText:(nullable NSString *)cancelText sureText:(NSArray <NSString *>*)items presenter:(nullable UIViewController *)presenter action:(nullable HandleBlock)handle;


+ (void)showWithTitle:(NSString *)title style:(UIAlertControllerStyle)style content:(NSString *)content cancelText:(nullable NSString *)cancelText sureText:(nullable NSArray<NSString *> *)items presenter:(nullable UIViewController *)presenter action:(nullable HandleBlock)handle;


@end


NS_ASSUME_NONNULL_END
