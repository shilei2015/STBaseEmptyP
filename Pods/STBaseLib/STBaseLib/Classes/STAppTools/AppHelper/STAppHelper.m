//
//  STAppHelper.h
//  STBase
//
//  Created by 石磊 on 2022/11/1.
//
#import "STAppHelper.h"
#import "UIViewController+BackButtonHandler.h"

@implementation STAppHelper

+ (void)fdasf {
    //禁止手机睡眠
    [UIApplication sharedApplication].idleTimerDisabled = YES;
}

@end

@implementation STAppHelper (Alert)

+ (void)confirmWithTitle:(NSString *)title content:(NSString *)content cancelText:(NSString *)cancelText sureText:(NSString *)sureText presenter:(UIViewController *)presenter action:(HandleBlock)handle {
    [self showWithTitle:title style:UIAlertControllerStyleAlert content:content cancelText:cancelText sureText:@[sureText] presenter:presenter action:handle];
}


+ (void)confirmWithTitle:(nullable NSString *)title content:(nullable NSString *)content presenter:(nullable UIViewController *)presenter action:(HandleBlock)handle {
    NSString * item1Text = NSLocalizedString(@"Cancel", nil);
    NSString * item2Text = NSLocalizedString(@"Sure", nil);
    [self confirmWithTitle:title content:content cancelText:item1Text sureText:item2Text presenter:presenter action:handle];
}



+ (void)sheetWithTitle:(NSString *)title content:(NSString *)content cancelText:(NSString *)cancelText sureText:(NSArray<NSString *> *)items presenter:(UIViewController *)presenter action:(HandleBlock)handle {
    
    [self showWithTitle:title style:UIAlertControllerStyleActionSheet content:content cancelText:cancelText sureText:items presenter:presenter action:handle];
}

+ (void)showWithTitle:(NSString *)title style:(UIAlertControllerStyle)style content:(NSString *)content cancelText:(nullable NSString *)cancelText sureText:(NSArray<NSString *> *)items presenter:(UIViewController *)presenter action:(HandleBlock)handle {

    UIAlertController * alert = [UIAlertController alertControllerWithTitle:title message:content preferredStyle:style];

    if (cancelText) {
        UIAlertAction * action1 = [UIAlertAction actionWithTitle:cancelText style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
            if (handle) {
                handle(nil,0,nil,cancelText);
            }
        }];

        [alert addAction:action1];
    }
    
    
    [items enumerateObjectsUsingBlock:^(NSString * _Nonnull itemText, NSUInteger idx, BOOL * _Nonnull stop) {
        UIAlertAction * itemAction = [UIAlertAction actionWithTitle:itemText style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
            if (handle) {
                handle(nil,((int)idx + 1),nil,itemText);
            }
        }];
        [alert addAction:itemAction];
    }];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (presenter) {
            [presenter presentViewController:alert animated:YES completion:nil];
        } else {
            [[UIViewController currentViewController] presentViewController:alert animated:YES completion:nil];
        }
    });

}




@end






