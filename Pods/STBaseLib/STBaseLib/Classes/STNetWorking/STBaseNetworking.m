//
//  STNetworking.m
//  STBase
//
//  Created by 石磊 on 2022/11/7.
//

#import "STBaseNetworking.h"
#import "ST_Refresher.h"
#import "NSString+STBase.h"
#import <YYModel/YYModel.h>
#import "STRefreshProtocal.h"
#import "MBProgressHUD+Hud.h"
#import "NSMutableDictionary+STBase.h"
#import "STDebug.h"
#import "UIImage+STSize.h"
#import "UIImageView+SDImage.h"


@interface STBaseNetworking ()

@property (nonatomic,strong) NSMutableDictionary *requestKeyMap;

@property (nonatomic,strong) NSMutableDictionary *publicHeaderMap;


@end



static NSString * kNetClassName_InApp = nil;


@implementation STBaseNetworking


+ (NSString *)baseUrlString {
    if ([STDebug debugHostType] == 1) {
        return [STBaseNetworking.appNetworkClass baseUrlString_debug];
    } else if ([STDebug debugHostType] == 2) {
        return [STBaseNetworking.appNetworkClass baseUrlString_release];;
    }
    return [STBaseNetworking.appNetworkClass baseUrlString_default];
}


+ (void)setCommHeaderParms:(NSDictionary *)headerParms {
    [STBaseNetworking manager].publicHeaderMap = [NSMutableDictionary dictionaryWithDictionary:headerParms];
}


+ (void)configAppNetworkClass:(Class)mClass {
    kNetClassName_InApp = NSStringFromClass(mClass);
    LDInfoLog(@"网络请求配置 mClass = %@",kNetClassName_InApp);
    [ST_Refresher configAppNetworkClass:mClass];
    
}

+ (Class<SDKNetworkingProtocal>)appNetworkClass {
    Class<SDKNetworkingProtocal> class = NSClassFromString(kNetClassName_InApp);
    return class;
}



+ (void)cancelRequestWithURL:(nullable NSString *)url {
    NSString * urlstring = url;
    if (![url hasPrefix:@"http"]) {
        urlstring = [[STBaseNetworking baseUrlString] stringByAppendingString:url];
    }
    [[YHNet sharedInstance] cancelRequestWithURL:urlstring];
}

// 取消所有的网络请求
+ (void)cancelAllRequest {
    [[YHNet sharedInstance] cancelAllRequest];
}

+ (instancetype)manager {
    static STBaseNetworking * sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}


+ (void)requestWithApiName:(NSString *)apiStr parms:(nullable NSDictionary *)requestParms requestType:(YHHttpMethod)type SuccessBlock:(nullable NetComplectBlock)complectBlock {
    [self requestWithApiName:apiStr parms:requestParms requestType:type SuccessBlock:complectBlock loadingHud:NO errorTip:NO needCache:NO identifer:nil];
}

+ (void)requestWithApiName:(NSString *)apiStr parms:(NSDictionary *)requestParms requestType:(YHHttpMethod)type SuccessBlock:(NetComplectBlock)complectBlock loadingHud:(BOOL)showHud errorTip:(BOOL)tipError {
    [self requestWithApiName:apiStr parms:requestParms requestType:type SuccessBlock:complectBlock loadingHud:showHud errorTip:tipError needCache:NO identifer:nil];
}

//MARK: - - - - - - - - - - - 发送请求
+ (void)requestWithApiName:(NSString *)apiStr parms:(NSDictionary *)requestParms requestType:(YHHttpMethod)type SuccessBlock:(NetComplectBlock)complectBlock loadingHud:(BOOL)showHud errorTip:(BOOL)tipError needCache:(BOOL)needCache identifer:(NSString *)identifer {
    
    NSString * URL = [NSString stringWithFormat:@"%@",apiStr];
    
    NSMutableDictionary *header = [STBaseNetworking manager].publicHeaderMap;
    if (!header) {header = [NSMutableDictionary new];}
    
    
    NSMutableDictionary * parmsDict = [NSMutableDictionary dictionaryWithDictionary:requestParms];
    [self dealWithHeaderDict:header requestParms:parmsDict];
    
    
    if (showHud) {
        [MBProgressHUD showMessage:nil];
    }
    
    if (![URL hasPrefix:@"http"]) {
        URL = [[STBaseNetworking baseUrlString] stringByAppendingString:URL];
    }
    
    if (!URL || ![NSURL URLWithString:URL]) {return;}
    
    [[YHNet sharedInstance] httpRequestWithMethod:type
                                              url:URL
                                            param:requestParms
                                          headers:header
                                       isUseHttps:NO
                            requestSerializerType:YHHttpRequestSerializerTypeJSON
                           responseSerializerType:YHHttpResponseSerializerTypeJSON
                                    progressBlock:^(CGFloat progress) {
        
    } successBlock:^(id  _Nonnull responseObject) {
        
        if (showHud) {[MBProgressHUD hideHUD];}
        
        [STBaseNetworking netComplectLogWithApi:URL header:header requestParms:requestParms responseObject:responseObject error:nil networkType:type];
        
        // Determine if the data is a dictionary
        if (![responseObject isKindOfClass:[NSDictionary class]]) {
            NSString * kErrorDomain = [NSString appBundleID];
            NSError *error = [NSError errorWithDomain:kErrorDomain code:-1 userInfo:@{NSLocalizedDescriptionKey: @"Data format error"}];
            
            if (tipError) {[self tipWithError:error];}
            if (complectBlock) {complectBlock(nil,error);}
            return;
        }
        
        if (tipError) {[self tipWithResponseData:responseObject];}
        
        if (complectBlock) {complectBlock(responseObject,nil);}
        
    } errorBlock:^(NSError * _Nonnull error) {
        if (showHud) {[MBProgressHUD hideHUD];}
        [STBaseNetworking netComplectLogWithApi:URL header:header requestParms:requestParms responseObject:nil error:error networkType:type];
        
        if (tipError) {[self tipWithError:error];}
        if (complectBlock) {complectBlock(nil,error);}
    }];
    
}


+ (nullable NSURLSessionDataTask *)uploadWithURL:(NSString *)url
                                           files:(NSArray<YHUploadFileModel *> *)files
                                           param:(nullable id)param
                                         headers:(nullable NSDictionary<NSString *, NSString *> *)headers
                                      isUseHttps:(BOOL)isUseHttps
                                   progressBlock:(nullable YHHttpRequestProgressBlock)progressBlock
                                    successBlock:(nullable YHHttpRequestSuccessBlock)successBlock
                                      errorBlock:(nullable YHHttpRequestErrorBlock)errorBlock showHud:(BOOL)showHud {
    
    if (showHud) {
        [MBProgressHUD showMessage:nil];
    }
    
    NSMutableDictionary *header = [STBaseNetworking manager].publicHeaderMap;
    if (!header) {header = [NSMutableDictionary new];}
    NSMutableDictionary * parmsDict = [NSMutableDictionary dictionaryWithDictionary:param];
    [self dealWithHeaderDict:header requestParms:parmsDict];
    
    NSURLSessionDataTask * task = [[YHNet sharedInstance] uploadWithURL:url files:files param:parmsDict headers:header isUseHttps:NO progressBlock:progressBlock successBlock:^(id  _Nonnull responseObject) {
        
        if (showHud) {
            [MBProgressHUD hideHUD];
        }
        
        [STBaseNetworking netComplectLogWithApi:url header:headers requestParms:param responseObject:responseObject error:nil networkType:YHHttpMethodPOST];
        if (successBlock) {
            successBlock(responseObject);
        }
        
    } errorBlock:^(NSError * _Nonnull error) {
        
        [STBaseNetworking netComplectLogWithApi:url header:headers requestParms:param responseObject:nil error:error networkType:YHHttpMethodPOST];
        if (errorBlock) {
            errorBlock(error);
        }
        
        if (showHud) {[self tipWithError:error];}
    }];
    return task;
}


+ (void)uploadImages:(NSArray<UIImage *> *)images
             preDeal:(nullable void(^)(UIImage *image))preDeal
                 url:(NSString *)url
        successBlock:(nullable YHHttpUpImageSuccessBlock)successBlock
          errorBlock:(nullable YHHttpRequestErrorBlock)errorBlock
             showHud:(BOOL)showHud
{
    
    if (showHud) {
        [MBProgressHUD showMessage:nil];
    }
    
    NSMutableDictionary *header = [STBaseNetworking manager].publicHeaderMap;
    if (!header) {header = [NSMutableDictionary new];}
    NSMutableDictionary * parmsDict = [NSMutableDictionary new];
    [self dealWithHeaderDict:header requestParms:parmsDict];
    
    if (![url hasPrefix:@"http"]) {
        url = [[STBaseNetworking baseUrlString] stringByAppendingString:url];
    }
    
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        NSMutableArray * uploadSucArr = [NSMutableArray new];
        __block int complectCount = 0;
        
        dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
        
        for (int i = 0; i < images.count; i ++) {
            
            LDInfoLog(@"上传第%d张图片",i);
            
            UIImage * image = images[i];
            
            UIImage * uploadImg = image;
            if (preDeal) {preDeal(uploadImg);}
            
            YHUploadFileModel * model = [YHUploadFileModel new];
            model.data = UIImageJPEGRepresentation(uploadImg, 0.8);
            model.name = @"File";
            model.fileName = @"File";
            model.mimeType = @"image/png";
            
            
            [[YHNet sharedInstance] uploadWithURL:url files:@[model] param:parmsDict headers:header isUseHttps:NO progressBlock:nil successBlock:^(id  _Nonnull responseObject) {
                
                
                complectCount += 1;
                
                [STBaseNetworking netComplectLogWithApi:url header:header requestParms:parmsDict responseObject:responseObject error:nil networkType:YHHttpMethodPOST];
                
                
                NSInteger code;
                NSError *error;
                NSDictionary * data;
                NSString * msg;
                
                if ([responseObject isKindOfClass:[NSDictionary class]]) {
                    code = [responseObject[@"code"] integerValue];

                    if (code == 0) {
                        data = responseObject[@"data"];
                        NSString * urlString = [NSString stringWithFormat:@"%@",data[@"src"]];
                        [UIImageView cacheImage:image urlstring:urlString];
                        [uploadSucArr addObject:urlString];

                    } else {
                        data = responseObject[@"data"];
                        msg = [NSString stringWithFormat:@"%@",data[@"toast"]];
                        NSString * kErrorDomain = [NSString appBundleID];
                        error = [NSError errorWithDomain:kErrorDomain code:code userInfo:@{NSLocalizedDescriptionKey: msg}];
                    }
                } else {
                    NSString * kErrorDomain = [NSString appBundleID];
                    error = [NSError errorWithDomain:kErrorDomain code:-1 userInfo:@{NSLocalizedDescriptionKey: @"Data format error"}];
                }
                
                LDInfoLog(@"第%d张上传完成",i);
                if (complectCount == images.count) {
                    if (showHud) {[MBProgressHUD hideHUD];}
                    if (successBlock) {
                        successBlock(uploadSucArr);
                    }
                }
                
                dispatch_semaphore_signal(semaphore);
                
            } errorBlock:^(NSError * _Nonnull error) {
                
                complectCount += 1;
                
                LDInfoLog(@"第%d张上传失败%@",i,error.localizedDescription);
                [self netComplectLogWithApi:url header:header requestParms:parmsDict responseObject:nil error:error networkType:YHHttpMethodPOST];
                
//                if (errorBlock) {
//                    errorBlock(error);
//                }
                
                if (complectCount == images.count) {
                    if (showHud) {[MBProgressHUD hideHUD];}
                    if (successBlock) {
                        successBlock(uploadSucArr);
                    }
                }
                dispatch_semaphore_signal(semaphore);
                
            }];
            
            dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
            
        }
    });
    
    LDInfoLog(@"上传操作完成");
}




+ (void)uploadFiles:(NSArray<YHUploadFileModel *> *)files
                 url:(NSString *)url
        successBlock:(nullable YHHttpUpImageSuccessBlock)successBlock
          errorBlock:(nullable YHHttpRequestErrorBlock)errorBlock
             showHud:(BOOL)showHud
{
    
    if (showHud) {
        [MBProgressHUD showMessage:nil];
    }
    
    NSMutableDictionary *header = [STBaseNetworking manager].publicHeaderMap;
    if (!header) {header = [NSMutableDictionary new];}
    NSMutableDictionary * parmsDict = [NSMutableDictionary new];
    
    [self dealWithHeaderDict:header requestParms:parmsDict];
    
    if (![url hasPrefix:@"http"]) {
        url = [[STBaseNetworking baseUrlString] stringByAppendingString:url];
    }
    
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        NSMutableArray * uploadSucArr = [NSMutableArray new];
        __block int complectCount = 0;
        
        dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
        
        for (int i = 0; i < files.count; i ++) {
            
            LDInfoLog(@"上传第%d个文件",i);
            
            YHUploadFileModel * model = files[i];
            [parmsDict sl_setObject:model.fileName forKey:@"Rename"];
            
            [[YHNet sharedInstance] uploadWithURL:url files:@[model] param:parmsDict headers:header isUseHttps:NO progressBlock:nil successBlock:^(id  _Nonnull responseObject) {
                
                complectCount += 1;
                
                [STBaseNetworking netComplectLogWithApi:url header:header requestParms:parmsDict responseObject:responseObject error:nil networkType:YHHttpMethodPOST];
                
                
                NSInteger code;
                NSError *error;
                NSDictionary * data;
                NSString * msg;
                
                if ([responseObject isKindOfClass:[NSDictionary class]]) {
                    code = [responseObject[@"code"] integerValue];

                    if (code == 0) {
                        data = responseObject[@"data"];
                        NSString * urlString = [NSString stringWithFormat:@"%@",data[@"src"]];
                        [uploadSucArr addObject:urlString];

                    } else {
                        data = responseObject[@"data"];
                        msg = [NSString stringWithFormat:@"%@",data[@"toast"]];
                        NSString * kErrorDomain = [NSString appBundleID];
                        error = [NSError errorWithDomain:kErrorDomain code:code userInfo:@{NSLocalizedDescriptionKey: msg}];
                    }
                } else {
                    NSString * kErrorDomain = [NSString appBundleID];
                    error = [NSError errorWithDomain:kErrorDomain code:-1 userInfo:@{NSLocalizedDescriptionKey: @"Data format error"}];
                }
                
                LDInfoLog(@"第%d个上传完成",i);
                if (complectCount == files.count) {
                    if (showHud) {[MBProgressHUD hideHUD];}
                    if (successBlock) {
                        successBlock(uploadSucArr);
                    }
                }
                
                dispatch_semaphore_signal(semaphore);
                
            } errorBlock:^(NSError * _Nonnull error) {
                
                complectCount += 1;
                
                LDInfoLog(@"第%d个上传失败%@",i,error.localizedDescription);
                [self netComplectLogWithApi:url header:header requestParms:parmsDict responseObject:nil error:error networkType:YHHttpMethodPOST];
                
                if (complectCount == files.count) {
                    if (showHud) {[MBProgressHUD hideHUD];}
                    if (successBlock) {
                        successBlock(uploadSucArr);
                    }
                }
                dispatch_semaphore_signal(semaphore);
                
            }];
            
            dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
            
        }
    });
    
    LDInfoLog(@"上传操作完成");
}






//MARK: - - - - - - - - - - - 处理header和参数
+ (void)dealWithHeaderDict:(NSMutableDictionary *)header requestParms:(NSMutableDictionary *)parmsDict {
    //处理签名验证
    if ([self.appNetworkClass conformsToProtocol:@protocol(SDKNetworkingProtocal)]) {
        [self.appNetworkClass signtureDealWithHeaderDict:header requestParms:parmsDict];
    }
}

+ (void)tipWithResponseData:(NSDictionary *)responseData {
    if ([self.appNetworkClass conformsToProtocol:@protocol(SDKNetworkingProtocal)]) {
        NSString * tipText = [self.appNetworkClass netWorkTipString:responseData];
        if ([tipText isKindOfClass:[NSString class]] && tipText.length) {
            [MBProgressHUD showTip:tipText];
        }
    }
}


+ (void)tipWithError:(nullable NSError *)error {
    if (!error) return;
    if ([self.appNetworkClass conformsToProtocol:@protocol(SDKNetworkingProtocal)]) {
        [MBProgressHUD showError:error.localizedDescription];
    }
}



//MARK: - - - - - - - - - - - 请求结束打印数据
+ (void)netComplectLogWithApi:(NSString *)apiStr header:(NSDictionary *)header requestParms:(NSDictionary *)requestParms responseObject:(id)responseObject error:(nullable NSError *)error networkType:(YHHttpMethod)type {
    
    if ([STDebug enableLog_requestDetail]) {
        
        NSString * networkType = @"POST";
        if (type == YHHttpMethodGET) {
            networkType = @"GET";
        }
        
        NSMutableString * logStr = [NSMutableString new];
        [logStr appendString:[NSString stringWithFormat:@"\n🟢🟢🟢🟢🟢🟢🟢🟢🟢🟢🟢🟢🟢🟢🟢🟢🟢🟢🟢🟢🟢🟢🟢🟢🟢🟢🟢🟢🟢🟢🟢🟢🟢🟢🟢🟢🟢🟢🟢🟢🟢🟢🟢🟢🟢{\n"]];
        [logStr appendString:[NSString stringWithFormat:@"                            ## 👉👉👉👉👉调用接口\n%@\n", apiStr]];
        [logStr appendString:[NSString stringWithFormat:@"```\n                            👉👉👉👉👉Header\n%@\n", header]];
        [logStr appendString:[NSString stringWithFormat:@"                            👉👉👉👉👉参数\n%@\n", requestParms]];
        [logStr appendString:[NSString stringWithFormat:@"                            👉👉👉👉👉请求方式\n%@\n", networkType]];
        
        
        if (!error) {
            [logStr appendString:[NSString stringWithFormat:@"                            ❤️❤️❤️❤️❤️❤️返回数据\n%@\n```", [responseObject yy_modelToJSONObject]]];
            [logStr appendString:@"}"];
            LDInfoLog(@"%@", logStr);
        } else {
            [logStr appendString:[NSString stringWithFormat:@"                            ❌❌❌❌❌error\n%@\n```", [error debugDescription]]];
            [logStr appendString:@"}"];
            LDErrorLog(@"%@", logStr);
        }
        
        
        
    } else if ([STDebug enableLog_api]) {
        NSMutableString * logStr = [NSMutableString new];
        if (!error) {
            [logStr appendString:[NSString stringWithFormat:@" ## ❤️❤️❤️❤️❤️❤️调用接口【%@】\n", apiStr]];
        } else {
            [logStr appendString:[NSString stringWithFormat:@" ## ❌❌❌❌❌调用接口【%@】\n", apiStr]];
        }
        LDErrorLog(@"%@", logStr);
    }
    
}

+ (BOOL)enableLog {
    return [STDebug enableLog_requestDetail];
}



+ (void)testSemaphore {
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        
        int count = 10;
        LDInfoLog(@"create-【1】");
        for (int i = 0; i < count; i ++) {
            LDInfoLog(@"Start-【%d】",i);
        
            dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                LDInfoLog(@"OK-【%d】",i);
                if (i == count - 1) {
                    LDInfoLog(@"全部完成");
                }
                
                dispatch_semaphore_signal(semaphore);
                
            });
            
            dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
            
        }
        
        LDInfoLog(@"任务完成");
        
    });
    
    
}
@end
