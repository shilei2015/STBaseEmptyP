
#import <Foundation/Foundation.h>

//#ifdef DEBUG
//#import <DWLogger/DWLogger.h>
//#import <DWLogger/DWLogView.h>
//#endif

//#define NSLog(FORMAT, ...) fprintf(stderr, "[%s %s %s %s-第%d行] %s\n", __DATE__ , __TIME__, __func__, [[[NSString stringWithUTF8String: __FILE__] lastPathComponent] UTF8String], __LINE__, [[[NSString alloc] initWithData:[[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] dataUsingEncoding:NSUTF8StringEncoding] encoding:NSNonLossyASCIIStringEncoding] UTF8String]?[[[NSString alloc] initWithData:[[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] dataUsingEncoding:NSUTF8StringEncoding] encoding:NSNonLossyASCIIStringEncoding] UTF8String]:[[NSString stringWithFormat: FORMAT, ## __VA_ARGS__] UTF8String]);




#ifdef DEBUG

#define NSLog(FORMAT, ...) fprintf(stderr,"--------------------------------------------------------------------------\n%s>%s:%d\t%s\n",__TIME__,[[[NSString stringWithUTF8String:__FILE__] lastPathComponent] UTF8String], __LINE__, [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);


#define LDLog(fmt, ...) {\
NSLog((@"\n📒📒📒" fmt), ##__VA_ARGS__);\
};

#define LDInfoLog(fmt, ...) {\
NSLog((@"\nℹ️ℹ️" fmt), ##__VA_ARGS__);\
};


#define LDErrorLog(fmt, ...) {\
NSLog((@"\n❌❌❌" fmt), ##__VA_ARGS__);\
};

#define LDWarnLog(fmt, ...) {\
NSLog((@"\n❗️❗️❗️" fmt), ##__VA_ARGS__);\
};

#define LDStepLog(fmt, ...) {\
NSLog((@"\n👣👣👣" fmt), ##__VA_ARGS__);\
};

#define LDLogFunction {\
NSLog((@"\n🚏🚏🚏 %@"), NSStringFromSelector(_cmd));\
}

#else

#define LDLog(...);
#define LDInfoLog(...);
#define LDErrorLog(...);
#define LDWarnLog(...);
#define LDStepLog(...)
#define LDLogFunction
#define NSLog(...)

#endif





NS_ASSUME_NONNULL_BEGIN

@interface STDebug : NSObject
///当前是否为DEBUG模式
+ (BOOL)isDebugModel;
+ (BOOL)isSimulator;

+ (void)debugDo:(void(^)(void))doCode;

@end


@interface STDebug (DebugSetting)

///是否打印调用的接口
+ (BOOL)enableLog_api;

///是否打印调用的接口详情
+ (BOOL)enableLog_requestDetail;

///是否打印timer运行日志
+ (BOOL)enableLog_timer;

///是否打印播放日志
+ (BOOL)enableLog_player;

///是否跳过验证
+ (BOOL)enableVerify;

///接口类型 0，1、2
+ (NSInteger)debugHostType;

///用户身份 0、1、2
+ (NSInteger)debugUserType;

///测试模式 0、1
+ (NSInteger)debugSysType;


+ (NSString *)version;

+ (NSString *)appId;


///通过key获取对应的值
+ (id)settingValueForKey:(NSString *)settingKey;

///获取settings.bundle中的值  未设置
+ (BOOL)enableKey:(NSString *)settingKey;

@end

NS_ASSUME_NONNULL_END
