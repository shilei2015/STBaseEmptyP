
#import "STDebug.h"
#import "NSObject+STBase.h"

#define KEnableInjection 0

@implementation STDebug

+ (void)load {
#ifdef DEBUG
    [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidFinishLaunchingNotification object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        [STDebug DebugLogStart];
    }];
#endif
}

+ (BOOL)isDebugModel {
#ifdef DEBUG
    return YES;
#endif
    return NO;
}

+ (BOOL)isSimulator {
#ifdef TARGET_IPHONE_SIMULATOR
    return YES;
#endif
    return NO;
}

#pragma mark 内部方法
+ (void)DebugLogStart {
#ifdef DEBUG
//    [DWLogManager configDefaultLogger];
//    [DWLogView showPot];
#endif
}

#define kSafeBlock(blockName,...) ({!blockName ? nil : blockName(__VA_ARGS__);})


+ (void)debugDo:(void (^)(void))doCode {
    if ([self isDebugModel]) {
        kSafeBlock(doCode);
    }
}


@end


@implementation STDebug (DebugSetting)

+ (BOOL)enableLog_api {
    return [self enableKey:@"kSettingKey_api"];
}


+ (BOOL)enableLog_requestDetail{
    return [self enableKey:@"kSettingKey_request"];
}


+ (BOOL)enableLog_timer {
    return [self enableKey:@"kSettingKey_timer"];
}

+ (BOOL)enableLog_player {
    return [self enableKey:@"kSettingKey_player"];
}

+ (BOOL)enableVerify {
    return [self enableKey:@"kSettingKey_verify" defaultV:YES];
}



+ (NSInteger)debugHostType {
    return [[self settingValueForKey:@"kSettingKey_baseApi"] integerValue];
}

+ (NSInteger)debugUserType {
    return [[self settingValueForKey:@"kSettingKey_userType"] integerValue];
}

+ (NSInteger)debugSysType {
    return [[self settingValueForKey:@"kSettingKey_systemType"] integerValue];
}

+ (NSString *)version {
    return [self settingValueForKey:@"kSettingKey_versionConfig_Version"];
}

+ (NSString *)appId {
    return [self settingValueForKey:@"kSettingKey_versionConfig_AppId"];
}

+ (BOOL)enableKey:(NSString *)settingKey defaultV:(BOOL)defaultV {
    id value = [self settingValueForKey:settingKey];
    if (value) {return [value boolValue];}
#ifdef DEBUG
    return YES;
#else
    return defaultV;
#endif
}


+ (BOOL)enableKey:(NSString *)settingKey {
    id value = [self settingValueForKey:settingKey];
    if (value) {return [value boolValue];}
    return NO;
}


+ (id)settingValueForKey:(NSString *)settingKey {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    id value = [userDefaults objectForKey:settingKey];
    if (value) {return value;}
    return value;
}


@end





#import <objc/runtime.h>
#import <objc/message.h>
#import <UIKit/UIKit.h>

@interface InjectionIIIHelper : NSObject

@end

@implementation InjectionIIIHelper

#ifdef TARGET_IPHONE_SIMULATOR

#if KEnableInjection
+ (void)load {

    //注册项目启动监听
    __block id observer =
    [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidFinishLaunchingNotification object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {

        //更改bundlePath
        [[NSBundle bundleWithPath:@"/Applications/InjectionIII.app/Contents/Resources/iOSInjection.bundle"] load];

        [[NSNotificationCenter defaultCenter] removeObserver:observer];
    }];

    /*
    //给UIViewController 注册injected 方法
    class_addMethod([UIViewController class], NSSelectorFromString(@"injected"), (IMP)injected, "v@:");
     */

    /*
    //给uiview 注册injected 方法
    class_addMethod([UIView class], NSSelectorFromString(@"injected"), (IMP)injected, "v@:");
     */

    //统一添加 injected 方法
//    class_addMethod([NSObject class], NSSelectorFromString(@"injected"), (IMP)injected, "v@:");
    
    class_addMethod([UIResponder class], NSSelectorFromString(@"injected"), (IMP)injected, "v@:");

}
#endif

#endif

/**
 InjectionIII 热部署会调用的一个方法，
 runtime给VC绑定上之后，每次部署完就重新viewDidLoad
 */
void injected (id self, SEL _cmd) {
    
    if ([self isKindOfClass:[UIViewController class]]) {//vc 刷新
        
        [NSObject resetEventTimeInTime:0.3 handle:^(id  _Nullable objself, NSInteger handleType, id  _Nullable obj, NSString * _Nullable des) {
            [InjectionIIIHelper reloadVC:self];
        }];
        
        
    } else if ([self isKindOfClass:[UIView class]]){//view 刷新
        
        
        UIViewController *vc = [InjectionIIIHelper viewControllerSupportView:self];
        if (vc && [vc isKindOfClass:[UIViewController class]]) {

            [NSObject resetEventTimeInTime:0.3 handle:^(id  _Nullable objself, NSInteger handleType, id  _Nullable obj, NSString * _Nullable des) {
                [InjectionIIIHelper reloadVC:vc];
            }];
        }
        
    } else {
        
        LDLog(@"SELF.CLASS = %@",NSStringFromClass([self class]));
        
    }
}


+ (void)reloadVC:(UIViewController *)vc {
    
    BOOL enableCustom = NO;
    SEL selector = @selector(injectionReload);
    if ([vc respondsToSelector:selector] && enableCustom) {
        [vc performSelector:selector];
//        vc = [vc.class new];
        LDLog(@"🍇🍇🍇🍇🍇🍇--1 %@",vc);
    } else {
        [vc beginAppearanceTransition:YES animated:YES];
        
        [vc viewDidLoad];
        [vc viewWillLayoutSubviews];
        [vc viewWillAppear:NO];
        
        [vc endAppearanceTransition];
        LDLog(@"🍇🍇🍇🍇🍇🍇--2 %@",vc);
    }
    
}

/**
 获取view 所属的vc，失败为nil
 */
+ (UIViewController *)viewControllerSupportView:(UIView *)view {
    for (UIView* next = [view superview]; next; next = next.superview) {
        UIResponder *nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            return (UIViewController *)nextResponder;
        }
    }
    return nil;
}


@end




#import "NSObject+STBase.h"
@interface NSObject (KVCDefender)
 
@end
 
 
@implementation NSObject (KVCDefender)
 
// 不建议拦截 `setValue:forKey:` 方法
+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
 
        // 拦截 `setValue:forKey:` 方法，替换自定义实现
        [NSObject exchangeSelector:@selector(setValue:forKey:) enchangeSel:@selector(ysc_setValue:forKey:)];
 
    });
}
 
//- (void)ysc_setValue:(id)value forKey:(NSString *)key {
//    if (key == nil) {
//        NSString *crashMessages = [NSString stringWithFormat:@"crashMessages : [<%@ %p> setNilValueForKey]: could not set nil as the value for the key %@.",NSStringFromClass([self class]),self,key];
//        NSLog(@"%@", crashMessages);
//        return;
//    }
//
//    @try {
//        [self ysc_setValue:value forKey:key];
//    } @catch (NSException *exception) {
//        NSLog(@"%@",exception);
//    }
//
//
//}
//
//- (void)setNilValueForKey:(NSString *)key {
//    NSString *crashMessages = [NSString stringWithFormat:@"crashMessages : [<%@ %p> setNilValueForKey]: could not set nil as the value for the key %@.",NSStringFromClass([self class]),self,key];
//    NSLog(@"%@", crashMessages);
//}
//
//- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
//    NSString *crashMessages = [NSString stringWithFormat:@"crashMessages : [<%@ %p> setValue:forUndefinedKey:]: this class is not key value coding-compliant for the key: %@,value:%@'",NSStringFromClass([self class]),self,key,value];
//    NSLog(@"%@", crashMessages);
//}
//
//- (nullable id)valueForUndefinedKey:(NSString *)key {
//    NSString *crashMessages = [NSString stringWithFormat:@"crashMessages :[<%@ %p> valueForUndefinedKey:]: this class is not key value coding-compliant for the key: %@",NSStringFromClass([self class]),self,key];
//    NSLog(@"%@", crashMessages);
//
//    return self;
//}
 
@end
